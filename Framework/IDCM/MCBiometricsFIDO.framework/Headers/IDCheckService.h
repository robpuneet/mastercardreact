//
//  IDCheckService.h
//
//  Copyright © 2016 mastercard. All rights reserved.

#import <Foundation/Foundation.h>
#import "Enums.h"
#import "IDCheckUIConfig.h"
#import "Account.h"

///----------------------------------
/// @name SDK Constants
///----------------------------------

/// Transaction authentication type
typedef NSString * TransactionAuthType;

/// "IN_BAND"
extern _Nonnull TransactionAuthType const TransactionAuthTypeInBand;

/// "TRANSFER_FUNDS"
extern _Nonnull TransactionAuthType const TransactionAuthTypeTransferFunds __attribute__((deprecated("Use TransactionAuthTypeInBand instead")));

/// "MOBILE_BANKING"
extern _Nonnull TransactionAuthType const TransactionAuthTypeMobileBanking __attribute__((deprecated("Use TransactionAuthTypeInBand instead")));

/// "CUSTOMER_SERVICE"
extern _Nonnull TransactionAuthType const TransactionAuthTypeCustomerService __attribute__((deprecated("Use TransactionAuthTypeInBand instead")));

/// "CALL_CENTER_TRANSACTION"
extern _Nonnull TransactionAuthType const TransactionAuthTypeCallCenterTransaction __attribute__((deprecated("Use TransactionAuthTypeInBand instead")));

/// "ONLINE_BANKING"
extern _Nonnull TransactionAuthType const TransactionAuthTypeOnlineBanking __attribute__((deprecated("Use TransactionAuthTypeInBand instead")));

/// "3DS"
extern _Nonnull TransactionAuthType const TransactionAuthType3DS;

/// "SUSPECT_TRANSACTION"
extern _Nonnull TransactionAuthType const TransactionAuthTypeSuspectTransaction;

/// "null"
extern _Nonnull TransactionAuthType const TransactionAuthTypeNull;


/// Transaction status
typedef NSString * TransactionStatus;

/// "ALL"
extern _Nonnull TransactionStatus  const TransactionStatusAll;

/// "PENDING"
extern _Nonnull TransactionStatus  const TransactionStatusPending;

/// "VERIFIED"
extern _Nonnull TransactionStatus  const TransactionStatusVerified;

/// "CANCELLED"
extern _Nonnull TransactionStatus  const TransactionStatusCancelled;

/// "CANCELED"
extern _Nonnull TransactionStatus  const TransactionStatusCanceled;

/// "ABANDONED"
extern _Nonnull TransactionStatus  const TransactionStatusAbandoned;

/// "VERIFICATION_PENDING"
extern _Nonnull TransactionStatus  const TransactionStatusVerificationPending;

/// "VERIFICATION_SUCCESSFUL"
extern _Nonnull TransactionStatus  const TransactionStatusVerificationSuccessful;

/// "FAILED"
extern _Nonnull TransactionStatus  const TransactionStatusVerificationFailed;

/// "DECLINED"
extern _Nonnull TransactionStatus  const TransactionStatusDeclined;

/// "EXPIRED_WITH_VERIFICATION_PENDING"
extern _Nonnull TransactionStatus  const TransactionStatusExpiredWithVerificationPending;

/// "FRAUD"
extern _Nonnull TransactionStatus  const TransactionStatusFraud;


/**
 Sets up device and account specific security parameters to identify authenticity of the device and the account. Responsible for preparing and processing data for all business routines.
 */
@interface IDCheckService: NSObject

///----------------------------------
/// @name Class level service methods
///----------------------------------

/**
 Returns the shared singleton instance.
 */
+ (IDCheckService * _Nonnull)sharedInstance;

///----------------------------------------------
/// @name Device Registration & Verification APIs
///----------------------------------------------

/**
 It prepares request data for Add Device API. Resulted information needs to be sent over the network to back-end server.
 @param completionHandler       Callback handler block returns the device data.
 
 - *responseString*:            Device data in json string value.
 - *error*:                     If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)initializeDeviceRegistrationWithCompletionHandler:(void (^ _Nullable)(NSString * _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 Processes received data from the server for Add Device API.
 @discussion                    Once complete device registration is successful then it will mark device as registered.
 @param accountID               Customer Account ID which binds to Biometrics Authentication process.
 @param registrationInfo        Base64EncodedString received on AddDevice API response.
 @param completionHandler       Callback handler block returns whether the device registration is success or not.
 
 - *isSuccess*:                 Bool value indicates the device registration is successfully completed or not.
 - *error*:                     If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)completeDeviceRegistrationForAccount:(NSString * _Nonnull)accountID
                              registrationInfo:(NSString * _Nullable)registrationInfo
                           completionHandler:(void (^ _Nullable)(BOOL isSuccess, NSError * _Nullable error))completionHandler;

/**
 Use this method to complete device verification once Add Device API completes successfully. Verification code needs to be received from a push notification and should be passed here to complete device verification process.
 @param verificationCode        Verification Code received in push notification.
 @param nickname                User specified nickname which is used to identify account for authentication process.
 @param completionHandler       Callback handler block returns whether the device verification is success or not.
 
 - *isSuccess*:                 Bool value indicates the device verification is successfully completed or not.
 - *error*:                     If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)verifyDeviceWithVerificationCode:(NSString * _Nonnull)verificationCode
                             forNickname:(NSString * _Nonnull)nickname
                       completionHandler:(void (^ _Nullable)(BOOL isSuccess, NSError * _Nullable error))completionHandler;

///----------------------------------
/// @name Authenticator Registration API
///----------------------------------

/**
 Use this method to register FIDO authenticator.
 @param arrayOfAuthenticatorTypes Use the Enum of AuthenticatorType to pass in the authenticator types array.
  */
- (void)registerAuthenticators:(NSArray*_Nonnull)arrayOfAuthenticatorTypes;

///----------------------------------
/// @name Enrolment APIs
///----------------------------------

/**
 Use this method to initiate FIDO-compliant biometric registration (enrolment) process. Send prepared data to back-end server to get FIDO-compliant biometric registration information.
 @param completionHandler   Callback block for complete initialize FIDO-compliant registration API.
 
 - *responseString*:        Returns required information for back-end to generate FIDO-compliant registration request.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)initializeFIDORegistrationWithCompletionHandler:(void(^ _Nullable)(NSString * _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 Use this to start FIDO-compliant biometric registration process.
 @param object              Object that is FIDO-compliant registration request data received from back-end.
 @param parentViewController Developer needs to pass in the reference of the view controller on top of which IDCM SDK will show its UI.
 @param delegate            IDCheckUIChooseAuthenticatorDelegate delegate to customize the choose authenticator dialog.
 @param completionHandler   Callback block that returns the response of process FIDO-compliant registration response.
 
 - *responseObject*:        Response string that is received as a part of callback from this api.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)processFIDORegistrationWithResponseObject:(NSString * _Nonnull)object
                         withParentViewController:(UIViewController* _Nonnull)parentViewController
                                         delegate:(id<IDCheckUIChooseAuthenticatorDelegate> _Nullable)delegate
                            withCompletionHandler:(void (^ _Nullable)(NSString * _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 Use this method to complete FIDO-compliant biometric registration process.
 @param response            Object that is returned in initializeFIDORegistrationWithCompletionHandler: API.
 @param completionHandler   Callback block that returns the response of completed FIDO-compliant acknowledgement registration response.
 
 - *responseObject*:        Response string that is received as a part of callback from this api.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)completeFIDORegistrationWithResponse:(NSString *_Nonnull)response
                       withCompletionHandler:(void (^_Nullable)(NSString * _Nullable responseObject, NSError * _Nullable error))completionHandler;

///----------------------------------
/// @name List Transaction APIs
///----------------------------------

/**
 Use this method to initalize list transaction to prepare payload for sending list transaction request to Mastercard authentication platform. Prepares request data to get list of transaction for specified status and type.
 @param transactionAuthType     Transaction type or usecase received for the transaction that can be PENDING, COMPLETED, CANCELED, etc.
 @param transactionStatus       Filter transactions list by its status. e.g. All, PENDING, VERIFIED, FAILED, CANCELLED, EXPIRED, etc.
 @param completionHandler       Callback handler block returns whether the device verification is success or not.
 
 - *responseString*:            Required information to fetch list of required transactions.
 - *error*:                     If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)initializeListTransactionsWithAuthType:(_Nonnull TransactionAuthType)transactionAuthType
                        withTransactionStatus:(_Nonnull TransactionStatus)transactionStatus
                        withCompletionHandler:(void (^ _Nullable)(NSString * _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 Processes received data from the server of List Transaction API.
 @param responseObject          Encrypted object received from issuer.
 @param completionHandler       Callback handler block returns the response string of the list transactions.
 
 - *responseString*:            Returns the list transaction data. Application may need to parse resulting data for further usage.
 - *error*:                     If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)completeListTransactionWithObject:(NSString * _Nonnull)responseObject
                     withCompletionHandler:(void (^ _Nullable)(NSDictionary * _Nullable responseString, NSError * _Nullable error))completionHandler;

///----------------------------------
/// @name Cancel Transaction APIs
///----------------------------------

/**
 Use this method for Verify Transaction API to cancel the verification. It prepares request data for cancellation of verification. Send prepared data to back-end server to cancel the authentication.
 @param transactionId           Unique Id for the given transaction.
 @param notMyTransaction        True, if transaction is not initiated by user. Otherwise false.
 @param completionHandler       Callback for initialize cancel transaction that will return the request object for complete cancel transaction API.
 
 - *responseString*:            Response of initialize transaction which is required for complete cancel transaction service.
 - *error*:                     If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)initializeCancelTransactionWithID:(NSString * _Nonnull)transactionId
                         notMyTransaction:(BOOL)notMyTransaction
                    withCompletionHandler:(void (^ _Nullable)(NSString * _Nullable responseString, NSError *  _Nullable error))completionHandler;

/**
 Processes received data from the server for cancellation of the transaction.
 @param object                  Result received from back-end server from initialized cancel transaction.
 @param completionHandler       Callback block that will return the result of the complete cancel transaction service.
 
 - *responseString*:            Response of the complete cancel transaction service.
 - *error*:                     If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)completeCancelTransactionWithObject:(id _Nonnull)object
                       withCompletionHandler:(void (^ _Nullable)(id _Nullable responseString, NSError * _Nullable error))completionHandler;

///----------------------------------
/// @name Authentication APIs
///----------------------------------

/**
 Use this method to initiate FIDO-compliant biometric authentication process. Send prepared data to back-end server to get FIDO-compliant biometric authentication information.
 @param transactionID                     Unique Id for the given transaction.
 @param transactionAuthenticationType     Authentication type for that transaction.
 @param auxiliaryData                     Auxiliary data contains the additional parameters for initialization of cancel transaction.
 @param authenticationAssuranceLevel      AssuranceLevel required for that transaction
 @param completionHandler   Callback block for complete initialize FIDO-compliant authentication API.
 
 - *responseString*:        Response string that is required for complete FIDO-compliant authentication API.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)initializeFIDOAuthenticationWithTransactionID:(NSString *_Nonnull)transactionID
                    withTransactionAuthenticationType:(NSString *_Nonnull)transactionAuthenticationType
                                    withAuxiliaryData:(NSString *_Nonnull)auxiliaryData
                     withAuthenticationAssuranceLevel:(AssuranceLevel)authenticationAssuranceLevel
                                withCompletionHandler:(void (^_Nullable)(NSString * _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 This will process FIDO-compliant biometric authentication process. It will process the authenticators to user to authenticate using Biometrics Face, Voice, Fingerprint, etc. Each authenticator provides 5 maximum attempts before it permanently locks. And after each attempts it locks temporarily. Locking period increases by 30 seconds every time. To unlock authenticator, you need to deregister the device and enroll it again.
 @param response            Object that is FIDO-compliant authentication request data received from back-end.
 @param delegate            IDCheckUIChooseAuthenticatorDelegate delegate to customize the choose authenticator dialog.
 @param parentViewController Developer needs to pass in the reference of the view controller on top of which IDCM SDK will show its UI.
 @param completionHandler   Callback block that returns the response of process FIDO-compliant acknowledgement authentication response.
 
 - *responseString*:        Response string that is received as a part of callback from this api.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)processFIDOAuthenticationWithResponse:(NSString * _Nonnull)response
                                     delegate:(id<IDCheckUIChooseAuthenticatorDelegate> _Nullable)delegate
                       onParentViewController:(UIViewController * _Nonnull)parentViewController
                        withCompletionHandler:(void (^_Nullable)(NSString *  _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 Use this method as a final step of FIDO-compliant biometric authentication.
 @param response            Object that is returned in processFIDOAuthenticationWithResponse: API.
 @param completionHandler   Callback block that returns the response of completed FIDO-compliant authentication response.
 
 - *isSuccess*:             True, indicates FIDO-compliant authentication is successful. False, otherwise.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)completeFIDOAuthenticationWithResponse:(NSString *_Nonnull)response
                         withCompletionHandler:(void (^_Nullable)(BOOL isSuccess, NSError * _Nullable error))completionHandler;

///-----------------------------------
/// @name Offline Fetch Template APIs
///-----------------------------------

/**
 Initializes Fetch Offline policies which contains policies with assurance levels.
 @param completionHandler   Callback block for initialize fetch policies.
 
 - *responseString*:        Response string that is required for Initialize Fetch Offline policy.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)initializeFetchOfflineTemplatesWithCompletionHandler:(void (^ _Nullable)(NSString * _Nullable responseString, NSError * _Nullable error))completionHandler;


/**
 Complete Fetch Offline policies which states if policies have been recived or not.
 @param responseObject      Response object that is received from the initializeFetchOfflineTemplatesWithCompletionHandler: API.
 @param completionHandler   Callback block for complete fetch policies.
 
 - *responseObject*:        Response string that is required for Complete Fetch Offline policy.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)completeFetchOfflineTemplatesWithObject:(id _Nonnull)responseObject
                           withCompletionHandler:(void (^_Nullable)(NSDictionary * _Nullable responseObject, NSError *_Nullable error))completionHandler;

/**
 Retrieves the number of offline transactions saved in DB.
 @return                    NSInteger of offline transactions saved
 */
- (NSInteger)getOfflineTransactionsDBCount;

///-----------------------------------
/// @name Offline Authentication APIs
///-----------------------------------

/**
 Authenticates offline transactions. Transaction ID must be unique for each transaction. Make sure policies for offline authentication are available. Each authenticator provides 5 maximum attempts before it permanently locks. And after each attempt it locks temporarily. Locking period increases by 30 seconds every time. To unlock authenticator, you need to deregister the device and enroll it again.
 @param transactionAuthType Transaction usecase for the transaction
 @param assuranceLevel      AssuranceLevel required for that transaction
 @param transactionAuxiliaryDataDict    transactionAuxiliaryData required for that transaction
 @param transactionId       transactionId required to create transaction which must be unique
 @param parentViewController Developer needs to pass in the reference of the view controller on top of which IDCM SDK will show its UI.
 @param completionHandler   Callback block for Initialize offline authentication
 
 - *responseString*:        Response string that is required for Initialize offline authentication.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)initializeOfflineAuthenticationWithTransactionAuthType:(NSString * _Nonnull)transactionAuthType
                                            withAssuranceLevel:(AssuranceLevel)assuranceLevel
                                  withTransactionAuxiliaryData:(NSDictionary * _Nonnull)transactionAuxiliaryDataDict
                                             withTransactionId:(NSString * _Nonnull)transactionId
                                        onParentViewController:(UIViewController * _Nonnull)parentViewController
                                         withCompletionHandler:(void (^ _Nullable)(NSString * _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 Initialize send offline transactions when network connection becomes available.
 @param completionHandler   Callback block for Initialize send offline authentication.
 
 - *responseString*:        Response string for Initialize send offline authentication.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)initializeSendOfflineTransactionsWithCompletionHandler:(void (^_Nullable)(NSString * _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 Complete send offline transactions which removes caches transactions inside SDK
 @param responseObject      Response object that is received from initialize method of send offline transaction.
 @param completionHandler   Callback block for send offline transactions.
 
 - *responseObject*:        Response string that is required for complete send offline transactions.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)completeSendOfflineTransactionsWithObject:(id _Nonnull)responseObject
                             withCompletionHandler:(void (^_Nullable)(id _Nullable responseObject, NSError * _Nullable error))completionHandler;

///------------------------------------
/// @name List and Disable Device APIs
///------------------------------------

/**
 Initialize list devices associated with same account.
 @param completionHandler   Callback block for initialize list devices.
 
 - *responseString*:        Response string for initialize list devices .
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)initializeListDevicesWithCompletionHandler:(void(^ _Nullable)(NSString * _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 Complete list devices which lists the devices linked to the same account.
 @param responseObject      ResponseObject for completing the list devices.
 @param completionHandler   Callback block for list devices api.
 
 - *responseString*:        Response string that is required for complete list devices.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)completeListDevicesWithObject:(id _Nonnull)responseObject
                withCompletionHandler:(void(^ _Nullable)(id _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 Initialize Disable devices associated with same account.
 @param arrayOfDevicesToDisable Array of devices to be disabled which are linked to current account.
 @param completionHandler       Callback block for initialize disable devices.
 
 - *responseString*:        Response string for initialize list devices .
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)initializeDisableDevices:(NSArray* _Nonnull)arrayOfDevicesToDisable
           withCompletionHandler:(void(^ _Nullable)(NSString * _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 Complete disable devices which confirms the operation status.
 @param responseObject      ResponseObject for completing disable devices api.
 @param completionHandler   Callback block for complete disable devices.
 
 - *responseString*:        Response string that is required for complete disable devices.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)completeDisableDevicesWithObject:(id _Nonnull)responseObject
                   withCompletionHandler:(void(^ _Nullable)(id _Nullable responseString, NSError * _Nullable error))completionHandler;

///-----------------------------------------
/// @name Unenrol or Deregister Device APIs
///-----------------------------------------

/**
 Initiate the process for un-enrollment or de-registration. Use this method to de-register all the FIDO-compliant biometric authenticators. User will not be able to authenticate once this process completes.
 @param completionHandler   Callback block for initialize deregister device.
 
 - *responseString*:        Response string that is required for initialize deregister device.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)initializeDeregisterDeviceWithCompletionHandler:(void(^ _Nullable)(NSString * _Nullable responseString, NSError * _Nullable error))completionHandler;

/**
 Complete deregister device which confirms the operation status.
 @param responseObject      Response object for completing deregister device api that is received from initializeDeregisterDeviceWithCompletionHandler: response.
 @param completionHandler   Callback block for complete deregister device.
 
 - *responseString*:        Response string that is required for complete deregister device.
 - *error*:                 If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
- (void)completeDeregisterDeviceWithObject:(id _Nonnull)responseObject
                       withCompletionHandler:(void(^ _Nullable)(id _Nullable responseString, NSError * _Nullable error))completionHandler;

///----------------------------------
/// @name Database APIs
///----------------------------------

/**
 Returns the account information registered with FIDO.
 @return                    Account object.
 */
- (Account * _Nonnull)getAccount;

/**
 Retrieves the Account ID for given nickname.
 @param nickname            Device nickname for which Account ID is expected.
 @return                    Account ID for the given nickname, otherwise nil.
 */
- (NSString * _Nullable)getAccountIDForNickname:(NSString * _Nonnull)nickname;

/**
 Retrieves the nickname for given account status.
 @param accountStatus       Status of the account for which nickname is needed.
 @return                    Device nickname for the given account status, otherwise nil.
 */
- (nullable NSString *)getNicknameForAccountStatus:(AccountStatus)accountStatus;

/**
 Returns the account status whether user is registered or not.
 @return                    True, if user is registered. Otherwise false.
 */
- (BOOL)isAccountRegistered;

/**
 Returns the account status whether device is verified or not.
 @return                    True, if device is verified. Otherwise false.
 */
- (BOOL)isDeviceVerified;

/**
 Returns the account status whether user is enrolled or not.
 @return                    True, if user is enrolled. Otherwise false.
 */
- (BOOL)isAccountEnrolled;

/**
 Use this API to delete account locally. Removes all the local data alongwith enrollment data associated with the account from the device. Generally used when account is deleted from another device or by back-end.
 */
- (void)deleteAccount;

@end
