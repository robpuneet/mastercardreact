//
//  IDCheckConfig.h
//  MCBiometricsFIDO
//
//  Copyright © 2017 Mastercard. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Provides configuration points of the SDK.
 */
@interface IDCheckConfig : NSObject

/**
 Sets the base url for MIS server.
 @param baseURLString URL string to be set for MIS server.
 */
+ (void)setBaseURL:(NSString * _Nonnull)baseURLString;

/**
 Gets the base url for MIS server.
 @return Base url in String format.
 */
+ (nullable NSString *)getBaseURL;

/**
 Sets the configurations needed for sdk.
 @param configDict Key-value pair to be set for the sdk.
 */
+ (void)setIDCheckConfiguration:(NSDictionary * _Nullable)configDict;

/**
 Gets the configurations that were set with setIDCheckConfiguration: method.
 @return Configurations in key-value pair.
 */
+ (nullable NSDictionary *)getIDCheckConfiguration;

/**
 Gets the configurations value for key.
 @return Configurations value.
 */
+ (id _Nullable )getValueFromConfigurationForKey:(NSString *_Nonnull)key;

/**
 Sets application screen capture while it is not in active state. Snapshot will be taken from Info.plist file by reading the following keys in order:
 
 - UILaunchStoryboardName
 - UILaunchImages,
 - UILaunchImageName,
 - UILaunchImageFile,
 
 If you do not wish to use screen capture, just set the flag as false. Defaults to false.
 **Note:** This method should always be called either from didFinishLaunchingWithOptions to register for screen capture.
 */
+ (void)setScreenCaptureFlag:(BOOL)isEnabled;

/**
 Gets the Screen Capture configuration. True for allowed screen capture, false otherwise.
 */
+ (BOOL)getScreenCaptureFlag;

/**
 Use this method to send the push notification data to SDK for further processing the payload. The complete payload needs to be passed here to complete the setup.
 @param payload                 Payload received in push notification.
 @param completionHandler       Callback handler block returns whether the setup is successful or not.
 
 - *status*:                    String value indicates if the push notification setup is successfully completed or not with the status message.
 - *error*:                     If an error occurred, this object describes the error. If the operation completed successfully, this value is nil.
 */
+ (void)setupPushNotificationWithPayload:(NSDictionary * _Nonnull)payload
                       completionHandler:(void (^ _Nullable)(NSDictionary * _Nullable status, NSError * _Nullable error))completionHandler;
@end

