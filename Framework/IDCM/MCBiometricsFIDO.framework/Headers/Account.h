//
//  Account.h
//  MCBiometricsFIDO
//
//  Copyright © 2017 Mastercard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Enums.h"

/**
 Holds account information for sdk.
 */
@interface Account : NSObject

/**
 Account id for given account.
 */
@property (nonatomic, strong) NSString *accountID;

/**
 Nickname for the given account.
 */
@property (nonatomic, strong) NSString *nickname;

/**
 Contains the account status for the given account.
 */
@property (nonatomic, assign) AccountStatus status;

@end
