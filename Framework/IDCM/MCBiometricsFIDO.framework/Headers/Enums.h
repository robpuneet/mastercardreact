//
//  Enums.h
//  MCBiometricsFIDO
//
//  Copyright © 2017 Mastercard. All rights reserved.
//

#ifndef Enums_h
#define Enums_h

/// Assurance level
typedef NS_ENUM(NSInteger, AssuranceLevel) {
    /// Low
    AssuranceLevelLow = 0,
    /// Medium
    AssuranceLevelMedium,
    /// High
    AssuranceLevelHigh,
};

///Account status for identity check account.
typedef NS_ENUM(NSInteger, AccountStatus) {
    /// When account status can't be retrieved, it returns ACCOUNT_INDETERMINATE
    ACCOUNT_INDETERMINATE = -1,
    /// When account is initialized and completed the device registration, then account status is Registered.
    ACCOUNT_REGISTERED = 0,
    /// When account is verified, then account status is Activated.
    ACCOUNT_VERIFIED = 1,
    /// When account has completed either of one enrollment type, then account status is Enrolled.
    ACCOUNT_ENROLLED = 2,
};


/// Here constants represents the error code generated when any error occurs in Mastercard sdk.
typedef NS_ENUM(NSInteger, ErrorConstants) {
    
    MCJailbrokenDeviceError = 3000,
    
    MCDeviceDisabledBeforeVerifying = 3118,
    
    MCDeviceDisabledError = 4405,
    
    MCInvalidDataError = 10001,
    
    MCNilInputError = 10002,
    
    MCOfflineError = 10003,
    
    MCInvalidNicknameError = 10004,
    
    MCInvalidFidoLicenseError = 10005,
    
    MCDecryptionFailError = 10006,
    
    MCBadJSONDataError = 10007,
    
    MCClientDatabaseError = 10008,
    
    MCInvalidAccountError = 10009,
    
    MCInvalidBiometricsError = 10010,
    
    MCOfflineTemplateNotFoundError = 10011,
    
    MCSomethingWentWrongError = 20001,
    
    // Common Error Code
    OFFLINE_ERROR_CODE                      = 111,
    INVALID_DATA_ERROR                      = 1051,
    DECRYPTION_FAILED_CODE                  = 4403,
    CHANNEL_BLOCKED_ERROR                   = 4406,
    SSL_ERROR_CODE                          = 12284,
    URL_DOMAIN_ERROR                        = 1112,
};

/// Types of FIDO Authenticators supported.
typedef NS_ENUM(NSInteger, AuthenticatorType) {
    AuthenticatorTypeFaceID,
    AuthenticatorTypeTouchID,
    AuthenticatorTypeFace,
    AuthenticatorTypePin,
    AuthenticatorTypeVoice,
};
#endif
