//
//  IDCheckUIConfig.h
//  MCBiometricsFIDO
//
//  Copyright © 2017 Mastercard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

/**
 This protocol will allow to customize the authenticator selection dialog for title, message and images.
 */
@protocol IDCheckUIChooseAuthenticatorDelegate <NSObject>

@optional
/**
 Gets the tint color for authenticator dialog.
 @return        UIColor.
 */
- (UIColor * _Nonnull)getTintColor;

/**
 Gets the attributed string title for the authenticator dialog.
 @return        Attributed title string.
 */
- (NSAttributedString * _Nonnull)getAttributedStringForTitle;

/**
 Gets the attributed string message for the authenticator dialog.
 @return        Attributed message string
 */
- (NSAttributedString * _Nonnull)getAttributedStringForMessage;

/**
 Sets the title for the UIAlertAction of authenticator dialog.
 @param index   Index of the UIAlertAction.
 @return        Title for alert action.
 */
- (NSString * _Nonnull)setTitleForIndex:(NSUInteger)index;

/**
 Sets the image for the UIAlertAction of authenticator dialog.
 @param index   Index of the UIAlertAction
 */
- (UIImage * _Nonnull)setImageForIndex:(NSUInteger)index;

@end

/**
 This class is kept for future customization in UI.
 */
@interface IDCheckUIConfig : NSObject

@end
