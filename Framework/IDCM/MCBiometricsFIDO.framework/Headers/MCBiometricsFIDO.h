//
//  MCBiometricsFIDO.h
//
//  Copyright © 2017 Mastercard. All rights reserved.

#import <UIKit/UIKit.h>

// Project version number for MCBiometricsFIDO.
FOUNDATION_EXPORT double MCBiometricsFIDOVersionNumber;

// Project version string for MCBiometricsFIDO.
FOUNDATION_EXPORT const unsigned char MCBiometricsFIDOVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MCBiometricsFIDO/PublicHeader.h>

#import "Account.h"
#import "Enums.h"
#import "IDCheckConfig.h"
#import "IDCheckService.h"
#import "IDCheckUIConfig.h"
