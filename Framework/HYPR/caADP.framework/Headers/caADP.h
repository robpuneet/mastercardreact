/*
 * Copyright (c) 2013-2016 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

/*
 * caADP.h
 *
 */

#ifndef caADP_
#define caADP_

#ifdef __cplusplus
extern "C" {
#endif

#include "tee_client_api.h"

uint8_t caOpen(int8_t *pSrcData, size_t nSrcDataLength, int8_t *pDstData,
                   size_t nDstDataLength, int8_t *pOutData, size_t nOutData);

TEEC_Result Send1(int8_t *pSrcData, size_t nSrcDataLength,
                      int8_t *pDstData, size_t nDstDataLength,
                      int8_t *pZ, size_t nZ);

void caClose(void);

#ifdef __cplusplus
}
#endif
#endif // caADP_
