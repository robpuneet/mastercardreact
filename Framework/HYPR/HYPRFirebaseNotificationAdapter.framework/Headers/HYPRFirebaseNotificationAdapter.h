//
//  HYPRFirebaseNotificationAdapter.h
//  HYPRFirebaseNotificationAdapter
//
//  Created by Robert Carlsen on 2/25/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HYPRFirebaseNotificationAdapter.
FOUNDATION_EXPORT double HYPRFirebaseNotificationAdapterVersionNumber;

//! Project version string for HYPRFirebaseNotificationAdapter.
FOUNDATION_EXPORT const unsigned char HYPRFirebaseNotificationAdapterVersionString[];

#import <HYPRFirebaseNotificationAdapter/HYPRFirebaseAdapter.h>

