//
//  HyprFirebaseAdapter.h
//  HyprFirebasePushNotificationAdapter
//
//  Created by Parita Shah on 1/12/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HYPRPushNotificationAdapter.h>

/**
 The Firebase Adapter expects configuration details such as keys and identifiers must be added via GoogleService-Info.plist. This file should be included in the Application bundle which is integrating HYPRFirebaseAdapter.
 */
@interface HYPRFirebaseAdapter : NSObject <HYPRPushNotificationAdapter>

+(instancetype)sharedAdapter;

/**
 Provide the incoming Firebase push notification payload to the adapter for processing

 This is typically obtained from one of the notification delegate APIs.

 @param userInfo The push notification payload to be processed.
 @param completion Completion handler called if the notifcation could be processed. Contains optional user info and error if any operation failed.
 @return YES if the adapter could process the payload as a valid HYPR message. NO otherwise.
 */
+(BOOL)handlePushPayloadUserInfo:(NSDictionary*)userInfo completion:(HYPRNotificationHandlerCompletion)completion;
@end
