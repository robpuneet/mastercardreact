//
//  HYPRFaceAuthenticatorFBA.h
//  HyprCore
//
//  Created by Ievgen Kreshchenko on 8/31/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HyprAuthenticatorProtocol.h>
#import <HyprCore/HYPRAuthenticatorCustomizationProtocol.h>

@interface HYPRFaceAuthenticatorFBA : NSObject <HyprAuthenticatorProtocol, HyprAuthenticatorUIProtocol, HYPRAuthenticatorCustomizationProtocol>

@end
