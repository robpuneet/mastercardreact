//
//  HYPRFace.h
//  HYPRFace
//
//  Created by Parita Shah on 2/13/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HYPRFace.
FOUNDATION_EXPORT double HYPRFaceVersionNumber;

//! Project version string for HYPRFace.
FOUNDATION_EXPORT const unsigned char HYPRFaceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HYPRFace/PublicHeader.h>

#import <HYPRFace/HYPRFaceAuthenticator.h>
#import <HYPRFace/HYPRFaceAuthenticatorFBA.h>
#import <HYPRFace/HYPRFaceAsm.h>
#import <HYPRFace/HYPRFaceAuthenticatorViewConfiguration.h>
