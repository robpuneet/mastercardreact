//
//  HYPRFaceAsm.h
//  HyprCore
//
//  Created by Parita Shah on 2/13/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HyprCore/HyprAsmProtocol.h>
#import <HyprCore/ASMManager.h>
#import <HyprCore/HYPRAuthenticatorCustomizationProtocol.h>

@interface HYPRFaceAsm : ASMManager <HyprAsmProtocol, HYPRAuthenticatorCustomizationProtocol>

@end
