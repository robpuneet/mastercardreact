//
//  HYPRFaceAuthenticatorViewConfiguration.h
//  HYPRFaceAuthenticatorViewConfiguration
//
//  Created by Ethan Moon on 2/14/19.
//  Copyright © 2019 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>
#import <UIKit/UIImage.h>
#import <UIKit/UIFont.h>
#import <UIKit/UIView.h>

@interface HYPRFaceAuthenticatorViewConfiguration : NSObject <NSCopying, NSCoding>

+ (instancetype _Nonnull )sharedInstance;

- (instancetype _Nonnull)init NS_UNAVAILABLE;
- (instancetype _Nonnull)new NS_UNAVAILABLE;

/**
 * Face authenticator top background color
 */
@property (nonatomic, copy, nullable) UIColor *topBackgroundColor;
/**
 * Face authenticator bottom background color
 */
@property (nonatomic, copy, nullable) UIColor *bottomBackgroundColor;
/**
 * Face authenticator instructions text
 */
@property (nonatomic, copy, nullable) NSString *instructionsText;
/**
 * Face authenticator instructions text font
 */
@property (nonatomic, copy, nullable) UIFont *instructionsTextFont;
/**
 * Face authenticator instructions text color
 */
@property (nonatomic, copy, nullable) UIColor *instructionsTextColor;
/**
 * Face authenticator silhouette image (Use this to replace the spinning circle)
 */
@property (nonatomic, copy, nullable) UIImage *faceSilhouetteImage;
/**
 * Face authenticator silhouette image content mode
 */
@property (nonatomic) UIViewContentMode faceSilhouetteContentMode;
/**
 * Face authenticator cancel button color
 */
@property (nonatomic, copy, nullable) UIColor *cancelButtonColor;

@end

