//
//  HYPRFaceAuthenticator.h
//  HyprCore
//
//  Created by Parita Shah on 2/13/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HyprAuthenticatorProtocol.h>
#import <HyprCore/HYPRAuthenticatorCustomizationProtocol.h>

@interface HYPRFaceAuthenticator : NSObject <HyprAuthenticatorProtocol, HyprAuthenticatorUIProtocol, HYPRAuthenticatorCustomizationProtocol>

@end
