//
//  CrustySession.h
//  HYPRADP
//
//  Created by Robert Panebianco on 7/30/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CrustyWrapper.h"

@class CrustyWrapper;

NS_ASSUME_NONNULL_BEGIN

@interface CrustySession : NSObject

+(instancetype)sharedInstance;
-(instancetype)new NS_UNAVAILABLE;

-(void)request:(CrustyWrapper*)crusty payload:(NSData*)payload receiveBytes:(NSMutableData*)receiveBytes error:(NSError *_Nullable*)error;
-(NSData*)decryptReply:(CrustyWrapper*)crusty receiveBytes:(NSData*)receiveBytes error:(NSError *_Nullable*)error;
-(NSData*)validateHMAC:(CrustyWrapper*)crusty receiveBytes:(NSData*)receiveBytes error:(NSError *_Nullable*)error;

@end

NS_ASSUME_NONNULL_END
