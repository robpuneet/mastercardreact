//
//  CrustyWrapper.h
//  HYPRADP
//
//  Created by Robert Panebianco on 7/30/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Crusty.h"
#import "CrustyContext.h"
#import "CrustySession.h"
#import "CrustyWrapperProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@class CrustySession;

@interface CrustyWrapper : NSObject <CrustyWrapperProtocol>
@end

NS_ASSUME_NONNULL_END
