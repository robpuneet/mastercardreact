//
//  Crusty.h
//  HYPRADP
//
//  Created by Robert Panebianco on 7/30/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Crusty : NSObject

+(instancetype)sharedInstance;
-(instancetype)new NS_UNAVAILABLE;

-(BOOL)open:(NSData*)var1 var2:(NSMutableData*)var2 var3:(NSUInteger)var3 var4:(NSData*)var4 var5:(NSUInteger)var5 var6:(NSMutableData*)var6 var7:(NSUInteger)var7 error:(NSError *_Nullable*)error;
-(BOOL)close;
-(int32_t)send:(NSData*)var1 var2:(NSUInteger)var2 var3:(NSMutableData*)var3 var4:(NSUInteger)var4 var5:(NSMutableData*)var5 var6:(NSUInteger)var6 error:(NSError *_Nullable*)error;
@end

NS_ASSUME_NONNULL_END
