//
//  CommandBinding.h
//  HYPRADP
//
//  Created by Ethan Moon on 8/2/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CrustyWrapperProtocol.h"

@interface CommandBinding: NSObject
-(instancetype)initWithCrustyWrapper:(id<CrustyWrapperProtocol> _Nonnull) crusty;
-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;
-(BOOL)process:(NSError ** _Nullable)error;
@end
