//
//  TrustCommand.h
//  HYPRADP
//
//  Created by Ethan Moon on 8/2/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TrustCommandProtocol.h"

@class CommandBinding;

NS_ASSUME_NONNULL_BEGIN
@interface TrustCommand: NSObject <TrustCommandProtocol>
-(instancetype)initWithError:(NSError**_Nullable)error;
-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;
@end
NS_ASSUME_NONNULL_END
