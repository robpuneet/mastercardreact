//
//  CrustyContext.h
//  HYPR_One
//
//  Created by Robert Panebianco on 3/16/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, SecureStates) {
    ESTABLISH_SESSION,
    CERT_EXCHANGE,
    CLIENT_REG_VERIFY,
    SESSION_ESTABLISHED,
    SECURE_COMMUNICATION,
    SESSION_RESET,
    UNKNOWN,
};

@interface CrustyContext : BaseModel

+(BOOL)resetData:(CrustyContext*)ctx error:(NSError* _Nullable*)error;

-(NSString *)identifier;
-(int32_t)counter;
-(SecureStates)state;
-(SecKeyRef)taPublicKey;

-(void)setIdentifier:(NSString * _Nonnull)identifier;
-(void)setState:(SecureStates)state;
-(void)setCounter:(int32_t)counter;

-(void)incrementCounter;

-(void)storeSessionKey:(NSData*)keyBytes error:(NSError *_Nullable*)error;
-(void)storeHMACKey:(NSData*)keyBytes error:(NSError *_Nullable*)error;
-(void)generateClientCert:(NSError *_Nullable*)error;
-(SecKeyRef)getClientCert:(NSError *_Nullable*)error;
-(void)setTACert:(NSData*)taPublicKeyBytes error:(NSError *_Nullable*)error;
-(NSData*)clientSign:(NSData*_Nullable)ecdhePub error:(NSError *_Nullable*)error;
-(BOOL)taVerify:(NSData*)rawSignature error:(NSError *_Nullable*)error;
-(NSData*)retrieveSessionKey:(NSError *_Nullable*)error;
-(NSData*)retrieveHMACKey:(NSError *_Nullable*)error;
-(BOOL)verifyHMAC:(NSData*)data hmac:(NSData*)hmac error:(NSError *_Nullable*)error;
-(NSData *)registeredDecrypt:(NSData *)payload iv:(NSData*)iv error:(NSError *_Nullable*)error;
-(NSData *)registeredEncrypt:(NSData *)payload iv:(NSData*)iv error:(NSError *_Nullable*)error;

@end

NS_ASSUME_NONNULL_END
