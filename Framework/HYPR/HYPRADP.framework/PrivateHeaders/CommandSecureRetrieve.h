//
//  CommandSecureRetrieve.h
//  HYPRWBC
//
//  Created by Ethan Moon on 8/1/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CrustyWrapperProtocol.h"

@class CommandSecureSize;

@interface CommandSecureRetrieve: NSObject
-(instancetype)initWithCrustyWrapper:(id<CrustyWrapperProtocol> _Nonnull) crusty andAlias:(NSString* _Nonnull) alias;
-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;
-(NSData*)process:(NSError ** _Nullable)error;
// For testing purposes
-(void)injectCommandSecureSize:(CommandSecureSize*) commandSecureSize;
@end
