//
//  CommandRSAGenKey.h
//  HYPRADP
//
//  Created by Ethan Moon on 8/6/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/SecBase.h>
#import "ParamSpecRSA.h"
#import "CrustyWrapperProtocol.h"

@interface CommandRSAGenKey: NSObject
-(instancetype)initWithCrustyWrapper:(id<CrustyWrapperProtocol> _Nonnull) crusty andParamSpecRSA:(ParamSpecRSA* _Nonnull) paramSpecRSA;
-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;
-(SecKeyRef)process:(NSError ** _Nullable)error;
@end
