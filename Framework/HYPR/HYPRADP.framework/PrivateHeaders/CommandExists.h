//
//  CommandExists.h
//  HYPRADP
//
//  Created by Ethan Moon on 8/1/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CrustyWrapperProtocol.h"

@interface CommandExists: NSObject
-(instancetype)initWithCrustyWrapper:(id<CrustyWrapperProtocol> _Nonnull) crusty andAlias:(NSString* _Nonnull) alias;
-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;
-(BOOL)process:(NSError ** _Nullable)error;
@end
