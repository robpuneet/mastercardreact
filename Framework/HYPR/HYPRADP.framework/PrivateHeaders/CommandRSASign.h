//
//  CommandRSASign.h
//  HYPRADP
//
//  Created by Ethan Moon on 8/6/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CrustyWrapperProtocol.h"

@interface CommandRSASign: NSObject
-(instancetype)initWithCrustyWrapper:(id<CrustyWrapperProtocol> _Nonnull) crusty withAlias:(NSString* _Nonnull) alias andData:(NSData* _Nonnull) data;
-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;
-(NSData*)process:(NSError ** _Nullable)error;
@end
