//
//  CommandRSARetrieveKey.h
//  HYPRADP
//
//  Created by Ethan Moon on 8/3/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/SecBase.h>
#import "CrustyWrapperProtocol.h"

@interface CommandRSARetrieveKey: NSObject
-(instancetype)initWithCrustyWrapper:(id<CrustyWrapperProtocol> _Nonnull) crusty andAlias:(NSString* _Nonnull) alias;
-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;
-(SecKeyRef)process:(NSError ** _Nullable)error;
@end
