//
//  CommandECGenKey.h
//  HYPRADP
//
//  Created by Ethan Moon on 8/1/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/SecBase.h>
#import "ParamSpecEC.h"
#import "CrustyWrapperProtocol.h"

@interface CommandECGenKey: NSObject

-(instancetype)initWithCrustyWrapper:(id<CrustyWrapperProtocol> _Nonnull) crusty andParamSpecEC:(ParamSpecEC* _Nonnull) paramSpecEC;
-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;
-(SecKeyRef)process:(NSError ** _Nullable)error;
@end
