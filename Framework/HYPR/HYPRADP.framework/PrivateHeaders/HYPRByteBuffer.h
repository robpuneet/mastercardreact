//
//  HYPRByteBuffer.h
//  HYPRADP
//
//  Created by Robert Panebianco on 7/31/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HYPRByteBuffer : NSObject
-(instancetype)initWithWrap:(NSData*)data;
-(int32_t)getInt;
-(void)get:(NSMutableData*)buffer;
@end
