//
//  CrustyWrapperProtocol.h
//  HYPRADP
//
//  Created by Ethan Moon on 8/7/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CrustySession;
@class Crusty;
@class CrustyContext;

NS_ASSUME_NONNULL_BEGIN

@protocol CrustyWrapperProtocol <NSObject>

-(Crusty*)wrapped;
-(CrustyContext*)ctx;
-(CrustySession*)session;

-(void)registerClient:(NSError *_Nullable*)error;

-(void)reKey:(NSError *_Nullable*)error;

-(NSData *)secureSend:(NSData *)payload receiveBytes:(NSMutableData*)receiveBytes error:(NSError *_Nullable*)error;

-(NSData *)partialSecureSend:(NSData *)payload receiveBytes:(NSMutableData*)receiveBytes error:(NSError *_Nullable*)error;

-(BOOL)oOpen:(NSMutableData*)x xLen:(NSUInteger)xLen y:(NSData*)y yLen:(NSUInteger)yLen z:(NSMutableData*)z zLen:(NSUInteger)zLen error:(NSError *_Nullable*)error;

-(int32_t)oSend:(NSData*)x xLen:(NSUInteger)xLen y:(NSMutableData*)y yLen:(NSUInteger)yLen z:(NSMutableData*)z zLen:(NSUInteger)zLen error:(NSError *_Nullable*)error;

@end

NS_ASSUME_NONNULL_END
