//
//  HYPRWBCError.h
//  HYPRWBC
//
//  Created by Robert Panebianco on 7/17/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 @discussion Constants used by NSError differentiate between "domains" of error codes, used to identify error codes which originate from specific subsystems.
 @const HYPRWBCErrorDomain Indicates an error within HYPRWBC framework
 */
FOUNDATION_EXPORT NSErrorDomain const HYPRWBCErrorDomain;

/// Error codes describing conditions within the HYPRWBCErrorDomain
typedef NS_ERROR_ENUM(HYPRWBCErrorDomain, HYPRWBCErrorCode)
{
    HYPRWBCErrorGeneric = -0,
    
    HYPRWBCErrorCrustyContext = -100,
    HYPRWBCErrorCrustyContextFailedToStoreSessionKey = -101,
    HYPRWBCErrorCrustyContextFailedToStoreHMACKey = -102,
    HYPRWBCErrorCrustyContextFailedToGenerateClientCert = -103,
    HYPRWBCErrorCrustyContextFailedToImportTACert = -104,
    HYPRWBCErrorCrustyContextFailedToGetTACert = -105,
    HYPRWBCErrorCrustyContextFailedToGetClientCert = -106,
    HYPRWBCErrorCrustyContextFailedToSignWithClientCert = -107,
    HYPRWBCErrorCrustyContextNoTAPublicKeySet = -108,
    HYPRWBCErrorCrustyContextFailedToConvertRawSignatureToDER = -109,
    HYPRWBCErrorCrustyContextFailedToConvertDERSignatureToRaw = -110,
    HYPRWBCErrorCrustyContextFailedToVerifyTASignature = -111,
    HYPRWBCErrorCrustyContextFailedToGetSessionKey = -112,
    HYPRWBCErrorCrustyContextFailedToGetHMACKey = -113,
    HYPRWBCErrorCrustyContextFailedToEncryptPlaintext = -114,
    HYPRWBCErrorCrustyContextFailedToDecryptCiphertext = -115,
    HYPRWBCErrorCrustyContextFailedToComputeHMAC = -116,
    
    HYPRWBCErrorCrustySession = -200,
    HYPRWBCErrorCrustySessionSendRequestFailed = -201,
    HYPRWBCErrorCrustySessionReKeyingFailed = -202,
    HYPRWBCErrorCrustySessionSendRequestBadReturnCode = -203,
    HYPRWBCErrorCrustySessionDecryptReplyInvalidPayload = -204,
    HYPRWBCErrorCrustySessionRegisteredDecryptFailed = -205,
    HYPRWBCErrorCrustySessionValidateHMACInvalidPayload = -206,
    HYPRWBCErrorCrustySessionValidateHMACFailed = -207,
    
    HYPRWBCErrorCrustyWrapper = -300,
    HYPRWBCErrorCrustyWrapperSecureSendRequest = -301,
    HYPRWBCErrorCrustyWrapperSecureSendDecryptReply = -302,
    HYPRWBCErrorCrustyWrapperSecureSendDecryptReplyInvalidPayload = -303,
    HYPRWBCErrorCrustyWrapperSecureSendDecryptFailed = -304,
    HYPRWBCErrorCrustyWrapperPartialSecureSendRequest = -305,
    HYPRWBCErrorCrustyWrapperPartialSecureSendValidateHMAC = -306,
    HYPRWBCErrorCrustyWrapperPartialSecureSendValidateHMACInvalidPayload = -307,
    HYPRWBCErrorCrustyWrapperPartialSecureSendValidateHMACFailed = -308,
    HYPRWBCErrorCrustyWrapperRegisterClient = -309,
    HYPRWBCErrorCrustyWrapperReKey = -310,
    HYPRWBCErrorCrustyWrapperUnrecoverableError = -311,
    
    HYPRWBCErrorECDH = -400,
    HYPRWBCErrorECDHSendUnregistered = -401,
    HYPRWBCErrorECDHSendRegistered = -402,
    HYPRWBCErrorECDHBadReturnCode = -403,
    HYPRWBCErrorECDHStoreKey = -404,
    HYPRWBCErrorECDHSignatureInvalid = -405,
    HYPRWBCErrorECDHInvalidTAReply = -406,
    
    HYPRWBCErrorRegistration = -500,
    HYPRWBCErrorRegistrationProvideCredentials = -501,
    HYPRWBCErrorRegistrationParseTACredentialsBadReturnCode = -502,
    HYPRWBCErrorRegistrationVerificationBadReturnCode = -503,
    HYPRWBCErrorRegistrationInvalidBlob = -504,
    HYPRWBCErrorRegistrationInvalidTACredentials = -505,
    HYPRWBCErrorRegistrationVerification = -506,
    
    HYPRWBCErrorTrustCommand = -600,
    HYPRWBCErrorTrustCommandInvalidAliasParameter = -601,
    HYPRWBCErrorTrustCommandInvalidDataParameter = -602,
    HYPRWBCErrorTrustCommandInvalidParamSpecParameter = -603,
    
    HYPRWBCErrorCommandDelete = -700,
    HYPRWBCErrorCommandDeleteInvalidParameter = -701,
    HYPRWBCErrorCommandDeleteInvalidKeyExistence = -702,
    
    HYPRWBCErrorCommandBinding = -800,
    
    HYPRWBCErrorCommandECGenKey = -900,
    HYPRWBCErrorCommandECGenKeyInvalidParameter = -901,
    HYPRWBCErrorCommandECGenKeyKeyType = -902,
    HYPRWBCErrorCommandECGenKeyKeyLength = -903,
    
    HYPRWBCErrorCommandRSAGenKey = -1000,
    HYPRWBCErrorCommandRSAGenKeyInvalidParameter = -1001,
    HYPRWBCErrorCommandRSAGenKeyKeyType = -1002,
    
    HYPRWBCErrorCommandECRetrieveKey = -1100,
    HYPRWBCErrorCommandECRetrieveKeyInvalidParameter = -1101,
    HYPRWBCErrorCommandECRetrieveKeyKeyType = -1102,
    HYPRWBCErrorCommandECRetrieveKeyKeyLength = -1103,
    
    HYPRWBCErrorCommandRSARetrieveKey = -1200,
    HYPRWBCErrorCommandRSARetrieveKeyInvalidParameter = -1201,
    HYPRWBCErrorCommandRSARetrieveKeyKeyType = -1202,
    
    HYPRWBCErrorCommandECSign = -1300,
    HYPRWBCErrorCommandECSignInvalidAliasParameter = -1301,
    HYPRWBCErrorCommandECSignInvalidDataParameter = -1302,
    HYPRWBCErrorCommandECSignSigLength = -1303,
    
    HYPRWBCErrorCommandRSASign = -1400,
    HYPRWBCErrorCommandRSASignInvalidAliasParameter = -1401,
    HYPRWBCErrorCommandRSASignInvalidDataParameter = -1403,
    
    HYPRWBCErrorCommandExists = -1500,
    HYPRWBCErrorCommandExistsInvalidParameter = -1501,
    HYPRWBCErrorCommandExistsKeyExistence = -1502,
  
    HYPRWBCErrorCommandSecureRetrieve = -1600,
    HYPRWBCErrorCommandSecureRetrieveInvalidParameter = -1601,
    
    HYPRWBCErrorCommandSecureSize = -1700,
    HYPRWBCErrorCommandSecureSizeInvalidParameter = -1701,
    
    HYPRWBCErrorCommandSecureStore = -1800,
    HYPRWBCErrorCommandSecureStoreInvalidParameter = -1801,
    HYPRWBCErrorCommandSecureStoreDataNotStored = -1802,
    
    HYPRWBCErrorTrust = -1900,
    
    HYPRWBCErrorCrusty = -2000,
    
    HYPRWBCErrorCommandAttestation = -2100,
    HYPRWBCErrorCommandAttestationInvalidAlias = -2101,
    HYPRWBCErrorCommandAttestationInvalidData = -2102,
    HYPRWBCErrorCommandAttestationSigLength = -2103,
    HYPRWBCErrorCommandAttestationNoCertificateChain = -2104,
    
};

@interface HYPRWBCError : NSObject
/**
 Builder for HYPRWBCError objects (NSError with HYPRWBCErrorDomain)
 
 @param code The error code for this error
 @param error (Optional) any underlying error object
 @return NSError instance in the HYPRWBCErrorDomain
 */
+(NSError*)errorWithCode:(HYPRWBCErrorCode)code underlyingError:(NSError* _Nullable)error;

/**
 Builder for HYPRWBCError objects (NSError with HYPRWBCErrorDomain)
 
 @param code The HYPRWBCErrorCode for this error
 @param error (Optional) any underlying error object
 @param userInfo (Optional) additional User Info dictionary
 @return NSError instance in the HYPRWBCErrorDomain
 */
+(NSError*)errorWithCode:(HYPRWBCErrorCode)code underlyingError:(NSError* _Nullable)error userInfo:(NSDictionary* _Nullable)userInfo;
@end

@interface NSError (HYPRWBC)
-(NSError*)rootError;
@end

NS_ASSUME_NONNULL_END
