//
//  Trust.h
//  HYPRADP
//
//  Created by Robert Panebianco on 8/20/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TrustCommandProtocol;

/**
 Completion block returned from Trust class
 
 @param itf An initialized object conforming to the TrustCommandProtocol. `nil` is returned on error
 @param error `nil` if the operation was successful. NSError in the HYPRWBCErrorDomain otherwise. See HYPRWBCError.h for details.
 */
typedef void(^HYPRWBCTrustResult)(id<TrustCommandProtocol> itf, NSError * _Nullable error);

/**
 Contains a singleton that can be used to install/initialize the HYPRADP SDK
 */
@interface Trust : NSObject

+(instancetype)sharedInstance;
-(instancetype)new NS_UNAVAILABLE;

/**
 Completes the installation/initialization of the HYPRADP SDK

 @param completion Expects a HYPRWBCTrustResult
 
 Usage:
 
 [[Trust sharedInstance] install:^(id<TrustCommandProtocol> itf, NSError * _Nullable error) {
   if(itf == nil) {
     // Developer handles error
   } else {
     // TrustCommandProtocol is initialized and ready to be used.
     // See TrustCommandProtocol.h
   }
 }];
 */

-(void)install:(HYPRWBCTrustResult)completion;
@end
