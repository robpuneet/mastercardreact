//
//  CertificateAttestation.h
//  HYPRADP
//
//  Created by Robert Panebianco on 9/11/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Flags used to specify the properties that should be present in the returned CertificateAttesation object of the getAttestation method from the TrustCommandProtocol

 - CertificateAttestationAll: Return all available properties (see below)
 - CertificateAttestationSignature: Only return the digital signature
 - CertificateAttestationChain: Only return the certificate chain of the stored certificate
 */
typedef NS_ENUM(uint8_t, CertificateAttestationFlags) {
    CertificateAttestationAll = 0x00,
    CertificateAttestationSignature = 0x01,
    CertificateAttestationChain = 0x02
};

@interface CertificateAttestation : NSObject

- (instancetype)initWithSignature:(NSData *)signature andCertificateChain:(NSArray<NSData*>*)certificateChain;
+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;

@property (nonatomic, strong) NSData *signature;
@property (nonatomic, strong) NSArray<NSData*> *certificateChain;

@end
