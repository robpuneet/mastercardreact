//
//  HYPRADP.h
//  HYPRADP
//
//  Created by Robert Panebianco on 7/25/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HYPRADP.
FOUNDATION_EXPORT double HYPRADPVersionNumber;

//! Project version string for HYPRADP.
FOUNDATION_EXPORT const unsigned char HYPRADPVersionString[];

#pragma mark - API
#import <HYPRADP/TrustCommandProtocol.h>

#pragma mark - Parameters classes
#import <HYPRADP/ParamSpecProtocol.h>
#import <HYPRADP/ParamSpecEC.h>
#import <HYPRADP/ParamSpecRSA.h>

#pragma mark - Return types
#import <HYPRADP/CertificateAttestation.h>

#pragma mark - Utilities
#import <HYPRADP/HYPRWBCError.h>
#import <HYPRADP/HYPRWBCLogger.h>

#import <HYPRADP/Trust.h>


