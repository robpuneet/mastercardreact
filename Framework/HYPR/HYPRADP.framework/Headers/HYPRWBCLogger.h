//
//  HYPRWBCLogger.h
//  HyprFidoFramework
//
//  Created by Parita Shah on 6/26/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HYPRWBCLogger;

#ifndef HYPRWBCLogHelper
#define HYPRWBCLogHelper(_flag, _message) [HYPRWBCLogger logMessage:(_message) flag:(_flag) file:__FILE__ function:__PRETTY_FUNCTION__ line:__LINE__]
#endif

#ifndef HYPRWBCLog
#define HYPRWBCLog(format, ...) HYPRWBCLogDebug(format, ##__VA_ARGS__)
#endif

#ifndef HYPRWBCLogVerbose
#define HYPRWBCLogVerbose(format, ...) HYPRWBCLogHelper(HYPRWBCLogFlagVerbose, (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HYPRWBCLogDebug
#define HYPRWBCLogDebug(format, ...)   HYPRWBCLogHelper(HYPRWBCLogFlagDebug,   (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HYPRWBCLogInfo
#define HYPRWBCLogInfo(format, ...)    HYPRWBCLogHelper(HYPRWBCLogFlagInfo,    (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HYPRWBCLogWarning
#define HYPRWBCLogWarning(format, ...) HYPRWBCLogHelper(HYPRWBCLogFlagWarning, (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HYPRWBCLogError
#define HYPRWBCLogError(format, ...)   HYPRWBCLogHelper(HYPRWBCLogFlagError,   (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif


/**
 Log flags are specified for each log statement. They are filtered by the log level.

 - HYPRWBCLogFlagVerbose: Highly detailed log messages. May affect runtime.
 - HYPRWBCLogFlagDebug: Detailed logs only used while debugging.
 - HYPRWBCLogFlagInfo: General information about app state.
 - HYPRWBCLogFlagWarning: Indicates possible error.
 - HYPRWBCLogFlagError: A unexpected error occurred.
 */
typedef NS_ENUM(NSUInteger, HYPRWBCLogFlag) {
    HYPRWBCLogFlagVerbose = 0,
    HYPRWBCLogFlagDebug   = 1,
    HYPRWBCLogFlagInfo    = 2,
    HYPRWBCLogFlagWarning = 3,
    HYPRWBCLogFlagError   = 4,
};

/**
 Log level is used to filter log flags.

 - HYPRWBCLogLevelAll: Output all log messages. Equivalent to HYPRWBCLogLevelVerbose.
 - HYPRWBCLogLevelVerbose: Output verbose and greater log messages.
 - HYPRWBCLogLevelDebug: Output debug and greater log messages.
 - HYPRWBCLogLevelInfo: Output info and greater log messages.
 - HYPRWBCLogLevelWarning: Output warning and greater log messages.
 - HYPRWBCLogLevelError: Output error log messages only.
 - HYPRWBCLogLevelNone: Disable all log messages.
 */
typedef NS_ENUM(NSUInteger, HYPRWBCLogLevel) {
    HYPRWBCLogLevelAll     = 0,
    HYPRWBCLogLevelVerbose = (HYPRWBCLogFlagVerbose),
    HYPRWBCLogLevelDebug   = (HYPRWBCLogFlagDebug),
    HYPRWBCLogLevelInfo    = (HYPRWBCLogFlagInfo),
    HYPRWBCLogLevelWarning = (HYPRWBCLogFlagWarning),
    HYPRWBCLogLevelError   = (HYPRWBCLogFlagError),
    HYPRWBCLogLevelNone    = NSUIntegerMax
};


/**
 Logging utility used by all Hypr Frameworks
 
 Override the `minimumLogLevel` property to configure a specific desired log output.
 
 eg. to disable all log output:
 @code [HYPRWBCLogger setMinimumLogLevel: HYPRWBCLogLevelNone];
 */
@interface HYPRWBCLogger : NSObject


/**
 Primative logging function. Should use the macros instead.

 @param message block to be evalutated when printing the log statement
 @param flag HYPRWBCLogFlag value for this message
 @param file Path to the file where the log stamement was generated.
 @param function Function where the log statement was generated.
 @param line Line where the log statement was generated
 @note file, function and line parameters are only output in Debug builds of the frameworks.
 */
+ (void) logMessage:(NSString * (^)(void))message
               flag:(HYPRWBCLogFlag)flag
               file:(const char *)file
           function:(const char *)function
               line:(NSUInteger)line;

/// Default log level is HYPRWBCLogLevelDebug (Debug build) or HYPRWBCLogLevelWarning (Release build)
+ (HYPRWBCLogLevel) minimumLogLevel;

/**
 Override the default minimum log level.

 @param minimumLogLevel All log messages with a HYPRWBCLogFlag value equal to or greater than the minimumLogLevel are output to the console.
 */
+ (void) setMinimumLogLevel:(HYPRWBCLogLevel)minimumLogLevel;
@end
