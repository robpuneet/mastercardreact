//
//  ParamSpecEC.h
//  HYPRADP
//
//  Created by Ethan Moon on 8/2/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParamSpecProtocol.h"

// Constant used to specify the secp256r1 curve
FOUNDATION_EXPORT const int32_t EC_NIST_P256;

/**
 Describes an EC key to the generateKeyPair, getPublicKey, and sign TrustCommandProtocol methods. An alias and a curve can be specified. The curve parameter is only required for generateKeyPair. Currently, only the NIST P-256 curve is supported
 */
@interface ParamSpecEC: NSObject <ParamSpecProtocol>
-(instancetype)initWithAlias:(NSString* _Nonnull) alias andCurve:(int32_t) curve;
-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;

-(int32_t)getCurve;

@end
