//
//  TrustCommandProtocol.h
//  HYPRADP
//
//  Created by Robert Panebianco on 7/23/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/SecBase.h>
#import "ParamSpecProtocol.h"
#import "CertificateAttestation.h"

/**
 Exposes cryptographic operations and secure storage offered with the HYPRADP SDK when initialized
 */
@protocol TrustCommandProtocol <NSObject>

/**
 Stores a data item in secure storage
 
 @param alias The handle the data will be stored under
 @param data The data to be stored
 @param error HYPRWBCError.h
 @return The result of the store or false if an error is preseent
 */
-(BOOL)store:(NSString *)alias data:(NSData*)data error:(NSError *_Nullable*)error;

/**
 Retrieves a data item that was previously stored using the store command

 @param alias The handle that contains the desired data
 @param error HYPRWBCError.h
 @return The previously stored data or nil if an error is present
 */
-(NSData*)retrieve:(NSString *)alias error:(NSError *_Nullable*)error;

/**
 Generates a cryptographic keypair that is available through a ParamSpec object
 
 @param paramSpec Describes the type of key pair to generate
 @param error HYPRWBCError.h
 @return The public key or nil if an error is present
 */
-(SecKeyRef)generateKeyPair:(id <ParamSpecProtocol>)paramSpec error:(NSError *_Nullable*)error;

/**
 Returns the public portion of a key generated with the generateKey method
 
 @param paramSpec Describes the type of key to retrieve
 @param error HYPRWBCError.h
 @return The public key or nil if an error is present
 */
-(SecKeyRef)getPublicKey:(id <ParamSpecProtocol>)paramSpec error:(NSError *_Nullable*)error;

/**
 Creates a digital signature using the specified key.
 
 For EC keys this is ECDSAwithSHA256
 For RSA key this is PKCS#1 RSASSA-PSS with MGF1 and SHA256*
 
 *SHA256 is used for both the hashing and mask generator function
 
 @param message The data to be signed
 @param paramSpec Describes the type of key to be used for the signature. (alias only required)
 @param error HYPRWBCError.h
 @return A digital signature over the message with the specified key or nil if an error is present
 */
-(NSData*)sign:(NSData*)message keySpec:(id <ParamSpecProtocol>)paramSpec error:(NSError *_Nullable*)error;

/**
 Returns a cryptographic signature over data to attest the ownership of a certificate
 
 @param alias The handle of the stored certificate
 @param data The data to be signed
 @param flags Describes the properties that should be filled of the returned CertificateAttestation object
 @param error See HYPRWBCError.h
 @return A CertificateAttestation object or nil if an error is present
 */
-(CertificateAttestation*)getAttestation:(NSString *)alias forData:(NSData*)data forFlags:(CertificateAttestationFlags)flags error:(NSError *_Nullable*)error;

/**
 Deletes a data or key item

 @param alias The data or key handle
 @param error HYPRWBCError.h
 @return The result of the delete or false if an error is present
 */
-(BOOL)delete:(NSString*)alias error:(NSError *_Nullable*)error;

/**
 Queries existence of a data item or key

 @param alias The data or key handle
 @param error HYPRWBCError.h
 @return The existence of the item or false if an error is present
 */
-(BOOL)exists:(NSString*)alias error:(NSError *_Nullable*)error;

@end
