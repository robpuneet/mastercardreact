//
//  NSObject+BuildInformation.h
//  HyprCoreUAFClient
//
//  Created by Robert Carlsen on 1/26/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

@import Foundation;

@interface NSObject (BuildInformation)

/**
 Returns the bundle version from Info.plist

 @return String representation of CFBundleShortVersionString
 */
+(NSString*)bundleVersion;
-(NSString*)bundleVersion;


/**
 Returns a describing the build information

 This is a custom property encoded into Info.plist at build time.
 The format is: yyyy-mm-dd <git sha> <commit count>
 eg. 2018-01-26 9782897 (148)

 @return String representation of HYPRBuildInfoString. Returns nil if the custom key is not present in the Info dictionary.
 */
+(NSString*)buildInformation;
-(NSString*)buildInformation;
@end
