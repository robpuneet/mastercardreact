//
//  ParamSpecRSA.h
//  HYPRADP
//
//  Created by Ethan Moon on 8/2/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParamSpecProtocol.h"

// Constant used to specify a 2048-bit RSA key
FOUNDATION_EXPORT const int32_t RSA_KEYSIZE_2048;

/**
  Describes an RSA key to the generateKeyPair, getPublicKey, and sign TrustCommandProtocol methods. An alias and a key size can be specified. The key size parameter is only required for generateKeyPair. Currently, only 2048-bit keys are supported.
 */
@interface ParamSpecRSA: NSObject <ParamSpecProtocol>
-(instancetype)initWithAlias:(NSString* _Nonnull) alias andKeySize:(int) keySize;
-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;

@end
