//
//  ParamSpecProtocol.h
//  HYPRADP
//
//  Created by Ethan Moon on 8/2/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 This interface is used to generalize handles to the classes ParamSpecEC and ParamSpecRSA.
 */
@protocol ParamSpecProtocol <NSObject>
-(NSString*)getAlias;
-(BOOL)isValidKeyGen;
-(BOOL)isValidAlias;
@end
