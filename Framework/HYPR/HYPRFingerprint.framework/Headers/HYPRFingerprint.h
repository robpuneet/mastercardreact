//
//  HYPRFingerprintAuthenticator.h
//  HYPRFingerprintAuthenticator
//
//  Created by Robert Carlsen on 11/8/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HYPRFingerprintAuthenticator.
FOUNDATION_EXPORT double HYPRFingerprintAuthenticatorVersionNumber;

//! Project version string for HYPRFingerprintAuthenticator.
FOUNDATION_EXPORT const unsigned char HYPRFingerprintAuthenticatorVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HYPRFingerprint/PublicHeader.h>

#import <HYPRFingerprint/HYPRFingerprintAuthenticator.h>
#import <HYPRFingerprint/HYPRFingerprintAuthenticatorFBA.h>
#import <HYPRFingerprint/HYPRFingerprintAsm.h>
#import <HYPRFingerprint/HYPRFingerprintAuthenticatorViewConfiguration.h>

