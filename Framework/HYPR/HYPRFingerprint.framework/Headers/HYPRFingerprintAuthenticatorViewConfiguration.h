//
//  HYPRFingerprintAuthenticatorViewConfiguration.h
//  HyprCore
//
//  Created by Ievgen Kreshchenko on 1/24/19.
//  Copyright © 2019 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HYPRFingerprintAuthenticatorViewConfiguration : NSObject <NSCopying, NSCoding>

+ (instancetype _Nonnull )sharedInstance;

- (instancetype _Nonnull)init NS_UNAVAILABLE;
- (instancetype _Nonnull)new NS_UNAVAILABLE;

/**
 * Fingerprint authenticator prompt text
 */
@property (nonatomic, copy, nullable) NSString *authenticationPromptText;
/**
 * Biometry lockout enroll alert title
 */
@property (nonatomic, copy, nullable) NSString *biometryLockoutEnrollAlertTitle;
/**
 * Biometry lockout verify alert title
 */
@property (nonatomic, copy, nullable) NSString *biometryLockoutVerifyAlertTitle;
/**
 * Biometry lockout alert message
 */
@property (nonatomic, copy, nullable) NSString *biometryLockoutAlertMessage;
/**
 * Biometry lockout alert close button title
 */
@property (nonatomic, copy, nullable) NSString *biometryLockoutAlertCloseButtonTitle;

@end

