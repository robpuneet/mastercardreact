//
//  HYPRVoice.h
//  HYPRVoice
//
//  Created by Ethan Moon on 11/27/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HYPRVoice.
FOUNDATION_EXPORT double HYPRVoiceVersionNumber;

//! Project version string for HYPRVoice.
FOUNDATION_EXPORT const unsigned char HYPRVoiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HYPRVoice/PublicHeader.h>

#import <HYPRVoice/HYPRVoiceAuthenticator.h>
#import <HYPRVoice/HYPRVoiceAsm.h>
#import <HYPRVoice/HYPRVoiceAuthenticatorFBA.h>
#import <HYPRVoice/HYPRVoiceAuthenticatorViewConfiguration.h>
