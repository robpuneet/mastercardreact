//
//  HYPRVoiceAuthenticatorViewConfiguration.h
//  HYPRVoiceAuthenticatorViewConfiguration
//
//  Created by Ethan Moon on 2/14/19.
//  Copyright © 2019 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>
#import <UIKit/UIFont.h>
#import <UIKit/UIImage.h>
#import <UIKit/UIView.h>

@interface HYPRVoiceAuthenticatorViewConfiguration : NSObject <NSCopying, NSCoding>

+ (instancetype _Nonnull )sharedInstance;

- (instancetype _Nonnull)init NS_UNAVAILABLE;
- (instancetype _Nonnull)new NS_UNAVAILABLE;

/**
 * Voice authenticator background color
 */
@property (nonatomic, copy, nullable) UIColor *backgroundColor;
/**
 * Voice authenticator title
 */
@property (nonatomic, copy, nullable) NSString *titleText;
/**
 * Voice authenticator title color
 */
@property (nonatomic, copy, nullable) UIColor *titleTextColor;
/**
 * Voice authenticator title font
 */
@property (nonatomic, copy, nullable) UIFont *titleTextFont;
/**
 * Voice authenticator registration primary information text
 */
@property (nonatomic, copy, nullable) NSString *regPrimaryInformationText;
/**
 * Voice authenticator text color
 */
@property (nonatomic, copy, nullable) UIColor *textColor;
/**
 * Voice authenticator text Font
 */
@property (nonatomic, copy, nullable) UIFont *textFont;
/**
 * Voice authenticator registration passphrase text color
 */
@property (nonatomic, copy, nullable) UIColor *regPassphraseTextColor;
/**
 * Voice authenticator registration passphrase text font
 */
@property (nonatomic, copy, nullable) UIFont *regPassphraseTextFont;
/**
 * Voice authenticator registration secondary information text
 */
@property (nonatomic, copy, nullable) NSString *regSecondaryInformationText;
/**
 * Voice authenticator registration button text
 */
@property (nonatomic, copy, nullable) NSString *regButtonText;
/**
 * Voice authenticator registration button text color
 */
@property (nonatomic, copy, nullable) UIColor *regButtonTextColor;
/**
 * Voice authenticator registration button text font
 */
@property (nonatomic, copy, nullable) UIFont *regButtonTextFont;
/**
 * Voice authenticator registration button inactive color
 */
@property (nonatomic, copy, nullable) UIColor *regButtonInactiveColor;
/**
 * Voice authenticator registration button active color
 */
@property (nonatomic, copy, nullable) UIColor *regButtonActiveColor;
/**
 * Voice authenticator registration primary instruction text
 */
@property (nonatomic, copy, nullable) NSString *regPrimaryInstructionText;
/**
 * Voice authenticator registration secondary instruction text
 */
@property (nonatomic, copy, nullable) NSString *regSecondaryInstructionText;
/**
 * Voice authenticator authentication primary instruction text
 */
@property (nonatomic, copy, nullable) NSString *authPrimaryInstructionText;
/**
 * Voice authenticator authentication button text
 */
@property (nonatomic, copy, nullable) NSString *authHintButtonText;
/**
 * Voice authenticator authentication button text color
 */
@property (nonatomic, copy, nullable) UIColor *authHintButtonTextColor;
/**
 * Voice authenticator authentication button text font
 */
@property (nonatomic, copy, nullable) UIFont *authHintButtonTextFont;
/**
 * Voice authenticator authentication button active color
 */
@property (nonatomic, copy, nullable) UIColor *authHintButtonActiveColor;
/**
 * Voice authenticator sound bar color
 */
@property (nonatomic, copy, nullable) UIColor *soundBarColor;
/**
 * Voice authenticator success dot color
 */
@property (nonatomic, copy, nullable) UIColor *regDotSuccessColor;
/**
 * Voice authenticator unknown dot color
 */
@property (nonatomic, copy, nullable) UIColor *regDotUnknownColor;
/**
 * Voice authenticator border line color
 */
@property (nonatomic, copy, nullable) UIColor *borderLineColor;
/**
 * Voice authenticator overlay background color
 */
@property (nonatomic, copy, nullable) UIColor *overlayBackgroundColor;
/**
 * Voice authenticator passphrase font
 */
@property (nonatomic, copy, nullable) UIFont *overlayPassphraseFont;
/**
 * Voice authenticator passphrase color
 */
@property (nonatomic, copy, nullable) UIColor *overlayPassphraseColor;
/**
 * Voice authenticator overlay title text
 */
@property (nonatomic, copy, nullable) NSString *overlayTitleText;
/**
 * Voice authenticator overlay title font
 */
@property (nonatomic, copy, nullable) UIFont *overlayTitleFont;
/**
 * Voice authenticator overlay title color
 */
@property (nonatomic, copy, nullable) UIColor *overlayTitleColor;
/**
 * Voice authenticator cancel button color
 */
@property (nonatomic, copy, nullable) UIColor *cancelButtonColor;
/**
 * Hide voice hint textfield. By default, voice hint textfield is shown
 */
@property (nonatomic) BOOL isVoiceHintTextfieldHidden;
/**
 * Voice authenticator voice icon image
 */
@property (nonatomic, copy, nullable) UIImage *voiceIconImage;
/**
 * Voice authenticator voice icon image content mode
 */
@property (nonatomic) UIViewContentMode voiceIconContentMode;
/**
 * Hide the animated Voice Sound Waves
 */
@property (nonatomic) BOOL isVoiceSoundWaveHidden;

- (instancetype)initWithBackgroundColor:(nullable UIColor *)backgroundColor titleText:(nullable NSString *)titleText titleTextColor:(nullable UIColor *)titleTextColor titleTextFont:(nullable UIFont *)titleTextFont regPrimaryInformationText:(nullable NSString *)regPrimaryInformationText textColor:(nullable UIColor *)textColor textFont:(nullable UIFont *)textFont regPassphraseTextColor:(nullable UIColor *)regPassphraseTextColor regPassphraseTextFont:(nullable UIFont *)regPassphraseTextFont regSecondaryInformationText:(nullable NSString *)regSecondaryInformationText regButtonText:(nullable NSString *)regButtonText regButtonTextColor:(nullable UIColor *)regButtonTextColor regButtonTextFont:(nullable UIFont *)regButtonTextFont regButtonInactiveColor:(nullable UIColor *)regButtonInactiveColor regButtonActiveColor:(nullable UIColor *)regButtonActiveColor regPrimaryInstructionText:(nullable NSString *)regPrimaryInstructionText regSecondaryInstructionText:(nullable NSString *)regSecondaryInstructionText authPrimaryInstructionText:(nullable NSString *)authPrimaryInstructionText authHintButtonText:(nullable NSString *)authHintButtonText authHintButtonTextColor:(nullable UIColor *)authHintButtonTextColor authHintButtonTextFont:(nullable UIFont *)authHintButtonTextFont authHintButtonActiveColor:(nullable UIColor *)authHintButtonActiveColor soundBarColor:(nullable UIColor *)soundBarColor regDotSuccessColor:(nullable UIColor *)regDotSuccessColor regDotUnknownColor:(nullable UIColor *)regDotUnknownColor borderLineColor:(nullable UIColor *)borderLineColor overlayBackgroundColor:(nullable UIColor *)overlayBackgroundColor overlayPassphraseFont:(nullable UIFont *)overlayPassphraseFont overlayPassphraseColor:(nullable UIColor *)overlayPassphraseColor overlayTitleText:(nullable NSString *)overlayTitleText overlayTitleFont:(nullable UIFont *)overlayTitleFont overlayTitleColor:(nullable UIColor *)overlayTitleColor cancelButtonColor:(nullable UIColor *)cancelButtonColor isVoiceHintTextfieldHidden:(BOOL)isVoiceHintTextfieldHidden voiceIconImage:(nullable UIImage *)voiceIconImage voiceIconContentMode:(UIViewContentMode)voiceIconContentMode isVoiceSoundWaveHidden:(BOOL)isVoiceSoundWaveHidden;

@end

