//
//  HYPRVoiceAuthenticatorFBA.h
//  HyprCore
//
//  Created by Ethan Moon on 12/3/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HyprAuthenticatorProtocol.h>
#import <HyprCore/HYPRAuthenticatorCustomizationProtocol.h>

@interface HYPRVoiceAuthenticatorFBA : NSObject <HyprAuthenticatorProtocol, HyprAuthenticatorUIProtocol, HYPRAuthenticatorCustomizationProtocol>
/*
 Sets timeout for the hint overlay screen, after which the screen will be dismissed.
 @param timeout specified in miliseconds
 */
+(void)setOverlayTimeout:(long)timeout;
@end
