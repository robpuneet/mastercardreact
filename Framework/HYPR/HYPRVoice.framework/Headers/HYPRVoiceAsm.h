//
//  HYPRVoiceAsm.h
//  HYPRVoice
//
//  Created by Ethan Moon on 11/28/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HyprCore/HyprAsmProtocol.h>
#import <HyprCore/ASMManager.h>
#import <HyprCore/HYPRAuthenticatorCustomizationProtocol.h>

@interface HYPRVoiceAsm : ASMManager <HyprAsmProtocol, HYPRAuthenticatorCustomizationProtocol>
/*
 Sets timeout for the hint overlay screen, after which the screen will be dismissed.
 @param timeout specified in miliseconds
 */
+(void)setOverlayTimeout:(long)timeout;
@end
