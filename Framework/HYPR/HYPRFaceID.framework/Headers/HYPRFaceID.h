//
//  HYPRFaceID.h
//  HYPRFaceID
//
//  Created by Robert Panebianco on 2/7/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HYPRFaceID.
FOUNDATION_EXPORT double HYPRFaceIDVersionNumber;

//! Project version string for HYPRFaceID.
FOUNDATION_EXPORT const unsigned char HYPRFaceIDVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HYPRFaceID/PublicHeader.h>

#import <HYPRFaceID/HYPRFaceIDAuthenticator.h>
#import <HYPRFaceID/HYPRFaceIDAuthenticatorFBA.h>
#import <HYPRFaceID/HYPRFaceIDAsm.h>
#import <HYPRFaceID/HYPRFaceIDAuthenticatorViewConfiguration.h>
