//
//  HYPRFaceIDAsm.h
//  HYPRFaceID
//
//  Created by Robert Panebianco on 2/7/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HyprAsmProtocol.h>
#import <HyprCore/ASMManager.h>

@interface HYPRFaceIDAsm : ASMManager <HyprAsmProtocol>

@end
