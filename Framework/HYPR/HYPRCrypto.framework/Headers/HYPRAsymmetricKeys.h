//
//  HYPRAsymmetricKeys.h
//  TrustedDevice
//
//  Created by Robert Panebianco on 7/13/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYPRAsymmetricKeys : NSObject

+(instancetype)factory;

-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;

-(BOOL)generateECKeyFor256BitCurve:(NSString *)alias error:(NSError*_Nullable*)error;

-(SecKeyRef)getECKey:(NSString *)alias error:(NSError*_Nullable*)error;

-(BOOL)importECPublicKey:(NSData *)publicKeyBytes keySizeInBits:(NSNumber*)keySizeInBits alias:(NSString *)alias error:(NSError*_Nullable*)error;

-(SecKeyRef)getImportedECPublicKey:(NSString *)alias error:(NSError*_Nullable*)error;

-(NSData*)getECPublicKeyBytes:(SecKeyRef)key error:(NSError*_Nullable*)error;

-(void)deleteAllAsymmetricKeys;
-(void)deleteAsymmetricKey:(NSString *)alias;

@end

NS_ASSUME_NONNULL_END
