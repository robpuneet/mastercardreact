//
//  AES.h
//  TrustedDevice
//
//  Created by Robert Panebianco on 7/13/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYPRAES : NSObject
+(NSData *)encryptCBCNoPadding:(NSData *)plaintext key:(NSData*)key iv:(NSData*)iv error:(NSError*_Nullable*)error;
+(NSData *)decryptCBCNoPadding:(NSData *)ciphertext key:(NSData*)key iv:(NSData*)iv error:(NSError*_Nullable*)error;
+(size_t) sizeRequiredForNoPaddingOption:(size_t)inputLen;
+(NSData *)generateRandomAESIv;
@end

NS_ASSUME_NONNULL_END
