//
//  HYPRHMAC.h
//  TrustedDevice
//
//  Created by Robert Panebianco on 7/16/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYPRHMAC : NSObject
+(NSData*)calculateHMACWithSHA256:(NSData*)message key:(NSData*)key error:(NSError*_Nullable*)error;
@end

NS_ASSUME_NONNULL_END
