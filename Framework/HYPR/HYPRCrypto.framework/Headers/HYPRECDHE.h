//
//  ECDHE.h
//  TrustedDevice
//
//  Created by Robert Panebianco on 7/13/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYPRECDHE : NSObject
-(BOOL)generate256BitEphemeralKey:(NSError*_Nullable*)error;
-(SecKeyRef)getEphemeralPublicKey:(NSError*_Nullable*)error;
-(NSData*)getSHA256SharedSecretFrom256BitParticipantKey:(SecKeyRef)participantPublicKey error:(NSError*_Nullable*)error;
-(void)destroy;
@end

NS_ASSUME_NONNULL_END
