//
//  HYPRASN1.h
//  HYPRCrypto
//
//  Created by Robert Panebianco on 7/24/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYPRASN1 : NSObject
+(NSData*)derEncodedECDSAWithSHA256SignatureToRaw:(NSData*)signature error:(NSError*_Nullable*)error;
+(NSData*)rawECDSAWithSHA256SignatureToDEREncoding:(NSData*)signature error:(NSError*_Nullable*)error;
@end

NS_ASSUME_NONNULL_END
