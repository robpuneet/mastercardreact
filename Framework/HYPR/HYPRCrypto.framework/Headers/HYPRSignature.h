//
//  Signature.h
//  TrustedDevice
//
//  Created by Robert Panebianco on 7/13/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYPRSignature : NSObject
+ (NSData*)signECDSAWithSHA256:(NSData*)message privateKey:(SecKeyRef)privateKey error:(NSError*_Nullable*)error;
+ (BOOL)verifyECDSAWithSHA256:(NSData*)signature message:(NSData*)message publicKey:(SecKeyRef)publicKey error:(NSError*_Nullable*)error;
@end

NS_ASSUME_NONNULL_END
