//
//  HYPRSHA.h
//  HYPRCrypto
//
//  Created by Robert Panebianco on 7/31/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYPRSHA2 : NSObject
+(NSData *)SHA224:(NSData *)data error:(NSError*_Nullable*)error;
+(NSData *)SHA256:(NSData *)data error:(NSError*_Nullable*)error;
+(NSData *)SHA384:(NSData *)data error:(NSError*_Nullable*)error;
+(NSData *)SHA512:(NSData *)data error:(NSError*_Nullable*)error;
@end

NS_ASSUME_NONNULL_END
