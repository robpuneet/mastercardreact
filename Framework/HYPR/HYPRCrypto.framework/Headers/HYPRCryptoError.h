//
//  HYPRCryptoError.h
//  HYPRCrypto
//
//  Created by Robert Panebianco on 7/17/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 @discussion Constants used by NSError differentiate between "domains" of error codes, used to identify error codes which originate from specific subsystems.
 @const HYPRCryptoErrorDomain Indicates an error within HYPRCrypto framework
 */
FOUNDATION_EXPORT NSErrorDomain const HYPRCryptoErrorDomain;

/// Error codes describing conditions within the HYPRCryptoErrorDomain
typedef NS_ERROR_ENUM(HYPRCryptoErrorDomain, HYPRCryptoErrorCode)
{
    HYPRCryptoErrorGeneric = -0,
    
    HYPRCryptoErrorAES = -100,
    HYPRCryptoErrorAESInvalidIv = -101,
    HYPRCryptoErrorAESInvalidMode = -102,
    HYPRCryptoErrorAESCBCNoPaddingInvalidCiphertextLength = -103,
    HYPRCryptoErrorAESInvalidPlaintext = -104,
    HYPRCryptoErrorAESInvalidCiphertext = -105,
    HYPRCryptoErrorAESInvalidCipherInput = -106,
    HYPRCryptoErrorAESInvalidKey = -107,
    
    HYPRCryptoErrorAsymmetricKeys = -200,
    HYPRCryptoErrorAsymmetricKeysDataProvider = -201,
    HYPRCryptoErrorAsymmetricKeysCopyPublicKey = -202,
    HYPRCryptoErrorAsymmetricKeysInvalidAlias = -203,
    HYPRCryptoErrorAsymmetricKeysInvalidPrivateKey = -204,
    HYPRCryptoErrorAsymmetricKeysInvalidKeySize = -205,
    HYPRCryptoErrorAsymmetricKeysInvalidPublicKey = -206,

    HYPRCryptoErrorSignature = -300,
    HYPRCryptoErrorSignatureSignNotSupported = -301,
    HYPRCryptoErrorSignatureVerifyNotSupported = -302,
    HYPRCryptoErrorSignatureInvalidMessage = -303,
    HYPRCryptoErrorSignatureInvalidSignature = -304,
    HYPRCryptoErrorSignatureInvalidKey = -305,
    
    HYPRCryptoErrorHMAC = -400,
    HYPRCryptoErrorHMACInvalidMessage = -401,
    HYPRCryptoErrorHMACInvalidKey = -402,
    HYPRCryptoErrorHMACInvalidAlgorithm = -403,
    
    HYPRCryptoErrorECDHE = -500,
    HYPRCryptoErrorECDHENoEphemeralKey = -501,
    HYPRCryptoErrorECDHECopyPublicKey = -502,
    HYPRCryptoErrorECDHEInvalidEphemeralKeySize = -503,
    HYPRCryptoErrorECDHEInvalidParticipantKeySize = -504,
    HYPRCryptoErrorECDHEKeyExchangeNotSupported = -505,
    HYPRCryptoErrorECDHEVerifyNotSupported = -506,
    HYPRCryptoErrorECDHEInvalidParticipantKey = -507,
    HYPRCryptoErrorECDHEKeySizeMismatch = -508,
    
    HYPRCryptoErrorSymmetricKeys = -600,
    HYPRCryptoErrorSymmetricKeysInvalidAlias = -601,
    HYPRCryptoErrorSymmetricKeysInvalidKey = -602,
    HYPRCryptoErrorSymmetricKeysDataProvider = -603,
    
    HYPRCryptoErrorASN1 = -700,
    HYPRCryptoErrorASN1InvalidSequenceLength = -701,
    HYPRCryptoErrorASN1NotASequence = -702,
    HYPRCryptoErrorASN1InvalidSignatureLength = -703,
    HYPRCryptoErrorASN1InvalidIntegerLength = -704,
    HYPRCryptoErrorASN1NotAInteger = -705,
    HYPRCryptoErrorASN1InvalidRawSignatureLength = -706,
    
    HYPRCryptoErrorSHA2 = -800,
    HYPRCryptoErrorSHA2UnsupportedDigest = -801,
    HYPRCryptoErrorSHA2InvalidInput = -802,
    HYPRCryptoErrorSHA2FailedToComputeDigest = -803,
    
    
    
};

@interface HYPRCryptoError : NSObject
/**
 Builder for HYPRCryptoError objects (NSError with HYPRCryptoErrorDomain)
 
 @param code The error code for this error
 @param error (Optional) any underlying error object
 @return NSError instance in the HYPRCryptoErrorDomain
 */
+(NSError*)errorWithCode:(HYPRCryptoErrorCode)code underlyingError:(NSError* _Nullable)error;

/**
 Builder for HYPRCryptoError objects (NSError with HYPRCryptoErrorDomain)
 
 @param code The HYPRCryptoErrorCode for this error
 @param error (Optional) any underlying error object
 @param userInfo (Optional) additional User Info dictionary
 @return NSError instance in the HYPRCryptoErrorDomain
 */
+(NSError*)errorWithCode:(HYPRCryptoErrorCode)code underlyingError:(NSError* _Nullable)error userInfo:(NSDictionary* _Nullable)userInfo;
@end

@interface NSError (HYPRCrypto)
-(NSError*)rootError;
@end

NS_ASSUME_NONNULL_END
