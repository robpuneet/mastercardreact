//
//  HYPRSymmetricKeys.h
//  HYPRCrypto
//
//  Created by Robert Panebianco on 7/24/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYPRSymmetricKeys : NSObject

+(instancetype)factory;

-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;

-(BOOL)saveGenericKey:(NSData *)keyBytes alias:(NSString *)alias error:(NSError*_Nullable*)error;
-(NSData*)getGenericKey:(NSString *)alias error:(NSError*_Nullable*)error;

-(void)deleteAllSymmetricKeys;
-(void)deleteSymmetricKey:(NSString *)alias;

@end

NS_ASSUME_NONNULL_END
