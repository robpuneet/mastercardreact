//
//  HYPRCryptoLogger.h
//  HyprFidoFramework
//
//  Created by Parita Shah on 6/26/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HYPRCryptoLogger;

#ifndef HYPRCryptoLogHelper
#define HYPRCryptoLogHelper(_flag, _message) [HYPRCryptoLogger logMessage:(_message) flag:(_flag) file:__FILE__ function:__PRETTY_FUNCTION__ line:__LINE__]
#endif

#ifndef HYPRCryptoLog
#define HYPRCryptoLog(format, ...) HYPRCryptoLogDebug(format, ##__VA_ARGS__)
#endif

#ifndef HYPRCryptoLogVerbose
#define HYPRCryptoLogVerbose(format, ...) HYPRCryptoLogHelper(HYPRCryptoLogFlagVerbose, (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HYPRCryptoLogDebug
#define HYPRCryptoLogDebug(format, ...)   HYPRCryptoLogHelper(HYPRCryptoLogFlagDebug,   (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HYPRCryptoLogInfo
#define HYPRCryptoLogInfo(format, ...)    HYPRCryptoLogHelper(HYPRCryptoLogFlagInfo,    (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HYPRCryptoLogWarning
#define HYPRCryptoLogWarning(format, ...) HYPRCryptoLogHelper(HYPRCryptoLogFlagWarning, (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HYPRCryptoLogError
#define HYPRCryptoLogError(format, ...)   HYPRCryptoLogHelper(HYPRCryptoLogFlagError,   (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif


/**
 Log flags are specified for each log statement. They are filtered by the log level.

 - HYPRCryptoLogFlagVerbose: Highly detailed log messages. May affect runtime.
 - HYPRCryptoLogFlagDebug: Detailed logs only used while debugging.
 - HYPRCryptoLogFlagInfo: General information about app state.
 - HYPRCryptoLogFlagWarning: Indicates possible error.
 - HYPRCryptoLogFlagError: A unexpected error occurred.
 */
typedef NS_ENUM(NSUInteger, HYPRCryptoLogFlag) {
    HYPRCryptoLogFlagVerbose = 0,
    HYPRCryptoLogFlagDebug   = 1,
    HYPRCryptoLogFlagInfo    = 2,
    HYPRCryptoLogFlagWarning = 3,
    HYPRCryptoLogFlagError   = 4,
};

/**
 Log level is used to filter log flags.

 - HYPRCryptoLogLevelAll: Output all log messages. Equivalent to HYPRCryptoLogLevelVerbose.
 - HYPRCryptoLogLevelVerbose: Output verbose and greater log messages.
 - HYPRCryptoLogLevelDebug: Output debug and greater log messages.
 - HYPRCryptoLogLevelInfo: Output info and greater log messages.
 - HYPRCryptoLogLevelWarning: Output warning and greater log messages.
 - HYPRCryptoLogLevelError: Output error log messages only.
 - HYPRCryptoLogLevelNone: Disable all log messages.
 */
typedef NS_ENUM(NSUInteger, HYPRCryptoLogLevel) {
    HYPRCryptoLogLevelAll     = 0,
    HYPRCryptoLogLevelVerbose = (HYPRCryptoLogFlagVerbose),
    HYPRCryptoLogLevelDebug   = (HYPRCryptoLogFlagDebug),
    HYPRCryptoLogLevelInfo    = (HYPRCryptoLogFlagInfo),
    HYPRCryptoLogLevelWarning = (HYPRCryptoLogFlagWarning),
    HYPRCryptoLogLevelError   = (HYPRCryptoLogFlagError),
    HYPRCryptoLogLevelNone    = NSUIntegerMax
};


/**
 Logging utility used by all Hypr Frameworks
 
 Override the `minimumLogLevel` property to configure a specific desired log output.
 
 eg. to disable all log output:
 @code [HYPRCryptoLogger setMinimumLogLevel: HYPRCryptoLogLevelNone];
 */
@interface HYPRCryptoLogger : NSObject


/**
 Primative logging function. Should use the macros instead.

 @param message block to be evalutated when printing the log statement
 @param flag HYPRCryptoLogFlag value for this message
 @param file Path to the file where the log stamement was generated.
 @param function Function where the log statement was generated.
 @param line Line where the log statement was generated
 @note file, function and line parameters are only output in Debug builds of the frameworks.
 */
+ (void) logMessage:(NSString * (^)(void))message
               flag:(HYPRCryptoLogFlag)flag
               file:(const char *)file
           function:(const char *)function
               line:(NSUInteger)line;

/// Default log level is HYPRCryptoLogLevelDebug (Debug build) or HYPRCryptoLogLevelWarning (Release build)
+ (HYPRCryptoLogLevel) minimumLogLevel;

/**
 Override the default minimum log level.

 @param minimumLogLevel All log messages with a HYPRCryptoLogFlag value equal to or greater than the minimumLogLevel are output to the console.
 */
+ (void) setMinimumLogLevel:(HYPRCryptoLogLevel)minimumLogLevel;
@end
