//
//  HYPRPINAuthenticatorCustomizationProtocol.h
//  HyprCore
//
//  Created by Edward Poon on 8/22/19.
//  Copyright © 2019 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol HYPRPINAuthenticatorCustomizationProtocol <NSObject>
/*
 Sets the invalid PIN Regex, any PIN that matches this regex will not
 be accepted during enrollment
 @param invalidPINRegex
 */
+(void)setInvalidPINRegEx:(NSRegularExpression*)invalidPINRegex;
/*
 Sets the invalid PIN alert title for the alert controller
 when an invalid PIN (based off the regex) is inputted
 @param invalidPINAlertTitle
 */
+(void)setInvalidPINAlertTitle:(NSString*)invalidPINAlertTitle;
/*
 Sets the invalid PIN alert message for the alert controller
 when an invalid PIN (based off the regex) is inputted
 @param invalidPINAlertMessage
 */
+(void)setInvalidPINAlertMessage:(NSString*)invalidPINAlertMessage;
@end

NS_ASSUME_NONNULL_END
