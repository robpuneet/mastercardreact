//
//  HYPRPINAsm.h
//  HyprCore
//
//  Created by Parita Shah on 1/26/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HyprAsmProtocol.h>
#import <HyprCore/ASMManager.h>

@interface HYPRPINAsm : ASMManager <HyprAsmProtocol>

@end
