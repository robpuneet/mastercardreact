//
//  HYPRPINAuthenticator.h
//  HyprCore
//
//  Created by Parita Shah on 1/26/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HyprAuthenticatorProtocol.h>
#import "HYPRPINAuthenticatorCustomizationProtocol.h"

@interface HYPRPINAuthenticator : NSObject <HyprAuthenticatorProtocol, HyprAuthenticatorUIProtocol, HYPRPINAuthenticatorCustomizationProtocol>

@end
