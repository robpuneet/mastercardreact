//
//  HYPRPIN.h
//  HYPRPIN
//
//  Created by Parita Shah on 1/26/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HYPRPIN.
FOUNDATION_EXPORT double HYPRPINVersionNumber;

//! Project version string for HYPRPIN.
FOUNDATION_EXPORT const unsigned char HYPRPINVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HYPRPIN/PublicHeader.h>

#import <HYPRPIN/HYPRPINAuthenticator.h>
#import <HYPRPIN/HYPRPINAuthenticatorFBA.h>
#import <HYPRPIN/HYPRPINAsm.h>
#import <HYPRPIN/HYPRPinAuthenticatorViewConfiguration.h>
#import <HYPRPIN/HYPRPINAuthenticatorCustomizationProtocol.h>
