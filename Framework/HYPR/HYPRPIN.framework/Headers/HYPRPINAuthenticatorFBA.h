//
//  HYPRPINAuthenticatorFBA.h
//  HyprCore
//
//  Created by Ievgen Kreshchenko on 8/30/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HyprAuthenticatorProtocol.h>
#import "HYPRPINAuthenticatorCustomizationProtocol.h"

@interface HYPRPINAuthenticatorFBA : NSObject <HyprAuthenticatorProtocol, HyprAuthenticatorUIProtocol, HYPRPINAuthenticatorCustomizationProtocol>

@end
