//
//  HYPRPINAuthenticatorViewConfiguration.h
//  HYPRPINAuthenticatorViewConfiguration
//
//  Created by Ethan Moon on 2/14/19.
//  Copyright © 2019 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>
#import <UIKit/UIFont.h>

@interface HYPRPINAuthenticatorViewConfiguration : NSObject <NSCopying, NSCoding>

+ (instancetype _Nonnull )sharedInstance;

- (instancetype _Nonnull)init NS_UNAVAILABLE;
- (instancetype _Nonnull)new NS_UNAVAILABLE;

/**
 * PIN authenticator top background color
 */
@property (nonatomic, copy, nullable) UIColor *topBackgroundColor;
/**
 * PIN authenticator bottom background color
 */
@property (nonatomic, copy, nullable) UIColor *bottomBackgroundColor;
/**
 * PIN authenticator enrollment title text
 */
@property (nonatomic, copy, nullable) NSString *enrollmentTitleText;
/**
 * PIN authenticator verification title text
 */
@property (nonatomic, copy, nullable) NSString *verificationTitleText;
/**
 * PIN authenticator enrollment title text font
 */
@property (nonatomic, copy, nullable) UIFont *titleTextFont;
/**
 * PIN authenticator enrollment title text color
 */
@property (nonatomic, copy, nullable) UIColor *titleColor;
/**
 * PIN authenticator enrollment top instructions text
 */
@property (nonatomic, copy, nullable) NSString *enrollmentTopInstructionsText;
/**
 * PIN authenticator enrollment bottom instructions text
 */
@property (nonatomic, copy, nullable) NSString *enrollmentBottomInstructionsText;
/**
 * PIN authenticator instructions text font
 */
@property (nonatomic, copy, nullable) UIFont *instructionsTextFont;
/**
 * PIN authenticator instructions text color
 */
@property (nonatomic, copy, nullable) UIColor *instructionsTextColor;
/**
 * PIN authenticator confirm button text
 */
@property (nonatomic, copy, nullable) NSString *confirmButtonText;
/**
 * PIN authenticator button color
 */
@property (nonatomic, copy, nullable) UIColor *buttonColor;
/**
 * PIN authenticator button text font
 */
@property (nonatomic, copy, nullable) UIFont *buttonTextFont;
/**
 * PIN authenticator input field and underline color
 */
@property (nonatomic, copy, nullable) UIColor *pinInputColor;

@end

