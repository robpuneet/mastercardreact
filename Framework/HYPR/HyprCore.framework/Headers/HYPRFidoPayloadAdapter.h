//
//  HYPRFidoPayloadAdapter.h
//  HyprCore
//
//  Created by Edward Poon on 4/22/19.
//  Copyright © 2019 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class HYPRSession;
@interface HYPRFidoPayloadAdapter : NSObject
/**
 * Returns an NSURLRequest that contains the UAF registration request from the HYPR FIDO Server
 * The UAF registration request contains the policy + challenge among other things for
 * the specified userName, rpUrl, rpAppID, and regActionId
 * @param username - FIDO Username
 * @param rpUrl - HYPR Relying Party URL
 * @param rpAppId - HYPR Relying Party Application ID
 * @param actionId - Policy name used for registration
 * Typically used in conjunction with Fido Client Adapter (FCA) integration when using a HYPR Fido Server
 * but not using the HYPR Relying Party Client (which does server communication).
 */
-(NSURLRequest*)registerGetRequestWithUsername:(NSString*)username rpUrl:(NSString*)rpUrl rpAppId:(NSString*)rpAppId regActionId:(NSString*)actionId;
/**
 * Returns an NSURLRequest that contains the UAF registration response for the HYPR FIDO Server
 * The UAF registration response contains the assertion among other things
 * @param responseString - The UAF Registration Response from the UAF Client
 * @param rpUrl - HYPR Relying Party URL
 * Typically used in conjunction with Fido Client Adapter (FCA) integration when using a HYPR Fido Server
 * but not using the HYPR Relying Party Client (which does server communication).
 */
-(NSURLRequest* _Nullable)registerSendRequestWithUafClientResponseString:(NSString*)responseString rpUrl:(NSString*)rpUrl;
/**
 * Returns an NSURLRequest that contains the UAF authentication request from the HYPR FIDO Server
 * The UAF authentication request contains the policy + challenge among other things for
 * the specified userName, rpUrl, rpAppID, and authActionId
 * @param username - FIDO Username
 * @param rpUrl - HYPR Relying Party URL
 * @param rpAppId - HYPR Relying Party Application ID
 * @param actionId - Policy name used for registration
 * Typically used in conjunction with Fido Client Adapter (FCA) integration when using a HYPR Fido Server
 * but not using the HYPR Relying Party Client (which does server communication).
 */
-(NSURLRequest*)authenticateGetRequestWithUsername:(NSString*)username rpUrl:(NSString*)rpUrl rpAppId:(NSString*)rpAppId authActionId:(NSString*)actionId;
/**
 * Returns an NSURLRequest that contains the UAF authentication response for the HYPR FIDO Server
 * The UAF authentication response contains the assertion among other things
 * @param responseString - The UAF Authentication Response from the UAF Client
 * @param rpUrl - HYPR Relying Party URL
 * Typically used in conjunction with Fido Client Adapter (FCA) integration when using a HYPR Fido Server
 * but not using the HYPR Relying Party Client (which does server communication).
 */
-(NSURLRequest*)authenticateSendRequestWithUafClientResponseString:(NSString*)responseString rpUrl:(NSString*)rpUrl;
/**
 * Returns an NSURLRequest that contains the UAF deregistration request from the HYPR FIDO Server
 * @param username - FIDO Username
 * @param rpUrl - HYPR Relying Party URL
 * @param rpAppId - HYPR Relying Party Application ID
 * @param keyIdList - The list of KeyIds to deregister
 * Typically used in conjunction with Fido Client Adapter (FCA) integration when using a HYPR Fido Server
 * but not using the HYPR Relying Party Client (which does server communication).
 */
-(NSURLRequest*)deregisterGetRequestWithUsername:(NSString*)username rpUrl:(NSString*)rpUrl rpAppId:(NSString*)rpAppId keyIdList:(NSArray<NSString*>* _Nullable)keyIdList;

@end

NS_ASSUME_NONNULL_END
