//
//  HyprAsmProtocol.h
//  HyprCore
//
//  Created by Robert Carlsen on 9/1/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HyprAuthenticatorProtocol;
// Do we need a separate AsmAuthenticator protocol?
// that would manage the authenticator indexes?


@protocol HyprAsmProtocol <NSObject>
// need to declare some data model (ASMContext?)
// that stores the current AppRegistrations (which includes KeyIDs)

// there does not seem to be a direct connection between an
// Authenticator instance and the AppRegistrations (appId + keyId).

// the ASM manages them separately.
// the ASM manages the AppRegistrations and invokes the Authenticator only for verification during operation processing.

/**
 Get authenticators managed by this ASM

 @return List of current authenticators available.
 */
-(nullable NSArray<id<HyprAuthenticatorProtocol>>*)authenticators;

/**
 Get key IDs for the provided authenticator.

 @param authenticator Authenticator to retrieve key IDs
 @return List of current key IDs associated with the provided authenticator.
 */
-(nullable NSArray<NSString*>*)keyIDsForAuthenticator:(nonnull id<HyprAuthenticatorProtocol>)authenticator;

-(_Nonnull instancetype)init;

@end
