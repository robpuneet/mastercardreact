//
//  HYPRBaseClient.h
//  HyprCore
//
//  Created by Robert Carlsen on 3/28/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HYPRCustomHeadersAdapter.h>

NS_ASSUME_NONNULL_BEGIN

/// This base class should be used as the foundation for additional, concrete subclasses
/// Create specific API methods in subclasses. See Relying Party Client and License Client.
@interface HYPRBaseClient : NSObject
@property(nonatomic, weak) id<HYPRCustomHeadersAdapter> customHeadersDelegate;
/**
 Default initializer to create a client with the designated host.

 @param host String describing the host where the HYPR server is accessible. eg. demo.biometric.software
 @return A new instance of the HYPR client.
 */
+(instancetype)clientWithHost:(NSString*)host;

/**
 Designated initializer to supplement the default initializer with a custom headers delegate
 
 @param host String describing the host where the HYPR server is accessible. eg. demo.biometric.software
 @param delegate HYPRCustomHeadersAdapter delegate conforming to the HYPRCustomHeadersAdapter protocol
 @return A new instance of the HYPR client.
 */
+(instancetype)clientWithHost:(NSString*)host customHeadersAdapter:(id<HYPRCustomHeadersAdapter>_Nullable)delegate;

/// Use the convenience constructor +clientWithHost: instead.
+(instancetype)new NS_UNAVAILABLE;

/**
 Designated initializer to create a client with the designated host, and optionally NSURLSession.

 @param urlSession NSURLSession that the client may use to send requests to the HYPR server. Pass nil to use the internal NSURLSession.
 @param host String describing the host where the HYPR server is accessible. eg. demo.biometric.software
 @return A new instance of the HYPR Client.
 */
-(instancetype)initWithURLSession:(NSURLSession* _Nullable)urlSession host:(NSString*)host;

/**
  Designated initializer to supplement the designated initializer for an already configured url session with a custom headers delegate
 
 @param urlSession NSURLSession that the client may use to send requests to the HYPR server. Pass nil to use the internal NSURLSession.
 @param host String describing the host where the HYPR server is accessible. eg. demo.biometric.software
 @param delegate HYPRCustomHeadersAdapter delegate conforming to the HYPRCustomHeadersAdapter protocol
 @return A new instance of the HYPR Client.
 */
-(instancetype)initWithURLSession:(NSURLSession* _Nullable)urlSession host:(NSString*)host customHeadersAdapter:(id<HYPRCustomHeadersAdapter>_Nullable)delegate;
/**
 Designated initializer to create a client with the designated host, and optionally SSL Pinning.

 If pinning configuration is provided, the internal URL Session will pin SSL connections using them to secure the connection against proxying.

 @param domainConfiguration NSDictionary SSL Pinning configuration for the provided host. Currently expects dictionary which conforms to the TrustKit TSKPinnedDomains key.
 @param host String describing the host where the HYPR License server is accessible. eg. demo.biometric.software
 @return A new instance of the HYPR Client.
 */
-(instancetype)initWithSSLPinningConfiguration:(NSDictionary* _Nullable)domainConfiguration host:(NSString*)host;

/**
 Designated initializer to supplement the designated initializer for a host and optional SSL pinning configuration with a custom headers delegate
 
 If pinning configuration is provided, the internal URL Session will pin SSL connections using them to secure the connection against proxying.
 
 @param domainConfiguration NSDictionary SSL Pinning configuration for the provided host. Currently expects dictionary which conforms to the TrustKit TSKPinnedDomains key.
 @param host String describing the host where the HYPR License server is accessible. eg. demo.biometric.software
@param delegate HYPRCustomHeadersAdapter delegate conforming to the HYPRCustomHeadersAdapter protocol
 @return A new instance of the HYPR Client.
 */
-(instancetype)initWithSSLPinningConfiguration:(NSDictionary* _Nullable)domainConfiguration host:(NSString*)host customHeadersAdapter:(id<HYPRCustomHeadersAdapter>_Nullable)delegate;

/// Use the designated initializer -initWithURLSession:host: instead.
-(instancetype)init NS_UNAVAILABLE;

/**
 Set SSL Pinning Enable for all communications. Default value is enabled.
 
 @param enabled Boolean value to enable/disable SSL Pinning
 */
-(void)setSSLPinningEnabled:(BOOL)enabled;

/// Cancel all queued and in-flight requests. Catch the cancellation in the request completion handler.
-(void)cancelAllRequests;

@end

NS_ASSUME_NONNULL_END
