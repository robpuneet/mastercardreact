//
//  HYPRConstants.h
//  HyprCore
//
//  Created by Robert Carlsen on 8/21/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#ifndef HYPRConstants_h
#define HYPRConstants_h

#import <Foundation/Foundation.h>

extern NSString * const HYPR_UAF_AAID_FACEID;
extern NSString * const HYPR_UAF_AAID_FACEID_FULL_BASIC;
extern NSString * const HYPR_UAF_AAID_FINGERPRINT;
extern NSString * const HYPR_UAF_AAID_FINGERPRINT_FULL_BASIC;
extern NSString * const HYPR_UAF_AAID_FACE;
extern NSString * const HYPR_UAF_AAID_FACE_FULL_BASIC;
extern NSString * const HYPR_UAF_AAID_VOICE;
extern NSString * const HYPR_UAF_AAID_VOICE_FULL_BASIC;
extern NSString * const HYPR_UAF_AAID_PIN;
extern NSString * const HYPR_UAF_AAID_PIN_FULL_BASIC;
extern NSString * const HYPR_UAF_AAID_PIN_HEADLESS;
extern NSString * const HYPR_UAF_AAID_LOOPBACK;

// Constants for the SSL Pinning key algorithm types
typedef NSString* HYPRKeyAlgorithmType;
FOUNDATION_EXPORT const HYPRKeyAlgorithmType kHYPRKeyAlgorithmRsa2048;
FOUNDATION_EXPORT const HYPRKeyAlgorithmType kHYPRKeyAlgorithmRsa4096;
FOUNDATION_EXPORT const HYPRKeyAlgorithmType kHYPRKeyAlgorithmEcDsaSecp256r1;
FOUNDATION_EXPORT const HYPRKeyAlgorithmType kHYPRKeyAlgorithmEcDsaSecp384r1;


typedef NS_ENUM(NSUInteger, HYPREnrollmentResult) {
    HYPREnrollmentResultSuccess,        /// enrollment was successful
    HYPREnrollmentResultCancelled,      /// enrollment was cancelled
    HYPREnrollmentResultFailed,         /// enrollment failed
    HYPREnrollmentResultAlreadyEnrolled,/// the user was already enrolled
    HYPREnrollmentResultUnavailable,    /// the underlying verification system is not available for enrollment
    HYPREnrollmentResultUnknownError,   /// an unknown error occurred
};

typedef NS_ENUM(NSUInteger, HYPRAuthenticationResult) {
    HYPRAuthenticationResultSuccess,      /// verification successful
    HYPRAuthenticationResultCancelled,    /// the verification was cancelled
    HYPRAuthenticationResultFailed,       /// the verification failed
    HYPRAuthenticationResultUnavailable,  /// the underlying verification system is not enrolled or otherwise not available
    HYPRAuthenticationResultUnknownError, /// an unknown error occurred
};

typedef NS_ENUM(NSUInteger, HYPRTransactionConfirmationResult) {
    HYPRTransactionConfirmationResultSuccess,               /// the user confirmed the transaction
    HYPRTransactionConfirmationResultCancelled,             /// the user cancelled the transaction
    HYPRTransactionConfirmationResultAccessDenied,          /// the authenticator does not support transaction confirmation
    HYPRTransactionConfirmationResultCannotRenderContent,   /// the authenticator cannot render the transaction content
    HYPRTransactionConfirmationResultUnknownError,          /// an unknown error occurred
};

typedef NS_ENUM(NSUInteger, HYPRUserDeletionResult) {
    HYPRUserDeletionResultSuccess,          /// deletion of user-specific authenticator data was successful
    HYPRUserDeletionResultNotSupported,     /// this authenticator does not internally store user-specific data.
    HYPRUserDeletionResultNotEnrolled,      /// user data not found for specified identifier
    HYPRUserDeletionResultUnknownError,     /// an unknown error occurred while deleting user-specific data
};

typedef NS_ENUM(NSUInteger, HYPRAPIEventResult) {
    HYPRAPIEventResultFailure,
    HYPRAPIEventResultQRCodeScanFinished,      /// QR code scan finished and dismissed the view
    HYPRAPIEventResultAuthenticated,          /// an authentication process has returned successfully
    HYPRAPIEventResultUnlocked,                /// an unlock process has returned successfully
    HYPRAPIEventResultCompleted,              /// all API calls have completed successfully in a given operation
};

typedef NS_ENUM(NSUInteger, HYPRResetOption) {
    HYPRResetOptionFull,                // Full option will reset data on local device and on relying party
    HYPRResetOptionLocal,               // Local option will reset data only on local device
};

typedef NS_ENUM(NSUInteger, HYPRUserAgentProfileType) {
    HYPRUserAgentProfileTypeWeb,            //Registered web profiles
    HYPRUserAgentProfileTypeWorkstation     //Registered workstation profiles
};

typedef NS_ENUM(NSUInteger, HYPRUserAgentPINInputType) {
    HYPRUserAgentPINInputTypeAlert,            //Alert screen for user PIN input
    HYPRUserAgentPINInputTypeQRCodeScan        //QR Scan screen for PIN
};

#endif /* HYPRConstants_h */
