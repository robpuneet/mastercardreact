//
//  HYPRLoopbackAuthenticator.h
//  HyprCore
//
//  Created by Robert Carlsen on 8/21/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HyprAuthenticatorProtocol.h>

// provides interface for ASM to access authenticator functions
// must internally interact with the authentication provider
@interface HYPRLoopbackAuthenticator : NSObject <HyprAuthenticatorProtocol>

@end
