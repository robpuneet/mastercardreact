//
//  HYPRUserAgentStrings.h
//  HyprCore
//
//  Created by Ievgen Kreshchenko on 1/28/19.
//  Copyright © 2019 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HYPRUserAgentStrings : NSObject

// Strings for default AlertView for entering the pin for device registration, for example during license setup flow (not for pin authenticator).
// AlertView title. Default is "Enter Setup PIN"
@property (nonatomic, copy) NSString* registerRemoteDeviceAlertTitle;
// AlertView pin textfield placeholder. Default is "Enter Setup PIN"
@property (nonatomic, copy) NSString* registerRemoteDeviceAlertTextFieldPlaceholder;
// AlertView cancel button title. Default is "Cancel"
@property (nonatomic, copy) NSString* registerRemoteDeviceAlertCancelButtonTitle;
// AlertView submit button title
@property (nonatomic, copy) NSString* registerRemoteDeviceAlertSubmitButtonTitle;

// Strings for customization the OOB authentication screen
// Authentication confirmation message. Default is "Do you want to complete this authentication?"
@property (nonatomic, copy) NSString* OOBAuthenticationScreenConfirmationMessage;

@end
