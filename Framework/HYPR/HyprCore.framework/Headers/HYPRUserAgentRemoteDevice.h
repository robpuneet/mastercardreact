/**
 * This file is generated using the remodel generation script.
 * The name of the input file is HYPRUserAgentRemoteDevice.value
 */

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, HYPRWorkstationStatus) {
    HYPRWorkstationStatusUnlocked,     /// workstation is unlocked.
    HYPRWorkstationStatusLocked,       /// workstation is logged in and locked.
    HYPRWorkstationStatusUnreachable,  /// workstation is unreachable by the rp.
    HYPRWorkstationStatusAsleep,       /// workstation is in hibernation.
    HYPRWorkstationStatusUnregistered, /// workstation is unregistered, the workstation will be removed in this case. Should never be seen by the SDK user
    HYPRWorkstationStatusUnknown,      /// an unknown status message has been return.
};

@interface HYPRUserAgentRemoteDevice : NSObject <NSCopying, NSCoding>

/**
 * Unique device identifier provided during Device Setup.
 */
@property (nonatomic, readonly, copy) NSString *deviceIdentifier;
/**
 * User-friendly, display name for this device. may be customized by the user.
 */
@property (nonatomic, readonly, copy) NSString *displayName;
/**
 * The machine name associated with this Remote Device
 */
@property (nonatomic, readonly, copy) NSString *deviceName;
/**
 * TBD. Currently 2 values supported:"WEB", "WORKSTATION"
 */
@property (nonatomic, readonly, copy) NSString *deviceType;
/**
 * The username associated with this Remote Device
 */
@property (nonatomic, readonly, copy) NSString *deviceUsername;
/**
 * Optional, actionIds for operations. Typically contains keys for default registration and authentication.
 */
@property (nonatomic, readonly, copy) NSDictionary<NSString*,NSString*> *actionIds;
/**
 * Status message of the workstation, received from StatusRequest. Can have following values: 'LOCKED', 'UNLOCKED', 'UNREACHABLE', 'UNREGISTERED', 'ASLEEP'
 */
@property (nonatomic, readonly, copy) NSString *statusMessage;

@property (readonly) HYPRWorkstationStatus status;

/**
 * API version 
 */
@property (nonatomic, readonly, copy) NSNumber *apiVersion;

+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithDeviceIdentifier:(NSString *)deviceIdentifier displayName:(NSString *)displayName deviceName:(NSString *)deviceName deviceType:(NSString *)deviceType deviceUsername:(NSString *)deviceUsername actionIds:(NSDictionary<NSString*,NSString*> *)actionIds statusMessage:(NSString *)statusMessage apiVersion:(NSNumber *)apiVersion;

@end

