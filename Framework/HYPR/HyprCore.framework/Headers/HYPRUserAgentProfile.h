/**
 * This file is generated using the remodel generation script.
 * The name of the input file is HYPRUserAgentProfile.value
 */

#import <Foundation/Foundation.h>
#import "HYPRUserAgentAccount.h"
#import "HYPRUserAgentProfileConfiguration.h"
#import "HYPRUserAgentPersona.h"

@interface HYPRUserAgentProfile : NSObject <NSCopying, NSCoding>

/**
 * User-facing description for this Profile. May be modified by the user.
 */
@property (nonatomic, readonly, copy) NSString *displayName;
/**
 * Configuration information for this profile
 */
@property (nonatomic, readonly, copy) HYPRUserAgentProfileConfiguration *configuration;
/**
 * Persona is a unique identifier to separate users across Profiles. This must be shared with the ASM.
 */
@property (nonatomic, readonly, copy) HYPRUserAgentPersona *persona;
/**
 * List of registered User Accounts in this Profile.
 */
@property (nonatomic, readonly, copy, nullable) NSArray<HYPRUserAgentAccount*> *userAccounts;

+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithDisplayName:(NSString *)displayName configuration:(HYPRUserAgentProfileConfiguration *)configuration persona:(HYPRUserAgentPersona *)persona userAccounts:(nullable NSArray<HYPRUserAgentAccount*> *)userAccounts;

@end

