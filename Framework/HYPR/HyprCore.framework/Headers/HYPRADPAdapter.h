//
//  HYPRADPAdapter.h
//  HyprCore
//
//  Created by Ievgen Kreshchenko on 9/17/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ParamSpecProtocol;

@interface HYPRADPAdapter : NSObject

+ (instancetype _Nonnull )sharedInstance;

/**
 registers the Hypr ADP solution, used for replacing secure enclave functionality for old devices iPhone 5C, iPhone 5 and lower and for support of Full Basic Attestation authenticators
 @param completion - will return an error if the HYPR SDK was not able to initialize the adp framework instance
 */
- (void)initializeHYPRADPWithCompletion:(void (^_Nullable)(NSError*_Nullable))completion;

/**
 checks if the ADP is available.
 @return YES if ADP is initialized, NO if ADP is not initialized.
 @note method may return NO for some period of time after calling to initializeHYPRADPWithCompletion:, because the ADP initialization may take significant time and runs asynchronously.
 */
- (BOOL)isADPAvailable;

#pragma mark - Full basic attestation

/**
 returns signed data
 @param message - data to be signed
 @param aaid - aaid of authenticator, currently being registetered
 @param error - error returned, if operation fails
 @return signed data
 */
- (NSData*_Nullable)attestationSignature:(NSData*_Nonnull)message forAAID:(NSString*_Nonnull)aaid error:(NSError *_Nullable*_Nullable)error;

/**
 returns certificates chain
 @param aaid - aaid of authenticator, currently being registetered
 @param error - error returned, if operation fails
 @return array of certificates data
 */
- (NSArray<NSData*> *_Nullable)attestationCertificateChainForAAID:(NSString*_Nonnull)aaid error:(NSError *_Nullable*_Nullable)error;

#pragma mark - Encryption keys generation, retrieving, signing

/**
 generates the private and public key pair
 @param alias - alias, assotiated with the key pair
 @param error - error, describing reasons if method fails
 @return public key bits
 */
-(NSData* _Nullable)generateKeyPair:(NSString *_Nonnull)alias error:(NSError *_Nullable*_Nullable)error;

/**
 returns the public key reference
 @param alias - alias, assotiated with the key
 @param error - error, describing reasons if method fails
 @return public key reference
 */
-(SecKeyRef _Nullable)publicKey:(NSString *_Nonnull)alias error:(NSError *_Nullable*_Nullable)error;

/**
 returns the public key SHA256 hash
 @param alias - alias, assotiated with the key
 @param error - error, describing reasons if method fails
 @return public key hash
 */
-(NSString* _Nullable)publicKeyHash:(NSString *_Nonnull)alias error:(NSError *_Nullable*_Nullable)error;

/**
 returns the data, signed with the key, assosiated with the alias
 @param message - the data to sign
 @param alias - alias, assotiated with the key
 @param error - error, describing reasons if method fails
 @return signed data
 */
-(NSData* _Nullable)sign:(NSData* _Nonnull)message alias:(NSString *_Nonnull)alias error:(NSError *_Nullable*_Nullable)error;

#pragma mark - Data storing

-(NSData*_Nullable)retrieve:(NSString *_Nonnull)alias error:(NSError *_Nullable*_Nullable)error;
-(BOOL)store:(NSString *_Nonnull)alias data:(NSData*_Nonnull)data error:(NSError *_Nullable*_Nullable)error;
-(BOOL)delete:(NSString*_Nonnull)alias error:(NSError *_Nullable*_Nullable)error;
-(BOOL)exists:(NSString*_Nonnull)alias error:(NSError *_Nullable*_Nullable)error;

@end
