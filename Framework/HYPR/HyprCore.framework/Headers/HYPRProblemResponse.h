/**
 * This file is generated using the remodel generation script.
 * The name of the input file is HYPRProblemResponse.value
 */

#import <Foundation/Foundation.h>

/**
 * Response Object conforming to RFC7807 provided by HYPR Servers in the event of a problem.
 */
@interface HYPRProblemResponse : NSObject <NSCopying, NSCoding>

/**
 * Required. URL describing the type of the problem.
 */
@property (nonatomic, readonly, copy) NSString *type;
/**
 * Required. User-facing message describing the problem
 */
@property (nonatomic, readonly, copy) NSString *title;
/**
 * Required. API-specific status code
 */
@property (nonatomic, readonly, copy) NSNumber *status;
/**
 * Required. Detailed description of the problem.
 */
@property (nonatomic, readonly, copy) NSString *detail;
/**
 * Optional field. The Session ID related to this response.
 */
@property (nonatomic, readonly, copy) NSString *sessionId;

+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithType:(NSString *)type title:(NSString *)title status:(NSNumber *)status detail:(NSString *)detail sessionId:(NSString *)sessionId;

@end

