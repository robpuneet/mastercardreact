//
//  HYPRPushNotificationAdapter.h
//  HyprCore SDK
//
//  Created by Robert Carlsen on 12/15/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#ifndef HYPRPushNotificationAdapter_h
#define HYPRPushNotificationAdapter_h

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// Notification Center Key to be posted by the adapter implementation if/when the Push Notification Provider updates registration information.
extern NSString * const HYPRPushNotificationRegistrationUpdated;

// Key for transaction title in the userInfo dictionary of HYPRNotificationHandlerCompletion completion block
extern NSString * const HYPR_OOB_ACTION_TITLE_KEY;
// Key for transaction message in the userInfo dictionary of HYPRNotificationHandlerCompletion completion block
extern NSString * const HYPR_OOB_ACTION_MESSAGE_KEY;
// Key for OOB action type in the userInfo dictionary of HYPRNotificationHandlerCompletion completion block
extern NSString * const HYPR_OOB_ACTION_TYPE;
// Key for OOB remote device in the userInfo dictionary of HYPRNotificationHandlerCompletion completion block
extern NSString * const HYPR_OOB_ACTION_REMOTE_DEVICE_KEY;

// Possible values for the HYPR_OOB_ACTION_TYPE key in userInfo dictionary
typedef NS_ENUM(NSUInteger, HYPROOBActionType) {
    HYPROOBActionTypeAuthenticate,                    // Authentication, for example to login
    HYPROOBActionTypeConfirmTransaction,              // Transaction confirmation
    HYPROOBActionTypeUnknown                          // Unknown type, something went wrong
};

typedef void(^HYPRNotificationHandlerCompletion)(NSDictionary *  _Nullable userInfo, NSError * _Nullable error);
typedef BOOL(^HYPRNotificationHandlerAction)(NSDictionary *jsonPayload, HYPRNotificationHandlerCompletion completion);

/// A protocol to permit various Push Notification providers to be integrated with our client.
@protocol HYPRPushNotificationAdapter <NSObject>
// the adapter should have a mechanism to be configured with necessary parameters/keys...but that is specific to concrete implementations of the Adapter

/// typically channelId in Urban Airship systems or notification key in Firebase; could be the Apple Device Token or alternate identifier appropriate for the Push Provider
-(NSString*)devicePushIdentifier;

/// typically GCM App ID in Firebase or Application Key in Urban Airship.
-(NSString*)providerAppIdentifier;

-(BOOL)areNotificationsAuthorized;

-(BOOL)userPushNotificationsEnabled;

/// this is more to set the user notifications to be enabled on the first run...
/// which could trigger the iOS permission dialog
-(void)userPushNotificationsEnabled:(BOOL)enabled NS_SWIFT_NAME(userPushNotifications(enabled:));

/// register a block which should be called when an incoming push notification has been received
/// the payload is expected to be a JSON Object (Dictionary)
- (void)setPushNotificationHandlerAction:(HYPRNotificationHandlerAction _Nonnull)handlerAction NS_SWIFT_NAME(setPushNotificationHandler(handlerAction:));

// Resets the push notification push provider
-(void)resetWithCompletion:(void(^)(NSError* error))completion;
@end

NS_ASSUME_NONNULL_END

#endif /* HYPRPushNotificationAdapter_h */
