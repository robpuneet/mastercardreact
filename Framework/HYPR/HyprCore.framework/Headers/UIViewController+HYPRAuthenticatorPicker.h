//
//  UIViewController+HYPRAuthenticatorPicker.h
//  HyprCore
//
//  Created by Ievgen Kreshchenko on 10/22/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickerViewAuthenticatorInfo : NSObject

@property (nonatomic, strong) NSString* _Nonnull title;
@property (nonatomic, strong) NSString* _Nonnull authenticatorDescription;
@property (nonatomic, strong) NSString* _Nonnull aaid;
@property (nonatomic, strong) UIImage* _Nullable icon;

@end
/**
 This category implements the interface for getting the authenticators info from the Fido Client for presenting it on the UI and sending back to the Fido Client adapter the user's choice.
 */
@interface UIViewController (HYPRAuthenticatorPicker)

/**
 @property authenticatorInfoArray - The NSArray of NSArrays of PickerViewAuthenticatorInfo objects with authenticators' information: title, authenticatorDescription, AAID and icon.
 */
@property (nonatomic, strong) NSArray<NSArray<PickerViewAuthenticatorInfo*>*>* _Nonnull authenticatorInfoArray;
/**
 Method to be called when user picked one of the presented options
 @param index - index of the picked object in the original authenticatorInfoArray
 */
- (void)pickedItemAtIndex:(NSUInteger)index;
/**
 Method to be called when user cancelled the action
 */
- (void)cancelled;

@end
