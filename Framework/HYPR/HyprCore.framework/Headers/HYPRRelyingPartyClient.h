//
//  HYPRRelyingPartyClient.h
//  HyprCore
//
//  Created by Robert Carlsen on 11/15/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HYPRBaseClient.h>
#import <HyprCore/HYPRRelyingPartyConstants.h>

@class HYPRRelyingPartyBaseResponse;
@class HYPRRelyingPartyDeviceSetupRequest;
@class HYPRRelyingPartyDeviceRegistrationRequest;
@class HYPRRelyingPartyDeviceAuthorizationRequest;
@class HYPRRelyingPartyDeviceAuthorizeUnlockRequest;
@class HYPRRelyingPartyDeviceAuthorizationCompleteRequest;
@class HYPRRelyingPartyDeviceStatusRequest;
@class HYPRRelyingPartyDeviceCancelUnlockRequest;
@class HYPRRelyingPartyDeviceDeregisterRequest;
@class HYPRRelyingPartyFIDOGetRequest;
@class HYPRRelyingPartyFIDOSendRequest;
@class HYPRRelyingPartyReportDiagnosticRequest;

NS_ASSUME_NONNULL_BEGIN

typedef void(^HYPRRelyingPartyClientResponse)(HYPRRelyingPartyBaseResponse * _Nullable response, NSError * _Nullable error);
typedef void(^HYPRCommonResponse)(id _Nullable response, NSError * _Nullable error);

/**
 Client which implements the HYPR Relying Party API.
 */
@interface HYPRRelyingPartyClient : HYPRBaseClient

// Out-of-band Operations
-(void)deviceSetupWithRequest:(HYPRRelyingPartyDeviceSetupRequest*)deviceSetupRequest completion:(HYPRRelyingPartyClientResponse)completion;
-(void)deviceRegistrationRequest:(HYPRRelyingPartyDeviceRegistrationRequest*)deviceRegistrationRequest completion:(HYPRRelyingPartyClientResponse)completion;
-(void)deviceAuthorizeUnlockRequest:(HYPRRelyingPartyDeviceAuthorizeUnlockRequest*)deviceAuthorizeUnlockRequest completion:(HYPRRelyingPartyClientResponse)completion;
-(void)deviceCancelUnlockRequest:(HYPRRelyingPartyDeviceCancelUnlockRequest*)deviceAuthorizeCancelRequest completion:(HYPRRelyingPartyClientResponse)completion;
-(void)deviceAuthorizationCompleteRequest:(HYPRRelyingPartyDeviceAuthorizationCompleteRequest*)deviceAuthorizationCompleteRequest completion:(HYPRRelyingPartyClientResponse)completion;
-(void)deviceStatusRequest:(HYPRRelyingPartyDeviceStatusRequest*)deviceStatusRequest completion:(HYPRRelyingPartyClientResponse)completion;
-(void)deviceDeregisterRequest:(HYPRRelyingPartyDeviceDeregisterRequest*)deviceDeregisterRequest forDevice:(HYPRUserAgentRemoteDevice*)device completion:(HYPRRelyingPartyClientResponse)completion;

// FIDO Operations
-(void)registrationGetRequest:(HYPRRelyingPartyFIDOGetRequest*)registrationGetRequest completion:(HYPRRelyingPartyClientResponse)completion;
-(void)registrationSendRequest:(HYPRRelyingPartyFIDOSendRequest*)registrationSendRequest completion:(HYPRRelyingPartyClientResponse)completion;
-(void)authenticationGetRequest:(HYPRRelyingPartyFIDOGetRequest*)authenticationGetRequest completion:(HYPRRelyingPartyClientResponse)completion;
-(void)authenticationSendRequest:(HYPRRelyingPartyFIDOSendRequest*)authenticationSendRequest completion:(HYPRRelyingPartyClientResponse)completion;
-(void)deregistrationGetRequest:(HYPRRelyingPartyFIDOGetRequest*)deregistrationGetRequest completion:(HYPRRelyingPartyClientResponse)completion;

// Diagnostics
-(void)reportDiagnosticRequest:(HYPRRelyingPartyReportDiagnosticRequest*)reportDiagnosticsRequest;
@end

NS_ASSUME_NONNULL_END
