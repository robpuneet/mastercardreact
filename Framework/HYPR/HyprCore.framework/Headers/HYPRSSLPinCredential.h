/**
 * This file is generated using the remodel generation script.
 * The name of the input file is HYPRSSLPinCredential.value
 */

#import <Foundation/Foundation.h>

@interface HYPRSSLPinCredential : NSObject <NSCopying, NSCoding>

/**
 * Credential content. Currently expected to be a base64-encoded string of a SHA256 hash of a DER-encoded public key.
 */
@property (nonatomic, readonly, copy) NSString *content;
/**
 * Key algorithm. Current expected values: RSA_2048, RSA_4096, ECDSA_SECP_256_R1, ECDSA_SECP_384_R1. See HYPRKeyAlgorithmType.
 */
@property (nonatomic, readonly, copy) NSString *algorithm;

+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithContent:(NSString *)content algorithm:(NSString *)algorithm;

@end

