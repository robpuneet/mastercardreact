//
//  HYPRAuthenticatorCustomizationProtocol.h
//  HyprCore
//
//  Created by Ethan Moon on 12/3/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#ifndef HYPRAuthenticatorCustomizationProtocol_h
#define HYPRAuthenticatorCustomizationProtocol_h

/*
 Protocol for Authenticators and ASM to conform for customization
*/
 
@protocol HYPRAuthenticatorCustomizationProtocol <NSObject>

/*
 Sets timeout for the authenticator, after which the authenticator will be dismissed.
 @param timeout specified in miliseconds
 */
+ (void)setTimeout:(long)timeout;

@end
#endif /* HYPRAuthenticatorCustomizationProtocol_h */
