//
//  HYPRPathUtilities.h
//  HyprCore
//
//  Created by Robert Carlsen on 2/22/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYPRPathUtilities : NSObject

/**
 The directory which should contain any persistent data stored by the HYPRCore SDK.

 Components (eg Authenticators) looking to store data in here should likely create a subdirectory for their data.

 @return URL to the base directory
 */
+ (NSURL* _Nullable)hyprDocumentsURL;
+ (NSString* _Nullable)hyprDocumentsDirectory;

+ (NSURL* _Nullable)applicationDocumentsURL;
+ (NSString * _Nullable)applicationDocumentsDirectory;

+ (NSURL* _Nullable)applicationsCachesURL;
+ (NSString * _Nullable)applicationCachesDirectory;

@end

NS_ASSUME_NONNULL_END
