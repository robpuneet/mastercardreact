//
//  HYPRInterstitialDelegateProtocol.h
//  HyprCore
//
//  Created by Robert Panebianco on 4/9/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//
#ifndef HYPRInterstitialDelegateProtocol_h
#define HYPRInterstitialDelegateProtocol_h

#import <UIKit/UIKit.h>
#import "HYPRInterstitialViewModel.h"

NS_ASSUME_NONNULL_BEGIN


// The following are valid keys in the userInfo dictionary that is passed to the customizeInterstitialView method for custom interstitial views.

/// Returns an NString* of which operation the delegate method was invoked in. See values below.
FOUNDATION_EXPORT NSString * const kHYPRInterstitialDelegateProtocolOperation;

/// Value for kHYPRInterstitialDelegateProtocolOperation key to describe a registration operation
FOUNDATION_EXPORT NSString * const kHYPRInterstitialDelegateProtocolOperationRegistration;

/// Value for kHYPRInterstitialDelegateProtocolOperation key to describe an authentication operation
FOUNDATION_EXPORT NSString * const kHYPRInterstitialDelegateProtocolOperationAuthentication;

/// Returns an NSNumber indicating the total number of required authenticators within a selected policy.
FOUNDATION_EXPORT NSString * const kHYPRInterstitialDelegateProtocolTotalAuthenticators;

/// Returns an NSNumber indicating the the index of the previous authenticator. Starts with 1.
FOUNDATION_EXPORT NSString * const kHYPRInterstitialDelegateProtocolAuthenticatorIndex;

/// Returns an NSString of the previous authenticators AAID.
FOUNDATION_EXPORT NSString * const kHYPRInterstitialDelegateProtocolPreviousAAID;

/// Returns an NSString describing the previous authenticator. E.g., "HYPR Face".
FOUNDATION_EXPORT NSString * const kHYPRInterstitialDelegateProtocolPreviousDescription;

/// Returns an NSString of the next authenticators AAID.
FOUNDATION_EXPORT NSString * const kHYPRInterstitialDelegateProtocolNextAAID;

/// Returns an NSString describing the next authenticator. E.g., "HYPR Face".
FOUNDATION_EXPORT NSString * const kHYPRInterstitialDelegateProtocolNextDescription;


@protocol HYPRInterstitialDelegateProtocol <NSObject>

/**
 A delegate can provide a configured view model to construct an interstitial view presented between authenticators.

 The userInfo will contain the following keys
 * kHYPRInterstitialDelegateProtocolTotalAuthenticators
 * kHYPRInterstitialDelegateProtocolAuthenticatorIndex
 * kHYPRInterstitialDelegateProtocolPreviousAAID
 * kHYPRInterstitialDelegateProtocolPreviousDescription
 * kHYPRInterstitialDelegateProtocolNextAAID
 * kHYPRInterstitialDelegateProtocolNextDescription
 * kHYPRInterstitialDelegateProtocolOperation

These keys are intended to be used as configuration data for a HYPRInterstitialViewModel.
E.g. An implementer of interstitialViewModelForUserInfo (and therefore protocol adherence to
HYPRInterstitialDelegateProtocol) may want to set the message property of a HYPRInterstitialViewModel
instance noting the user is on the 2nd authenticator of 3 in total.

 @param userInfo Dictionary providing context about the operation type, previous/next authenticators, and the authenticator count. See commments above about key/value types.
 @return Configured view model for the interstitial view. Returning `nil` will omit the interstitial view.
 */
-(HYPRInterstitialViewModel* _Nullable)interstitialViewModelForUserInfo:(NSDictionary<NSString*,id>*)userInfo;
@end

NS_ASSUME_NONNULL_END

#endif /* HYPRInterstitialDelegateProtocol_h */
