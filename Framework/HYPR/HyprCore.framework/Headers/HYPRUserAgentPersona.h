/**
 * This file is generated using the remodel generation script.
 * The name of the input file is HYPRUserAgentPersona.value
 */

#import <Foundation/Foundation.h>

@interface HYPRUserAgentPersona : NSObject <NSCopying, NSCoding>

/**
 * PersonaId is a randomly generated value upon creation.
 * However, it must persist throughout the lifetime of the profile to permit access to
 * user accounts which are associated with it. Discarding the persona will prevent access to
 * the associated user accounts.
 * @note This value *must* be shared with each ASM during FIDO Operations.
 */
@property (nonatomic, readonly, copy) NSString *personaId;
/**
 * User-facing description for this Persona. Typical examples are "Business" and "Personal"
 */
@property (nonatomic, readonly, copy) NSString *displayName;

+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithPersonaId:(NSString *)personaId displayName:(NSString *)displayName;

@end

