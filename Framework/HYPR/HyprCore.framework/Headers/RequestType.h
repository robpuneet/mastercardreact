//
//  RequestType.h
//  HyprCore
//
//  Created by Robert Carlsen on 8/18/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString* const kASMRequestGetInfoKey;
extern NSString* const kASMRequestRegisterKey;
extern NSString* const kASMRequestAuthenticateKey;
extern NSString* const kASMRequestDeregisterKey;
extern NSString* const kASMRequestGetRegistrationsKey;
extern NSString* const kASMRequestOpenSettingsKey;

/// ASM Request Type
typedef NS_ENUM(NSUInteger, RequestType) {
    RequestTypeUnknown = 0,
    RequestTypeGetInfo,         // "GetInfo",
    RequestTypeRegister,        // "Register",
    RequestTypeAuthenticate,    // "Authenticate",
    RequestTypeDeregister,      // "Deregister",
    RequestTypeGetRegistrations,// "GetRegistrations",
    RequestTypeOpenSettings,    // "OpenSettings"
};

/// Map RequestType enumeration to string value:
NSString* stringValueForRequestType(RequestType request);

/// Map Request string representation to enumeration:
RequestType requestTypeValueForString(NSString* value);

