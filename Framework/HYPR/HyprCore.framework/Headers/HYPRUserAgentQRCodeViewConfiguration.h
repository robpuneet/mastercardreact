//
//  HYPRUserAgentQRCodeViewConfiguration.h
//  HyprCore
//
//  Created by Ethan Moon on 3/4/19.
//  Copyright © 2019 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HYPRUserAgentQRCodeViewConfiguration : NSObject

@property (nonatomic, copy) NSString* titleText;
@property (nonatomic, copy) UIFont* titleFont;
@property (nonatomic, copy) UIColor* titleColor;
@property (nonatomic, copy) UIImage* logoImage;
@property (nonatomic, copy) NSString* alertMessageFailedDueToPermission;
@property (nonatomic, copy) NSString* alertMessageFailedGeneral;
@property (nonatomic, copy) NSString* alertTitle;
@property (nonatomic, copy) NSString* alertDismissText;
@property (nonatomic, copy) NSString* alertOpenSettingsText;

@end
