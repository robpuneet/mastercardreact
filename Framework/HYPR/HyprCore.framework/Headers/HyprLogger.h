//
//  HyprLogger.h
//  HyprFidoFramework
//
//  Created by Parita Shah on 6/26/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HyprLogger;

#ifndef HyprLogHelper
#define HyprLogHelper(_flag, _message) [HyprLogger logMessage:(_message) flag:(_flag) file:__FILE__ function:__PRETTY_FUNCTION__ line:__LINE__]
#endif

#ifndef HyprLog
#define HyprLog(format, ...) HyprLogDebug(format, ##__VA_ARGS__)
#endif

#ifndef HyprLogVerbose
#define HyprLogVerbose(format, ...) HyprLogHelper(HyprLogFlagVerbose, (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HyprLogDebug
#define HyprLogDebug(format, ...)   HyprLogHelper(HyprLogFlagDebug,   (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HyprLogInfo
#define HyprLogInfo(format, ...)    HyprLogHelper(HyprLogFlagInfo,    (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HyprLogWarning
#define HyprLogWarning(format, ...) HyprLogHelper(HyprLogFlagWarning, (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif

#ifndef HyprLogError
#define HyprLogError(format, ...)   HyprLogHelper(HyprLogFlagError,   (^{ return [NSString stringWithFormat:(format), ##__VA_ARGS__]; }))
#endif


/**
 Log flags are specified for each log statement. They are filtered by the log level.

 - HyprLogFlagVerbose: Highly detailed log messages. May affect runtime.
 - HyprLogFlagDebug: Detailed logs only used while debugging.
 - HyprLogFlagInfo: General information about app state.
 - HyprLogFlagWarning: Indicates possible error.
 - HyprLogFlagError: A unexpected error occurred.
 */
typedef NS_ENUM(NSUInteger, HyprLogFlag) {
    HyprLogFlagVerbose = 0,
    HyprLogFlagDebug   = 1,
    HyprLogFlagInfo    = 2,
    HyprLogFlagWarning = 3,
    HyprLogFlagError   = 4,
};

/**
 Log level is used to filter log flags.

 - HyprLogLevelAll: Output all log messages. Equivalent to HyprLogLevelVerbose.
 - HyprLogLevelVerbose: Output verbose and greater log messages.
 - HyprLogLevelDebug: Output debug and greater log messages.
 - HyprLogLevelInfo: Output info and greater log messages.
 - HyprLogLevelWarning: Output warning and greater log messages.
 - HyprLogLevelError: Output error log messages only.
 - HyprLogLevelNone: Disable all log messages.
 */
typedef NS_ENUM(NSUInteger, HyprLogLevel) {
    HyprLogLevelAll     = 0,
    HyprLogLevelVerbose = (HyprLogFlagVerbose),
    HyprLogLevelDebug   = (HyprLogFlagDebug),
    HyprLogLevelInfo    = (HyprLogFlagInfo),
    HyprLogLevelWarning = (HyprLogFlagWarning),
    HyprLogLevelError   = (HyprLogFlagError),
    HyprLogLevelNone    = NSUIntegerMax
};


/**
 Logging utility used by all Hypr Frameworks
 
 Override the `minimumLogLevel` property to configure a specific desired log output.
 
 eg. to disable all log output:
 @code [HyprLogger setMinimumLogLevel: HyprLogLevelNone];
 */
@interface HyprLogger : NSObject


/**
 Primative logging function. Should use the macros instead.

 @param message block to be evalutated when printing the log statement
 @param flag HyprLogFlag value for this message
 @param file Path to the file where the log stamement was generated.
 @param function Function where the log statement was generated.
 @param line Line where the log statement was generated
 @note file, function and line parameters are only output in Debug builds of the frameworks.
 */
+ (void) logMessage:(NSString * (^)(void))message
               flag:(HyprLogFlag)flag
               file:(const char *)file
           function:(const char *)function
               line:(NSUInteger)line;

/// Default log level is HyprLogLevelDebug (Debug build) or HyprLogLevelWarning (Release build)
+ (HyprLogLevel) minimumLogLevel;

/**
 Override the default minimum log level.

 @param minimumLogLevel All log messages with a HyprLogFlag value equal to or greater than the minimumLogLevel are output to the console.
 */
+ (void) setMinimumLogLevel:(HyprLogLevel)minimumLogLevel;
@end
