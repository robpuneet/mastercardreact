//
//  ASMManager.h
//  HyprCore
//
//  Created by Robert Carlsen on 8/25/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <HyprCore/HyprAsmProtocol.h>
#import <HyprCore/RequestType.h>

@class ASMContext;

@protocol HyprAsmProviderProtocol;

// XXX: this header is temporarily public; change back to project visibilty after building HyprAsmProtocol.
NS_ASSUME_NONNULL_BEGIN

// maybe need to pick protocol *or* base class, but not both
@interface ASMManager : NSObject<HyprAsmProtocol>

/**
 Initialize a new instance of an ASM Manager with a given context.

 @param context The context for this ASM
 @return Newly initialized instance
 */
+(instancetype)managerWithContext:(ASMContext*)context;

/**
 Initialize a new instance of an ASM Manager with a given context.

 @param context The context for this ASM
 @return Newly initialized instance
 */
-(instancetype)initWithContext:(ASMContext*)context NS_DESIGNATED_INITIALIZER;

// use one of the designated initializers above
-(instancetype)init NS_UNAVAILABLE;

/**
 Factory method to return appropriate ASM provider for the requested operation type.

 @param requestType Current operation type
 @return New instance of the required ASM provider to handle the request and prepare a response.
 */
-(nullable id<HyprAsmProviderProtocol>)providerForRequestType:(RequestType)requestType;


#pragma mark - HyprAsmProtocol methods
/**
 Get authenticators managed by this ASM

 @return List of current authenticators available.
 */
-(nullable NSArray<id<HyprAuthenticatorProtocol>>*)authenticators;


/**
 Get key IDs for the provided authenticator.

 @param authenticator Authenticator to retrieve key IDs
 @return List of current key IDs associated with the provided authenticator.
 */
-(nullable NSArray<NSString*>*)keyIDsForAuthenticator:(id<HyprAuthenticatorProtocol>)authenticator;


@end

NS_ASSUME_NONNULL_END
