/**
 * This file is generated using the remodel generation script.
 * The name of the input file is HYPRUserAgentAccount.value
 */

#import <Foundation/Foundation.h>
#import "HYPRUserAgentRemoteDevice.h"

@interface HYPRUserAgentAccount : NSObject <NSCopying, NSCoding>

/**
 * User-facing description of the user account. May be modified by the user.
 */
@property (nonatomic, readonly, copy) NSString *displayName;
/**
 * FIDO-specific username. Should be unique.
 */
@property (nonatomic, readonly, copy) NSString *fidoUsername;
/**
 * List of Remote Devices associated with this user account.
 */
@property (nonatomic, readonly, copy) NSArray<HYPRUserAgentRemoteDevice*> *remoteDevices;
/**
 * Set API Version of Relying Party
 * This value may change later as part of version negotiation 
 * Version 1 supports device registration, unlock, and de-registeration.
 * Version 2 and above supports device registration, unlock/login , and de-registeration.
 */
@property (nonatomic, readonly, copy) NSNumber *currentAPIVersion;

+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithDisplayName:(NSString *)displayName fidoUsername:(NSString *)fidoUsername remoteDevices:(NSArray<HYPRUserAgentRemoteDevice*> *)remoteDevices currentAPIVersion:(NSNumber *)currentAPIVersion;

@end

