//
//  HYPRViewConfiguration.h
//  HyprCore
//
//  Created by Parita Shah on 1/29/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <HyprCore/HYPRUserAgentStrings.h>
#import <HyprCore/HYPRUserAgentQRCodeViewConfiguration.h>

typedef NS_ENUM(NSUInteger, HYPRTheme) {
    HYPRThemeLight,
    HYPRThemeDark,
};

/// Notification identifier for subscribers to view configuration updates. e.g., when a profile is changed
/// Currently, there is no userInfo or object provided by this notification
FOUNDATION_EXPORT NSString * const HYPRViewConfigurationDidUpdateNotification;

@interface HYPRViewConfiguration : NSObject <NSCopying, NSCoding>
/// topBackgroundColor and bottomBackgroundColor can be set as same colors if solid colored background is desired
/// Setting top color for gradient color combination in background view
@property (nonatomic, copy) UIColor *topBackgroundColor;
/// Setting bottom color gradient color combination in background view
@property (nonatomic, copy) UIColor *bottomBackgroundColor;
/// Modifying the customer Logo. `nil` value removes the image; default dark/light themes provide the HYPR logo.
@property (nonatomic, copy) UIImage *customerLogoImage;
/// Modifying UILabel's tintColor() property
@property (nonatomic, copy) UIColor *labelTintColor;
/// Modifying UILabel's font property
@property (nonatomic, copy) UIFont *labelFont;

/// Font used for title labels
@property (nonatomic, copy) UIFont *titleFont;

/// Modifying UITextField's textColor() property
@property (nonatomic, copy) UIColor *textFieldTextColor;
/// Modifying UITextField's tintColor() property
@property (nonatomic, copy) UIColor *textFieldTintColor;
/// Modifying UITextField's font property
@property (nonatomic, copy) UIFont *textFieldFont;

/// Modifying UIButton's backgroundColor() property which are in the Footer View of the framework
@property (nonatomic, copy) UIColor *footerButtonBackgroundColor;
/// Modifying UIButton's tintColor() property which are in the Footer View of the framework
@property (nonatomic, copy) UIColor *footerButtonTintColor;
/// Modifying UIButton's textColor() property which are in the Footer View of the framework
@property (nonatomic, copy) UIColor *footerButtonTextColor;
/// Modifying UIButton's font() property which are in the Footer View of the framework
@property (nonatomic, copy) UIFont *footerButtonTextFont;
/// Modifying UIButton's borderColor() property which are in the Footer View of the framework
@property (nonatomic, copy) UIColor *footerButtonBorderColor;

/// Modifying UIButton's (login button) backgroundColor() property in the enabled state
@property (nonatomic, copy) UIColor *enabledButtonBackgroundColor;
/// Modifying UIButton's (login button) borderColor() property in the enabled state
@property (nonatomic, copy) UIColor *enabledButtonBorderColor;
/// Modifying UIButton's (login button) textColor() property in the enabled state
@property (nonatomic, copy) UIColor *enabledButtonTextColor;
/// Modifying highlightColor() property of the frameworks. highlightColor() works as a default color for a view controller
@property (nonatomic, copy) UIColor *highlightColor;
/// Modifying icon's fill color. Reflected in success/fail images' fill color and progress fill color
@property (nonatomic, copy) UIColor *iconFillColor;

/// Modifying "success" state icon fillColor()
@property (nonatomic, copy) UIColor *successImageFillColor;
/// Modifying "success" state UILabel
@property (nonatomic, copy) UIColor *successTextColor;
/// Modifying "success" state UIFont
@property (nonatomic, copy) UIFont *successFont;

/// Modifying "fail" state icon fillColor()
@property (nonatomic, copy) UIColor *failImageFillColor;
/// Modifying "fail" state UILabel
@property (nonatomic, copy) UIColor *failTextColor;
/// Modifying "fail" state UIFont
@property (nonatomic, copy) UIFont *failFont;

/// Modifying Instruction's label font
@property (nonatomic, copy) UIFont *instructionsLabelFont;
/// Modifying Instruction's label textColor
@property (nonatomic, copy) UIColor *instructionsLabelTextColor;

// Registration Success Title
@property (nonatomic, copy) NSString* successRegistrationTitle;

// Registration Success Text
@property (nonatomic, copy) NSString* successRegistrationMsg;

// Main View Screen Title
@property (nonatomic, copy) NSString* mainWorkstationTitle;

// Main Screen Tip Text
@property (nonatomic, copy) NSString* mainWorkstationAddText;

// Main Screen Button Tip Text
@property (nonatomic, copy) NSString* mainTapUnlockWorkstationText;

// Unlocking Computer Title
@property (nonatomic, copy) NSString* unlockingWorkstationText1;

// Unlocking Computer Instruction Text
@property (nonatomic, copy) NSString* unlockingWorkstationText2;

// Delete Screen Title
@property (nonatomic, copy) NSString* deleteWorkstationTitleText;

// Delete Screen Instruction Text1
@property (nonatomic, copy) NSString* deleteWorkstationText1;

// Delete Screen Instruction Text2
@property (nonatomic, copy) NSString* deleteWorkstationText2;

// Pairing Computer Title
@property (nonatomic, copy) NSString* pairingWorkstationTitleText;

// Pairing Computer Text
@property (nonatomic, copy) NSString* pairingWorkstationText;

// Manage Screen Title
@property (nonatomic, copy) NSString* manageWorkstationsTitleText;

// Manage Screen Text
@property (nonatomic, copy) NSString* manageNoWorkstationsText;

// Landing Screen Title
@property (nonatomic, copy) NSString* mainHeaderTitleText;

// Landing Screen Subtitle
@property (nonatomic, copy) NSString* mainHeaderSubtitleText;

// Transaction Screen Title
@property (nonatomic, copy) NSString* transactionTitleText;

// Transaction Screen Subtitle
@property (nonatomic, copy) NSString* transactionSubtitleText;

// Transaction Confirmation Text
@property (nonatomic, copy) NSString* transactionConfirmationText;

// Object that contains strings, customizable on UserAgent level
+ (HYPRUserAgentStrings*)userAgentStrings;

//  Object that contains UI related configuration for qr code view
+ (HYPRUserAgentQRCodeViewConfiguration*)qrCodeViewConfiguration;

@property (nonatomic, assign) HYPRTheme themeMode;

-(instancetype)initWithTheme:(HYPRTheme)theme;

+(instancetype)currentViewConfiguration;

+(void)setCurrentViewConfiguration:(HYPRViewConfiguration*)configuration;

@end
