/**
 * This file is generated using the remodel generation script.
 * The name of the input file is HYPRUserAgentProfileConfiguration.value
 */

#import <Foundation/Foundation.h>
#import "HYPRSSLPinCredential.h"

/**
 * The Configuration contains all information necessary to prepare the application
 * for use with a specific Relying Party Server / RP App ID.
 * This includes UI customization parameters and fundamental data to establish communication with the desired RP.
 */
@interface HYPRUserAgentProfileConfiguration : NSObject <NSCopying, NSCoding>

/**
 * The Relying Party App Id for this configuration
 */
@property (nonatomic, readonly, copy) NSString *rpAppId;
/**
 * The HYPR Relying Party Server URL
 */
@property (nonatomic, readonly, copy) NSString *rpServerUrl;
/**
 * Profile type. Corresponds to remote devices type of this profile. Can be "WEB", "WORKSTATION"
 */
@property (nonatomic, readonly, copy) NSString *deviceType;
/**
 * Relying Party Server SSL Pinning Credentials. Provide at least 2 credentials to enable SSL Pinning (current key and backup). Set to `nil` to disable SSL Pinning.
 */
@property (nonatomic, readonly, copy) NSArray<HYPRSSLPinCredential*> *rpSSLPinCredentials;
/**
 * Contents of the rest of the configuration TBD
 */
@property (nonatomic, readonly, copy) NSDictionary *additionalData;

+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithRpAppId:(NSString *)rpAppId rpServerUrl:(NSString *)rpServerUrl deviceType:(NSString *)deviceType rpSSLPinCredentials:(NSArray<HYPRSSLPinCredential*> *)rpSSLPinCredentials additionalData:(NSDictionary *)additionalData;

@end

