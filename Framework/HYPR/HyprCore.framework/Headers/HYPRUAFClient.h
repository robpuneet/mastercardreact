//
//  HYPRUAFClient.h
//  HyprCore
//
//  Created by Robert Carlsen on 8/10/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HYPRInterstitialDelegateProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@protocol HyprAsmProtocol;

@interface HYPRUAFClient : NSObject

/**
 Register new authenticator module.

 @warning Must register each desired Authenticator-Specific Module before invoking an instance of the UAF Client.

 @param moduleType Platform-specific implementation of the HyprAsmProtocol.
 */
+(void)registerAuthenticatorModule:(Class<HyprAsmProtocol>)moduleType;

/**
 Remove the authenticator module from the client.

 This Authenticator-Specific Module, and any Authenticators it manages, will no longer be included in UAF Operations.
 @warning This method is not typically used, but provided for completeness.

 @param moduleType Platform-specific implementation of the HyprAsmProtocol.
 */
+(void)unregisterAuthenticatorModule:(Class<HyprAsmProtocol>)moduleType;

/**
 Get the list of authenticator modules.

 @return List of currently registered Authenticator-Specific Modules for this UAF Client.
 */
+(nullable NSArray<Class<HyprAsmProtocol>>*)registeredAuthenticatorModules;

/**
 The FIDO Facet ID of the current application.

 @return string representation of the Facet ID
 */
+(NSString*)facetId;


/**
 The designated initializer for HYPRUAFClient

 @return new instance of the client.
 */
-(instancetype)init NS_DESIGNATED_INITIALIZER;

/**
 This method enables or disables presenting the UI for user to pick the AAID or AAIDs if more than one choice exists after policy matcing.
 @param enabled - enables presenting the AAID picker UI. By default it will not be presented and UAFClient will pick the first set of the policy matching result array.
 */

-(void)setAAIDPickerViewEnabled:(BOOL)enabled;
/**
 This method sets the custom UIViewController which will be presented to the user for picking the appropriate AAID(s) from the list, based on policy matching result array.
 If method is not called and the setAAIDPickerViewEnabled: method called with YES - the default viewController will be presented.
 @param viewController - the viewController MUST use the AuthenticatorPicker category. Check the UIViewController+AuthenticatorPicker.h for details
 @return error if the view controller fails to respond the category's methods.
 */

-(NSError* _Nullable)setAAIDPickerViewController:(UIViewController* _Nonnull)viewController;
/**
 Process a UAF Operation (eg Reg, Auth, Dereg).

 @param request Array of UAFProtocol Message Objects. eg UAFRegistrationRequest, UAFAuthenticationRequest, etc.
 @param username (optional) to focus an authentication operation on a specific user. if `nil` then standard FIDO specification rules apply to determine the registered user to use for the operation.
 @param parentViewController A view controller instance that the UAF Client may use to present any necessary views (eg. authenticators). If `nil` will attempt to use the Application's rootViewController. weakly retained.
 @param completion handler with the result of the operation as a JSON-compliant, UAF Response object
 */
-(void)handleUAFOperation:(NSArray<NSDictionary*>*)request username:(NSString* _Nullable)username parentViewController:(UIViewController* _Nullable)parentViewController interstitialViewDelegate:(id<HYPRInterstitialDelegateProtocol>_Nullable)delegate completion:(void(^)(NSDictionary* responseObject, NSError * _Nullable error))completion;

@end

NS_ASSUME_NONNULL_END
