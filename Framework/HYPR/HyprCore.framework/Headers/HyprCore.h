//
//  HyprCore.h
//  HyprCore
//
//  Created by Robert Carlsen on 7/5/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HyprCore.
FOUNDATION_EXPORT double HyprCoreVersionNumber;

//! Project version string for HyprCore.
FOUNDATION_EXPORT const unsigned char HyprCoreVersionString[];

// Utilities and Constants
#import <HyprCore/HyprLogger.h>
#import <HyprCore/HYPRPathUtilities.h>
#import <HyprCore/HYPRError.h>
#import <HyprCore/HYPRConstants.h>
#import <HyprCore/HYPRViewConfiguration.h>
#import <HyprCore/HYPRUserAgentStrings.h>
#import <HyprCore/HYPRUserAgentQRCodeViewConfiguration.h>

// FIDO Client
#import <HyprCore/HYPRUAFClient.h>
#import <HyprCore/HYPRUAFClient+AppAPI.h>
#import <HyprCore/HYPRUserAgentUtility.h>

// User Agent. Primary object to interface with HyprCore SDK
#import <HyprCore/HYPRUserAgent.h>

#import <HyprCore/HYPRInterstitialDelegateProtocol.h>
#import <HyprCore/HYPRInterstitialViewModel.h>

#import <HyprCore/HYPRBaseClient.h>
#import <HyprCore/HYPRRelyingPartyClient.h>
#import <HyprCore/HYPRRelyingPartyConstants.h>

#import <HyprCore/HYPRFidoClientAdapter.h>
#import <HyprCore/HYPRFidoPayloadAdapter.h>

#import <HyprCore/HyprAuthenticatorProtocol.h>
#import <HyprCore/HyprAsmProtocol.h>
#import <HyprCore/HYPRPushNotificationAdapter.h>

#import <HyprCore/HYPRAuthenticatorCustomizationProtocol.h>

#import <HyprCore/HYPRLoopbackAsm.h>
#import <HyprCore/HYPRLoopbackAuthenticator.h>

#import <HyprCore/ASMManager.h>

// Model objects
#import <HyprCore/HYPRProblemResponse.h>
#import <HyprCore/HYPRUserAgentProfile.h>
#import <HyprCore/HYPRUserAgentPersona.h>
#import <HyprCore/HYPRUserAgentAccount.h>
#import <HyprCore/HYPRUserAgentProfileConfiguration.h>
#import <HyprCore/HYPRUserAgentRemoteDevice.h>
#import <HyprCore/HYPRSSLPinCredential.h>

#import <HyprCore/RequestType.h>

// ADP
#import <HyprCore/HYPRADPAdapter.h>

#import <HyprCore/UIViewController+HYPRAuthenticatorPicker.h>


