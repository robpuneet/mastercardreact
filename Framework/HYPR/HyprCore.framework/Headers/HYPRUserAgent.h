//
//  HYPRUserAgent.h
//  HyprCore
//
//  Created by Robert Carlsen on 11/20/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <HyprCore/HYPRInterstitialDelegateProtocol.h>
#import <HyprCore/HYPRCustomHeadersAdapter.h>
#import <HyprCore/HYPRUserAgentProfile.h>
#import <HyprCore/HYPRConstants.h>
#import <HyprCore/HYPRADPAdapter.h>

NS_ASSUME_NONNULL_BEGIN


/**
 Completion block returned from most User Agent operations

 @param error `nil` if the operation was successful. NSError in the HYPRErrorDomain otherwise. See HYPRError.h for details.
 */
typedef void(^HYPRUserAgentGenericResult)(NSError * _Nullable error);

/**
 Block returned from select User Agent operations. The event informs the user at which point the block was called from. Expect mulitple invocations of the block.
 
 @param event returns the event the block was called in. See HYPRAPIEventResult.
 @param error `nil` if the operation was successful. NSError in the HYPRErrorDomain otherwise. See HYPRError.h for details.
 */
typedef void(^HYPRUserAgentEventResult)(HYPRAPIEventResult event, NSError * _Nullable error);

@protocol HYPRPushNotificationAdapter;

/**
 The User Agent is the primary interface for host applications.

 It communicates with the HYPR server via a Relying Party Client implementation
 and with the local FIDO client to service UAF operations.
 */
@interface HYPRUserAgent : NSObject

// Minimum API Version Support
@property(nonatomic, readonly) NSNumber* minimumAPIVersionSupport;

// Maximum API Version Support
@property(nonatomic, readonly) NSNumber* maximumAPIVersionSupport;

// Default API Version that user of SDK is able set. Will use this version if set otherwise default to SDK's highest API Version.
@property (nonatomic, readonly, nullable) NSNumber* defaultAPIVersion;

+(instancetype)sharedInstance;

-(instancetype)init NS_UNAVAILABLE;
-(instancetype)new NS_UNAVAILABLE;

#pragma mark - Network Settings

/**
 Set SSL Pinning Enable for all communications with RP. Default value is enabled.
 
 @param enabled Boolean value to enable/disable SSL Pinning
 */
+(void)setSSLPinningEnabled:(BOOL)enabled;

#pragma mark - UI Integration

/**
 Set the view controller that HYPR User Agent may use to present requried views.

 @param viewController The parent view controller which will be used for presentations. Weakly retained.
 */
+(void)setParentViewController:(UIViewController*)viewController;

/**
 Set the view delegate that will adhere to the HYPRInterstitialDelegateProtocol
 
 @param delegate The delegate that will implement the HYPRInterstitialDelegateProtocol methods. Weakly retained.
 */
+(void)setInterstitialViewDelegate:(id<HYPRInterstitialDelegateProtocol>)delegate;

/**
 Set the Push Notification Provider Adapter

 This is necessary to support push-notification-initiated operations, such as web login.

 @param adapter An implementation of the HYPRPushNotificationAdapter protocol. Weakly retained.
 */
+(void)setNotificationProviderAdapter:(id<HYPRPushNotificationAdapter>)adapter;

/**
 This method enables or disables presenting the UI for user to pick the AAID or AAIDs if more than one choice exists after policy matcing.
 @param enabled - enables presenting the AAID picker UI. By default it will not be presented. If set to NO - UAFClient will pick the first set of the policy matching result array.
 */
+(void)setAAIDPickerViewEnabled:(BOOL)enabled;
/**
 This method sets the custom UIViewController which will be presented to the user for picking the appropriate AAID(s) from the list, based on policy matching result array.
 If method is not called and the setAAIDPickerViewEnabled: method called with YES - the default viewController will be presented.
 @param viewController - the viewController MUST use the AuthenticatorPicker category. Check the UIViewController+AuthenticatorPicker.h for details
 @return error if the view controller fails to respond the category's methods.
 */
+(NSError* _Nullable)setAAIDPickerViewController:(UIViewController* _Nonnull)viewController;

#pragma mark - Client-Server interaction

/**
 Set the Custom Headers Adapter
 
 This is required if supporting the additional of custom headers to relying party payloads is needed.
 
 @param adapter an Implemention of the HYPRCustomHeadersAdapter protocol. Weakly retained.
 */
+(void)setCustomHeadersDelegate:(id<HYPRCustomHeadersAdapter>)adapter;

/**
 Set the default API version that HYPR User Agent will use to communicate with Relying Party Server.
 
 @note If Relying Party server can not support maximumAPIVersionSupport, API version must be specified.
 
 @param version API Version to use for communication with Relying Party Server.
 */
+(void)setDefaultAPIVersion:(NSNumber*)version;

#pragma mark - Advanced Device Protection (ADP) framework integration
/**
 registers the Hypr ADP solution, used for replacing secure enclave functionality for old devices iPhone 5C, iPhone 5 and lower and for support of Full Basic Attestation authenticators
 @param completion - returns with an error if the HYPR SDK was not able to initialize the adp framework instance
 */
+ (void)initializeHYPRADPWithCompletion:(void (^_Nullable)(NSError*_Nullable error))completion;

/**
 @return the instance of HYPRADPAdapter, the adapter for HYPRADP module initialized by calling initializeHYPRADPWithCompletion:
 @note if your HyprCore SDK doesn't support the ADP operations the adapter is still returned but with empty implementation
 */
- (HYPRADPAdapter*_Nullable)HYPRADPAdapter;

#pragma mark - Profile/Configuration Management
/**
 Begin the licensing/configuration flow.

 @param key PIN value presented by licensing website. Pass `nil` to be prompted for a QR Code scan view.
 @param completion Handler with the result of the operation. If successful, the newly created profile is set as the activeProfile.
 */
-(void)licenseConfigurationWithKey:(NSString* _Nullable)key completion:(HYPRUserAgentGenericResult)completion;

/**
 Begin the licensing/configuration flow.
 
 @param key PIN value presented by licensing website. Pass `nil` to be prompted for a QR Code scan view.
 @param eventBlock called upon completion of stage of license process: complete, failure, authentication completed, QR code scan finished. May be called more then once
 @note  Possible event values include:
        HYPRAPIEventResultFailure
        HYPRAPIEventResultQRCodeScanFinished
        HYPRAPIEventResultAuthenticated
        HYPRAPIEventResultCompleted
 */
-(void)licenseConfigurationWithKey:(NSString* _Nullable)key eventBlock:(HYPRUserAgentEventResult)eventBlock;

/**
 Cancel the latest license configuration request
 
 @param completion block with the result of the operation. Returns nil if success, error with details otherwise
 */
-(void)cancelLicenseConfiguration:(HYPRUserAgentGenericResult)completion;
/**
 Register a configured profile for use with HYPRCore.

 This method is primarily intended for applications which directly configure Profiles rather than dynamically configure them via the HYPR Licensing server.

 @param newProfile The configured profile with Relying Party URL, App ID, SSL Pinning credentials and additional configuration.
 @return NO if the profile limit of the corresponding type reached and profile is not added. YES - otherwise
 */
-(BOOL)registerProfile:(HYPRUserAgentProfile*)newProfile;

/**
 Profiles which have been registered.

 @return List of currently registered profiles. May be empty if no profiles have been registered.
 */
-(NSArray<HYPRUserAgentProfile*>* _Nullable)profiles;

/**
 Provides the currently active profile.

 @return Profile object with app configuration parameters. `nil` if no profiles are registered or have been selected.
 */
-(HYPRUserAgentProfile* _Nullable)activeProfile;

/**
 Switch the active profile to the specified value.

 @warn The provided profile *must* already be registered with the User Agent.

 Switching profiles will also invalidate the active user account.
 After switching, you may read the profile's configuration to re-configure the host application as needed.

 @param profile The profile the User Agent should make active.
 @return YES if the profile is valid and the User Agent could switch to it. NO if the profile is not registered.
 */
-(BOOL)switchActiveProfile:(HYPRUserAgentProfile*)profile;

/**
 Remove the specified profile from the User Agent
 
 @warn this will deregister any user accounts associated with the profile and workstations/web assosisated with accounts.
 
 @param profile The profile to unregister.
 @param completion Callback with the result of the operation.
 */
-(void)deregisterProfile:(HYPRUserAgentProfile*)profile completion:(HYPRUserAgentGenericResult _Nullable)completion;

/**
 Rename the provided profile.

 @note If this method returns an updated profile, then the profile list has been updated and should be re-loaded to retrieve the current values.

 @param profile The profile to update
 @param displayName A new display name for this profile.
 @return Updated profile. `nil` if the profile was not found in this User Agent.
 */
-(HYPRUserAgentProfile* _Nullable)renameProfile:(HYPRUserAgentProfile*)profile withDisplayName:(NSString*)displayName;

/**
 Limit the number of profiles of specific type allowed. By default no limit is set.
 
 @param limit limits the maximum number of profiles of the specified type. Set nil to remove the limit.
 @param profileType of HYPRUserAgentProfileType enum.
 */
-(void)setLimit:(NSNumber* _Nullable)limit forProfilesOfType:(HYPRUserAgentProfileType)profileType;

#pragma mark - User Management
/**
 Check if the provided FIDO Username is enrolled in the active profile.

 @param userName FIDO Username for the desired account
 @return YES if the profile contains a user account with the provided username. NO otherwise.
 */
-(BOOL)isUserRegistered:(NSString*)userName;

/**
 Get the active user account

 @return the active user account
 */
-(HYPRUserAgentAccount* _Nullable)activeUserAccount;

/**
 Rename the specified user account in the current Profile.

 @note If this method returns a user account, then the current Profile has been updated and should be re-loaded to retrieve the current values.

 @param userAccount User Account to update.
 @param displayName The desired display name.
 @return Updated User Account if found in the current profile. `nil` if the provided User Account is not registered with the current Profile.
 */
-(HYPRUserAgentAccount* _Nullable)renameUserAccount:(HYPRUserAgentAccount*)userAccount withDisplayName:(NSString*)displayName;

/**
 Switch the active user account to the specified value.

 @param account User account to use for subsequest operations
 @return YES if the account was found in the active profile. NO otherwise.
 */
-(BOOL)switchActiveUserAccount:(HYPRUserAgentAccount*)account;


/**
 Create a random user name which can be used to register new accounts.

 @return String which may be used for user registration.
 */
-(NSString*)generateUserName;

#pragma mark - Data Management
/**
 Reset the User Agent and all other persistent data managed by the HYPR Core framework per reset option.
 
 @param option This is the reset option to delete data locally only or on both locally and on relying party.
 @note  Possible reset option values include:
        HYPRResetOptionFull (recommended) - Full option will reset data on local device and on relying party
        HYPRResetOptionLocal (not recommended)- Local option will reset data only on local device
 @param completion Callback with the result of the operation.

 @warn this is the "nuclear option"; very destructive...will delete all profiles, user accounts and all FIDO data.
*/
+(void)resetUserAgentWithOption:(HYPRResetOption)option completion:(HYPRUserAgentGenericResult)completion;


#pragma mark - FIDO Operations
/**
 The FIDO UAF Facet ID of the calling agent.

 This is defined in the FIDO UAF specification. Typically derived from the host application bundle identifier.
 eg. ios:bundle-id:<bundle identifier>

 @return Caller Facet ID
 */
-(NSString*)callingFacetId;

// TODO: should return a token for each of these methods which could be used to cancel an in-flight operation.

/**
 Register a new user with the FIDO Server.

 @param userName (Optional) user name which will be used for this user with the FIDO Server. Should be unique. Pass `nil` to allow the User Agent to generate a username.
 @param actionId (Optional) action which should be used for this registration. Must be one of the defined values in the current Profile Configuration (TBD). Pass `nil` to perform the default action.
 @param completion Callback with the result of the operation.
 */
-(void)registerUserWithName:(NSString * _Nullable)userName action:(NSString * _Nullable)actionId completion:(HYPRUserAgentGenericResult)completion;

/**
 Re-register an existing User Account with the FIDO Server

 This may be used to add additional authenticator registrations and/or biometric enrollments for an existing user account.

 @param userAccount The user account which should be registered. Pass `nil` to use the activeUserAccount.
 @param actionId (Optional) action which should be used for this registration. Must be one of the defined values in the current Profile Configuration (TBD). Pass `nil` to perform the default action.
 @param completion Callback with the result of the operation.
 */
-(void)registerUser:(HYPRUserAgentAccount * _Nullable)userAccount action:(NSString * _Nullable)actionId completion:(HYPRUserAgentGenericResult)completion;

/**
 Deregister the specified user from the FIDO Server

 @warn This is a destructive action; credentials registered to this user will be deleted.

 @param userAccount The user account which should be deregistered. This user account will be removed from the current Policy.
 @param completion Callback with the result of the operation.
 */
-(void)deregisterUser:(HYPRUserAgentAccount* _Nullable)userAccount completion:(HYPRUserAgentGenericResult)completion;
// TODO: need to specify which authenticator(s) to deregister?

/**
 Authenticate a user with the Relying Party configured by the current Profile.

 @param userAccount The user account which should be used for authentication. Pass `nil` to use the activeUserAccount
 @param actionId (Optional) action which should be authenticated. Must be one of the defined values in the current Profile Configuration (TBD). Pass `nil` to perform the default action.
 @param completion Callback with the result of the operation.
 */
-(void)authenticateUser:(HYPRUserAgentAccount* _Nullable)userAccount action:(NSString * _Nullable)actionId completion:(HYPRUserAgentGenericResult)completion;

// TODO: introduce discovery API to query the FIDO Client for authenticator status

#pragma mark - Device Setup / Auth Operations

/**
 Get the list of currently registered Remote Devices for the active User Account.

 These objects may be passed to the other Remote Device methods to perform operations (eg. unlock, unregister) for them.

 @return list of Remote Devices registered to the active user account.
 */
-(NSArray<HYPRUserAgentRemoteDevice*>* _Nullable)registeredRemoteDevices;

/**
 Register a new Remote Device

 @param userAccount the User Account to associate with this Remote Device. Pass `nil` to create a new User Account during device setup.
 @param pin the PIN received from the remote device. Pass `nil` to be prompted for the pin through an alert.
 @param actionId Must be one of the defined values in the current Profile Configuration (TBD). Pass `nil` to perform the default action.
 @param completion handler called upon completion of the device setup operation.
 */
-(void)registerRemoteDeviceForUser:(HYPRUserAgentAccount* _Nullable)userAccount pin:(NSString* _Nullable)pin actionId:(NSString* _Nullable)actionId completion:(HYPRUserAgentGenericResult)completion;

/**
 Register a new Remote Device
 
 @param userAccount the User Account to associate with this Remote Device. Pass `nil` to create a new User Account during device setup.
 @param type Type of PIN input method: Alert or QR Code Scan. Selecting one will either prompt an alert view or a QR Code Scan for PIN input.
 @note Possible PIN input type values include:
        HYPRUserAgentPINInputTypeAlert
        HYPRUserAgentPINInputTypeQRCodeScan
 @param actionId Must be one of the defined values in the current Profile Configuration (TBD). Pass `nil` to perform the default action.
 @param completion handler called upon completion of the device setup operation.
 */
-(void)registerRemoteDeviceForUser:(HYPRUserAgentAccount* _Nullable)userAccount pinInputType:(HYPRUserAgentPINInputType)type actionId:(NSString* _Nullable)actionId completion:(HYPRUserAgentGenericResult)completion;

/**
 Cancel the latest register remote device request
 
 @param completion block with the result of the operation. Returns nil if success, error with details otherwise
 */
-(void)cancelRegisterRemoteDevice:(HYPRUserAgentGenericResult)completion;

/**
 Update statuses for registered remote devices of current active user account
 
 @note call registeredRemoteDevices method on success to get the updated instances of remote devices
 @param completion handler called upon completion of the device setup operation.
 */
-(void)updateRegisteredRemoteDevicesStatusesWithCompletion:(HYPRUserAgentGenericResult)completion;

/**
 Unlock the specified Remote Device
 
 @note This device must be owned by the active User Account.
 
 @param remoteDevice the Remote Device which should be unlocked.
 @param completion handler called upon completion of the device unlock operation.
 */
-(void)unlockRemoteDevice:(HYPRUserAgentRemoteDevice*)remoteDevice completion:(HYPRUserAgentGenericResult)completion;

/**
 Unlock the specified Remote Device with API specific handlers

 @note This device must be owned by the active User Account.

 @param remoteDevice the Remote Device which should be unlocked.
 @param eventBlock called upon completion of each stage of unlocking process: authenticate, unlock, complete
 @note  Possible event values include:
        HYPRAPIEventResultFailure
        HYPRAPIEventResultAuthenticated
        HYPRAPIEventResultUnlocked
        HYPRAPIEventResultCompleted
 */
-(void)unlockRemoteDevice:(HYPRUserAgentRemoteDevice*)remoteDevice eventBlock:(HYPRUserAgentEventResult)eventBlock;

/**
 Update the display name for the provided Remote Device

 @note This device must be owned by the active User Account
 @note If an updated remote device is returned, then the current Profile has been updated and should be reloaded to display the current version.

 @param remoteDevice the Remote Device which should be renamed
 @param displayName the desired display name for the Remote Device
 @return an update instance of the Remote Device. `nil` if the Remote Device was not found in the active User Account.
 */
-(HYPRUserAgentRemoteDevice * _Nullable)renameRemoteDevice:(HYPRUserAgentRemoteDevice*)remoteDevice withDisplayName:(NSString*)displayName;

/**
 Set the chosen Remote Device as the Default Remote Device
 
 @note This device must be owned by the active User Account
 @note If an updated remote device is returned, then the current Profile has been updated and should be reloaded to display the current version.
 
 @param remoteDevice the Remote Device which should be renamed
 @param isDefault Setting this value to YES will lead to setting this value to YES to current remote device and to NO for all other remove devices
 @return same instance of the Remote Device. `nil` if the Remote Device was not found in the active User Account.
 */
-(HYPRUserAgentRemoteDevice * _Nullable)setRemoteDevice:(HYPRUserAgentRemoteDevice * _Nonnull)remoteDevice default:(BOOL)isDefault;

/**
 Return the current default remote device
 
 @note This device must be owned by the active User Account
 
 @return a remote device, which was set to defaulf for the current user. nil if there is no default device for the current user
 */
- (HYPRUserAgentRemoteDevice * _Nullable)defaultRemoteDevice;


/**
  Cancel the unlock request previously completed for a remote device.
 
  @note This device must be owned by the active User Account.
 
 @param remoteDevice the Remote Device which should be unlocked.
 @param completion handler called upon completion of the device unlock operation.
 */
-(void)cancelUnlock:(HYPRUserAgentRemoteDevice*)remoteDevice completion:(HYPRUserAgentGenericResult)completion;

/**
 Unregister the specified Remote Devices locally and remotely

 @note The Remote Devices must be owned by the active user account and belong to the current active profile.

 @param remoteDevices the Remote Devices which should be removed indicated by the deviceIdentifier field.
 @param completion handler called upon completion of the deregister operation.
 */
-(void)deregisterRemoteDevices:(NSArray<HYPRUserAgentRemoteDevice*>*)remoteDevices completion:(HYPRUserAgentGenericResult)completion;

/**
 Limit the number of workstations of specific type allowed. By default no limit is set.
 
 @param limit limits the maximum number of workstations across all wokrstation profiles.  Set nil to remove the limit.
 */
-(void)setWorkstationsLimit:(NSNumber* _Nullable)limit;

#pragma mark - Report Diagnostics

/**
 Report diagnostics to the relying party server for an error
 
 @note This method will either succeed or silently fail.
 
 @param message the diagnostic data that will be sent to the relying party. This is intended to be an error message or stack trace.
 @param remoteDevice the Remote Device which encountered the error.
 */
-(void)reportDiagnostics:(NSString *)message forRemoteDevice:(HYPRUserAgentRemoteDevice * _Nullable)remoteDevice;

@end

NS_ASSUME_NONNULL_END
