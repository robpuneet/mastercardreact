//
//  HYPRInterstitialViewModel.h
//  HyprCore
//
//  Created by Robert Panebianco on 4/9/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, HYPRInterstitialViewMode) {
    HYPRInterstitialViewModeAlert,
    HYPRInterstitialViewModeFullScreen, // not implemented
};

@interface HYPRInterstitialViewModel : NSObject
@property (nonatomic) HYPRInterstitialViewMode viewMode;
@property (nonatomic, copy, nullable) NSString *title;
@property (nonatomic, copy, nullable) NSString *message;
@end

NS_ASSUME_NONNULL_END
