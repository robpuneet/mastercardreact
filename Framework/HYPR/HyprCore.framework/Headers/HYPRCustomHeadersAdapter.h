//
//  HYPRCustomHeadersAdapter.h
//  HyprCore
//
//  Created by Robert Panebianco on 5/8/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#ifndef HYPRCustomHeadersAdapter_h
#define HYPRCustomHeadersAdapter_h

NS_ASSUME_NONNULL_BEGIN


// The following are valid keys in the userInfo dictionary that is passed to the customHeadersForUserInfo method for custom http headers.

/// Returns an NString* of which operation the delegate method was invoked in. See values below.
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterOperation;

/// Value for kHYPRCustomHeadersAdapterOperation key to describe a registration operation
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterOperationRegistration;

/// Value for kHYPRCustomHeadersAdapterOperation key to describe an authentication operation
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterOperationAuthentication;

/// Value for kHYPRCustomHeadersAdapterOperation key to describe a deregister operation
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterOperationDeregister;

/// Value for kHYPRCustomHeadersAdapterOperation key to describe a OOB setup operation
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterOperationOOBSetup;

/// Value for kHYPRCustomHeadersAdapterOperation key to describe a OOB registration operation
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterOperationOOBRegistration;

/// Value for kHYPRCustomHeadersAdapterOperation key to describe an OOB authorization operation
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterOperationOOBAuthorization;

/// Value for kHYPRCustomHeadersAdapterOperation key to describe an OOB status operation
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterOperationOOBStatus;

/// Value for kHYPRCustomHeadersAdapterOperation key to describe an OOB deregister operation
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterOperationOOBDeregister;

/// Value for kHYPRCustomHeadersAdapterOperation key to describe an OOB cancel unlock operation
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterOperationOOBCancelUnlock;

/// Returns an NString* of which phase the delegate method was invoked in. See values below.
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterPhase;

/// Value for kHYPRCustomHeadersAdapterOperationPhase key to describe a get request
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterPhaseInit;

/// Value for kHYPRCustomHeadersAdapterOperationPhase key to describe a send request
FOUNDATION_EXPORT NSString * const kHYPRCustomHeadersAdapterPhaseComplete;


@protocol HYPRCustomHeadersAdapter <NSObject>

/**
 A delegate can be provided to append custom headers for the relying party
 
 The userInfo will contain the following keys
 * kHYPRCustomHeadersAdapterOperation
 * kHYPRCustomHeadersAdapterOperationPhase
 
 These keys are intended to be used as reference for appending the appropriate custom headers for http requests
 
 @param userInfo Dictionary providing context about the operation type and the HTTP request
 @return Dictionary containing keys/values that will be appended to the http request of the given operation and type
 */
-(NSDictionary * _Nullable)customHeadersForUserInfo:(NSDictionary<NSString*,NSString*>*)userInfo;

/**
 A delegate can be provided to process the headers returned in the relying party response
 
 The userInfo will contain the headers returned from the relying party
 
 @param userInfo Dictionary providing the response headers from the relying party
 */
-(void)processCustomHeadersFromUserInfo:(NSDictionary<NSString*,NSString*>*)userInfo;
@end

NS_ASSUME_NONNULL_END

#endif /* HYPRCustomHeadersAdapter_h */
