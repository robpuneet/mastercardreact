//
//  HYPRError.h
//  HyprCore
//
//  Created by Robert Carlsen on 2/14/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 @discussion Constants used by NSError differentiate between "domains" of error codes, used to identify error codes which originate from specific subsystems.
 @const HYPRErrorDomain Indicates an error within HYPR Core SDK
 */
FOUNDATION_EXPORT NSErrorDomain const HYPRErrorDomain;

// Underlying errors will be included in the NSUnderlyingErrorKey value of the User Info object

/// Key in userInfo for HYPRErrorServer code. Contains the HYPRProblemResponse object describing the server error.
FOUNDATION_EXPORT NSErrorUserInfoKey const HYPRErrorServerProblemKey;
/// Key in userInfo for user action. Contains the string with possible user action to resolve this issue.
FOUNDATION_EXPORT NSErrorUserInfoKey const HYPRErrorUserActionKey;



/// Error codes describing conditions within the HYPRErrorDomain
typedef NS_ERROR_ENUM(HYPRErrorDomain, HYPRErrorCode)
{
    HYPRErrorSuccess = -0,

    /// An unknown error occurred
    HYPRErrorUnknown = -1,

    /// Parameters supplied were not valid for the specified operation
    HYPRErrorInvalidParameters = -100,

    /// The registration operation failed
    HYPRErrorRegistrationFailed = -101,

    /// The authentication operation failed
    HYPRErrorAuthenticationFailed = -102,

    /// The deregister operation failed
    HYPRErrorDeregisterFailed = -103,

    /// Unable to register the remote device
    HYPRErrorRemoteDeviceRegistrationFailed = -104,
    HYPRErrorRemoteDeviceSetupRegistrationFailed = -105,
    HYPRErrorRemoteDeviceCompleteRegistrationFailed = -106,

    /// Unable to unlock the remote device (eg. unlock windows machine)
    HYPRErrorRemoteDeviceUnlockFailed = -107,
    
    /// Unable to cancel the authorized unlock (eg. unlock windows machine)
    HYPRErrorRemoteDeviceCancelUnlockFailed = -108,

    /// Unable to complete the remote device authentication (eg. push notification-based operation for web login or transaction)
    HYPRErrorRemoteDeviceAuthenticationFailed = -109,
    
    // Unable to complete the deregister of one or more remote devices
    HYPRErrorRemoteDeviceDeregisterFailed = -110,

    /// The operation was not granted permission to complete
    HYPRErrorInsufficientPermission = -111,

    /// Unable to successfully complete license setup
    HYPRErrorLicenseSetupFailed = -112,

    /// Unable to successfully complete status request
    HYPRErrorStatusUpdateFailed = -113,
    
    // Unable to successfully complete reset app
    HYPRErrorResetAppFailed = -114,
    
    /// The operation was cancelled
    HYPRErrorCancelled = -999,    

    // User Agent errors
    // likely errors about configuration, profiles, and users.
    HYPRErrorUserAgent = -1000,
    HYPRErrorUserAgentRegistrationGetRequestError = -1001,
    HYPRErrorUserAgentRegistrationInvalidUAFResponse = -1002,
    HYPRErrorUserAgentRegistrationSendRequestError = -1003,
    HYPRErrorUserAgentAuthenticationGetRequestError = -1004,
    HYPRErrorUserAgentAuthenticationInvalidUAFResponse = -1005,
    HYPRErrorUserAgentAuthenticationSendRequestError = -1006,
    HYPRErrorUserAgentDeregisterGetRequestError = -1007,
    HYPRErrorUserAgentDeregisterInvalidUAFResponse = -1008,
    HYPRErrorUserAgentDeviceSetupGetRequestError = -1009,
    HYPRErrorUserAgentDeviceSetupSendRequestError = -1010,
    HYPRErrorUserAgentDeviceSetupNewUserError = -1011,
    HYPRErrorUserAgentDeviceSetupExistingUserError = -1012,
    HYPRErrorUserAgentDeviceUnlockAuthenticationError = -1013,
    HYPRErrorUserAgentDeviceUnlockAuthorizationError = -1014,
    HYPRErrorUserAgentDeviceUnlockAuthenticationHMACError = -1015,
    HYPRErrorUserAgentDeviceUnlockAuthorizationCompleteError = -1016,
    HYPRErrorUserAgentDeviceCredentialsNotFound = -1017,
    HYPRErrorUserAgentDeviceAuthenticateWithShoveFailed = -1018,
    HYPRErrorUserAgentNoProfileRegistered = -1019,
    HYPRErrorUserAgentNoUserAccountRegistered = -1020,
    HYPRErrorUserAgentDeviceStatusUpdateError = -1021,
    HYPRErrorUserAgentDeviceCancelUnlockError = -1022,
    HYPRErrorUserAgentDeviceCancelUnlockSessionRetrieveError = -1023,
    HYPRErrorUserAgentDeviceDeregisterSessionRetrieveError = -1024,
    HYPRErrorUserAgentDeviceFidoGetSessionSaveError = -1025,
    HYPRErrorUserAgentDeviceFidoSendRegSessionSaveError = -1026,
    HYPRErrorUserAgentDeviceFidoSendAuthSessionSaveError = -1027,
    HYPRErrorUserAgentDeviceSetupSessionSaveError = -1028,
    HYPRErrorUserAgentDeviceRegistrationsSessionSaveError = -1029,
    HYPRErrorUserAgentDeviceAuthorizeUnlockSessionSaveError = -1030,
    HYPRErrorUserAgentDeviceUnlockLoggedOutError = -1031,
    HYPRErrorUserAgentDeviceUnlockUnlockedError = -1032,//attempt to unlock already unlocked workstation
    HYPRErrorUserAgentWebProfilesLimitReached = -1033,
    HYPRErrorUserAgentWorkstationProfilesLimitReached = -1034,
    HYPRErrorUserAgentWorkstationsLimitReached = -1035,
    HYPRErrorUserAgentWebRemoteDeviceAlreadyPaired = -1036,
    HYPRErrorUserAgentWorkstationRemoteDeviceAlreadyPaired = -1037,
    HYPRErrorUserAgentWorkstationRemoteDeviceAuthorizationRejected = -1038,
    // Relying Party Client errors
    // should be errors raised at the RP Client layer...may contain NSURLErrors
    // do we need a separate group of networking errors?
    HYPRErrorRelyingParty = -2000,
    HYPRErrorRelyingPartyURLDataTaskRequestFailed = -2001,
    HYPRErrorRelyingPartyJSONSerializationFailed = -2002,
    HYPRErrorRelyingPartyUnexpectedResponsePayloadFormat = -2003,

    HYPRErrorLicense = -2500,
    HYPRErrorLicenseURLDataTaskRequestFailed = -2501,
    HYPRErrorLicenseJSONSerializationFailed = -2502,
    HYPRErrorLicenseUnexpectedResponsePayloadFormat = -2503,

    // HYPR Server errors
    // should this be pretty generic? Mostly a wrapper for the server-provided code?
    HYPRErrorServer = -3000,

    // FIDO Client errors
    // the high-level to the FIDO operations (UAF layer)
    HYPRErrorClient = -4000,
    HYPRErrorClientInitializationError = -4001,
    HYPRErrorClientUnknownOperation = -4002,
    HYPRErrorClientInvalidRequestType = -4003,
    HYPRErrorClientHTTPUsed = -4004,
    HYPRErrorClientTrustedFacetsFromURLError = -4005,
    HYPRErrorClientInvalidTrustedFacetsCount = -4006,
    HYPRErrorClientTrustedFacetsFromError = -4007,

    HYPRErrorClientRegistrationInvalidRequest = -4008,
    HYPRErrorClientRegistrationASMError = -4009,
    HYPRErrorClientRegistrationAuthenticatorNotFound = -4010,
    HYPRErrorClientRegistrationASMHandleRequestUserCancelled = -4011,
    HYPRErrorClientRegistrationASMHandleRequestAccessDenied = -4012,
    HYPRErrorClientRegistrationASMHandleRequestError = -4013,
    HYPRErrorClientRegistrationRegistrationResponseError = -4014,

    HYPRErrorClientAuthenticationInvalidRequest = -4015,
    HYPRErrorClientAuthenticationASMError = -4016,
    HYPRErrorClientAuthenticationAuthenticatorNotFound = -4017,
    HYPRErrorClientAuthenticationASMHandleRequestUserCancelled = -4018,
    HYPRErrorClientAuthenticationASMHandleRequestAccessDenied = -4019,
    HYPRErrorClientAuthenticationASMHandleRequestError = -4020,
    HYPRErrorClientAuthenticationAuthenticationResponseError = -4021,

    HYPRErrorClientDeregisterInvalidRequest = -4022,
    HYPRErrorClientDeregisterASMError = -4023,
    HYPRErrorClientDeregisterASMHandleRequestAccessDenied = -4024,
    HYPRErrorClientDeregisterASMHandleRequestError = -4025,

    HYPRErrorClientUnknownUnknownError = -4026,
    HYPRErrorClientSetAAIDPickerViewControllerError = -4027,

    // UAF ASM Layer errors
    // UAF <-> ASM errors
    HYPRErrorASM = -5000,
    HYPRErrorASMRegistrationInvalidRequestType = -5001,
    HYPRErrorASMRegistrationBadRegisterParameters = -5002,
    HYPRErrorASMRegistrationAuthenticatorNotFound = -5003,
    HYPRErrorASMRegistrationNoRegisterRequest = -5004,
    HYPRErrorASMRegistrationRegisterOutError = -5005,
    HYPRErrorASMRegistrationRegisterOutAccessDenied = -5006,
    HYPRErrorASMRegistrationRegisterOutUserCancelled = -5007,
    HYPRErrorASMRegistrationRegisterOutAttestationNotSupported = -5008,
    HYPRErrorASMRegistrationRegisterOutUnknownError = -5009,

    HYPRErrorASMAuthenticationInvalidRequestType = -5010,
    HYPRErrorASMAuthenticationBadAuthenticateParameters = -5011,
    HYPRErrorASMAuthenticationAuthenticatorNotFound = -5012,
    HYPRErrorASMAuthenticationBadSigningRequestParameters = -5013,
    HYPRErrorASMAuthenticationAuthenticateOutError = -5014,
    HYPRErrorASMAuthenticationAuthenticateOutAccessDenied = -5015,
    HYPRErrorASMAuthenticationAuthenticateOutUserNotEnrolled = -5016,
    HYPRErrorASMAuthenticationAuthenticateOutUserCancelled = -5017,
    HYPRErrorASMAuthenticationAuthenticateOutCmdNotSupported = -5018,
    HYPRErrorASMAuthenticationAuthenticateOutAttestationNotSupported = -5019,
    HYPRErrorASMAuthenticationAuthenticateOutCannotRenderContent = -5020,
    HYPRErrorASMAuthenticationAuthenticateOutUnknownError = -5021,
    HYPRErrorASMAuthenticationAuthenticateZeroKeyHandles = -5022,

    HYPRErrorASMDeregisterInvalidRequestType = -5023,
    HYPRErrorASMDeregisterBadDegisterParameters = -5024,
    HYPRErrorASMDeregisterAuthenticatorNotFound = -5025,
    HYPRErrorASMDeregisterUnableToParseCommand = -5026,
    HYPRErrorASMDeregisterAccessDenied = -5027,
    HYPRErrorASMDeregisterNoKeyHandle = -5028,
    HYPRErrorASMDeregisterHashNotEqual = -5029,
    HYPRErrorASMDeregisterDeleteError = -5030,

    HYPRErrorASMOpenSettingsInvalidRequestType = -5031,
    HYPRErrorASMOpenSettingsAuthenticatorNotFound = -5032,
    HYPRErrorASMOpenSettingsCmdNotSupported = -5033,
    HYPRErrorASMOpenSettingsUnknownError = -5034,

    HYPRErrorASMGetRegistrationsBadGetRegistrationParameters = -5035,
    HYPRErrorASMGetRegistrationsInvalidRequestType = -5036,
    HYPRErrorASMGetRegistrationsAuthenticatorNotFound = -5037,

    HYPRErrorASMGetInfoInvalidRequestType = -5038,
    
    
    // UAF Authenticator Layer errors
    // ASM <-> Authenticator errors
    HYPRErrorAuthenticator = -6000,
    HYPRErrorAuthenticatorRegisterAuthenticatorUnableToParseCommand = -6001,
    HYPRErrorAuthenticatorRegisterAuthenticatorAuthenticatorNotFound = -6002,
    HYPRErrorAuthenticatorRegisterAuthenticatorNoPublicKeyData = -6003,
    HYPRErrorAuthenticatorRegisterAuthenticatorNoRegistrationData = -6004,
    HYPRErrorAuthenticatorRegisterAuthenticatorNoSignedData = -6005,
    HYPRErrorAuthenticatorRegisterAuthenticatorNoRawSignature = -6006,
    HYPRErrorAuthenticatorRegisterAuthenticatonResultCancelled = -6007,
    HYPRErrorAuthenticatorRegisterAuthenticatonResultFailed = -6008,
    HYPRErrorAuthenticatorRegisterAuthenticatonResultUnavailable = -6009,
    HYPRErrorAuthenticatorRegisterAuthenticatonResultUnknown = -6010,
    HYPRErrorAuthenticatorRegisterEnrollmentResultCancelled = -6011,
    HYPRErrorAuthenticatorRegisterEnrollmentResultFailed = -6012,
    HYPRErrorAuthenticatorRegisterEnrollmentResultUnavailable = -6013,
    HYPRErrorAuthenticatorRegisterEnrollmentResultUnknown = -6014,
    HYPRErrorAuthenticatorRegisterEnrollmentAlreadyEnrolled = -6015,
    HYPRErrorAuthenticatorRegisterAuthenticatorGenerateKeyError = -6016,
    HYPRErrorAuthenticatorRegisterAuthenticatorHashAndSignError = -6017,
    
    HYPRErrorAuthenticatorDeregisterUnableToParseCommand = -6018,
    HYPRErrorAuthenticatorDeregisterAuthenticatorNotFound = -6019,
    
    HYPRErrorAuthenticatorSignUnableToParseCommand = -6020,
    HYPRErrorAuthenticatorSignAuthenticatorNotFound = -6021,
    HYPRErrorAuthenticatorSignNoAuthenticatorInfo = -6022,
    HYPRErrorAuthenticatorSignUserNotEnrolled = -6023,
    HYPRErrorAuthenticatorSignZeroKeyHandles = -6024,
    HYPRErrorAuthenticatorSignHashAndSignError = -6025,
    HYPRErrorAuthenticatorSignNoSignature = -6026,
    HYPRErrorAuthenticatorSignNoRawSignature = -6027,
    HYPRErrorAuthenticatorSignAuthenticationUnknownError = -6028,
    HYPRErrorAuthenticatorSignAuthenticationAccessDenied = -6029,
    HYPRErrorAuthenticatorSignAuthenticationUserCancelled = -6030,
    HYPRErrorAuthenticatorSignAuthenticationUserNotEnrolled = -6031,
    
    HYPRErrorAuthenticatorSignTransactionUnknownError = -6032,
    HYPRErrorAuthenticatorSignTransactionAccessDenied = -6033,
    HYPRErrorAuthenticatorSignTransactionCannotRenderContent = -6034,
    HYPRErrorAuthenticatorSignTransactionResultCancelled = -6035,
    
    HYPRErrorAuthenticatorGetInfoUnableToParseCommand = -6036,
    HYPRErrorAuthenticatorOpenSettingsUnableToParseCommand = -6037,


    // Authenticator Implementation errors
    // Authenticator framework errors...likely contain errors from vendor frameworks
    HYPRErrorAuthenticatorImplementation = -7000,
    HYPRErrorFaceIDAuthenticator = -7001,
    HYPRErrorFaceIDAuthenticatorNotAvailable = -7002,
    HYPRErrorFaceIDAuthenticatorCannotRenderContent = -7003,
    HYPRErrorFaceIDAuthenticatorConfirmationResultCancelled = -7004,
    HYPRErrorFaceIDAuthenticatorDeletionResultNotSupported = -7005,
    HYPRErrorFaceIDAuthenticatorAlreadyEnrolled = -7006,
    HYPRErrorFaceIDAuthenticatorDeletionResultNotEnrolled = -7007,
    HYPRErrorFaceIDAuthenticatorDeletionResultUnknown = -7008,

    HYPRErrorFingerprintAuthenticator = -7009,
    HYPRErrorFingerprintAuthenticatorNotAvailable = -7010,
    HYPRErrorFingerprintAuthenticatorCannotRenderContent = -7011,
    HYPRErrorFingerprintAuthenticatorConfirmationResultCancelled = -7012,
    HYPRErrorFingerprintAuthenticatorDeletionResultNotSupported = -7013,
    HYPRErrorFingerprintAuthenticatorAlreadyEnrolled = -7014,
    HYPRErrorFingerprintAuthenticatorDeletionResultNotEnrolled = -7015,
    HYPRErrorFingerprintAuthenticatorDeletionResultUnknown = -7016,

    HYPRErrorPINAuthenticatorAlreadyEnrolled = -7017,
    HYPRErrorPINAuthenticatorCannotRenderContent = -7018,
    HYPRErrorPINAuthenticatorConfirmationResultCancelled = -7019,
    HYPRErrorPINAuthenticatorDeletionResultNotEnrolled = -7020,
    HYPRErrorPINAuthenticatorDeletionResultUnknown = -7021,
    HYPRErrorPINAuthenticatorUserIsNotEnrolled = - 7022,

    HYPRErrorFingerprintAuthenticatorUserCancelled = -7023,
    HYPRErrorFaceIDAuthenticatorUserCancelled = -7024,
    
    HYPRErrorFaceAuthenticatorAlreadyEnrolled = -7025,
    HYPRErrorFaceAuthenticatorUserIsNotEnrolled = -7026,
    HYPRErrorFaceAuthenticatorDeregisterUnknownError = -7027,
    HYPRErrorFaceAuthenticatorCannotRenderContent = -7028,
    HYPRErrorFaceAuthenticatorUserCancelled = -7029,
    HYPRErrorFaceAuthenticatorEnrollmentResultFailed = -7030,
    HYPRErrorFaceAuthenticatorEnrollmentResultCancelled = -7031,
    HYPRErrorFaceAuthenticatorEnrollmentResultTimedOut = -7032,
    HYPRErrorFaceAuthenticatorEnrollmentResultUnknownError = -7033,
    HYPRErrorFaceAuthenticatorVerificationResultFailed = -7034,
    HYPRErrorFaceAuthenticatorVerificationResultCancelled = -7035,
    HYPRErrorFaceAuthenticatorVerificationResultTimedOut = -7036,
    HYPRErrorFaceAuthenticatorVerificationResultUnknownError = -7037,
    HYPRErrorFaceAuthenticatorUserDataMigrationFailed = -7038,
    
    
    HYPRErrorVoiceAuthenticatorAlreadyEnrolled = -7040,
    HYPRErrorVoiceAuthenticatorUserIsNotEnrolled = -7041,
    HYPRErrorVoiceAuthenticatorDeregisterUnknownError = -7042,
    HYPRErrorVoiceAuthenticatorCannotRenderContent = -7043,
    HYPRErrorVoiceAuthenticatorUserCancelled = -7044,
    HYPRErrorVoiceAuthenticatorEnrollmentResultFailed = -7045,
    HYPRErrorVoiceAuthenticatorEnrollmentResultCancelled = -7046,
    HYPRErrorVoiceAuthenticatorEnrollmentResultTimedOut = -7047,
    HYPRErrorVoiceAuthenticatorEnrollmentResultUnknownError = -7048,
    HYPRErrorVoiceAuthenticatorVerificationResultFailed = -7049,
    HYPRErrorVoiceAuthenticatorVerificationResultCancelled = -7050,
    HYPRErrorVoiceAuthenticatorVerificationResultTimedOut = -7051,
    HYPRErrorVoiceAuthenticatorVerificationResultUnknownError = -7052,
    HYPRErrorVoiceAuthenticatorUserDataMigrationFailed = -7053,
    
    HYPRErrorPINHeadlessAuthenticatorAlreadyEnrolled = -7060,
    HYPRErrorPINHeadlessAuthenticatorCannotRenderContent = -7061,
    HYPRErrorPINHeadlessAuthenticatorConfirmationResultCancelled = -7062,
    HYPRErrorPINHeadlessAuthenticatorDeletionResultNotEnrolled = -7063,
    HYPRErrorPINHeadlessAuthenticatorDeletionResultUnknown = -7064,
    HYPRErrorPINHeadlessAuthenticatorUserIsNotEnrolled = -7065,
    HYPRErrorPINHeadlessAuthenticatorEnrollmentViewControllerNil = -7066,
    HYPRErrorPINHeadlessAuthenticatorVerificationViewControllerNil = -7067,

    // HYPR ADP access erroror
    HYPRErrorADPNotInitialized = -8000,
    HYPRErrorADPNotAvailable = -8001,
    HYPRErrorADPCannotInitializeMoreThanOneInstance = -8002,
    
    // HYPRCore errors, typically from utility methods
    HYPRErrorCoreHMACValidationFailed = -20000,
    
    // Version Negotiation Errors
    HYPRErrorVersionNegotiationLowerThanSupported = -30000,
    HYPRErrorVersionNegotiationHigherThanSupported = -30001,
    
    // QR Code Scan related Errors
    HYPRErrorQRCodeScanPermissionError = -30010,
};

@interface HYPRError : NSObject
/**
 Builder for HYPR Error objects (NSError with HYPRErrorDomain)

 @param code The error code for this error
 @param error (Optional) any underlying error object
 @return NSError instance in the HYPRErrorDomain
 */
+(NSError*)errorWithCode:(HYPRErrorCode)code underlyingError:(NSError* _Nullable)error;

/**
 Builder for HYPR Error objects (NSError with HYPRErrorDomain)

 @param code The HYPRErrorCode for this error
 @param error (Optional) any underlying error object
 @param userInfo (Optional) additional User Info dictionary
 @return NSError instance in the HYPRErrorDomain
 */
+(NSError*)errorWithCode:(HYPRErrorCode)code underlyingError:(NSError* _Nullable)error userInfo:(NSDictionary* _Nullable)userInfo;
@end

@interface NSError (HYPR)
-(NSError*)rootError;
@end


NS_ASSUME_NONNULL_END
