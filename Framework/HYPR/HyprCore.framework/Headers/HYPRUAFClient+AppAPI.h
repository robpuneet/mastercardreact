//
//  HYPRUAFClient+AppAPI.h
//  HyprCore
//
//  Created by Robert Carlsen on 2/15/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <HyprCore/HyprCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYPRUAFClient (AppAPI)

/**
 Process the FIDO UAF App API URL

 Designed to be called from the UIApplicationDelegate -application:openURL:sourceApplication handler.
 Will returns NO if the URL is not a FIDO API URL to permit other URL handlers to process the URL.

 @param url FIDO UAF App API URL
 @param sourceApplication FIDO User Agent / Relying Party Calling App.
 @param parentViewController A view controller instance that the UAF Client may use to present any necessary views (eg. authenticators). If `nil` will attempt to use the Application's rootViewController. weakly retained.
 @param completion Completion handler with the resulting FIDO UAF App API Response URL. Will be called on the main queue only.
 @return YES if the incoming URL conforms to the FIDO UAF App API and could be processed. NO otherwise.
 */
-(BOOL)handleOpenURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication parentViewController:(UIViewController* _Nullable)parentViewController completion:(void (^)(NSURL* responseURL))completion;
@end

NS_ASSUME_NONNULL_END
