//
//  HyprAuthenticatorProtocol.h
//  HyprCore
//
//  Created by Robert Carlsen on 8/21/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <LocalAuthentication/LocalAuthentication.h>

#import <HyprCore/HYPRConstants.h>
#import <HyprCore/HYPRError.h>

@class AuthenticatorInfo;

NS_ASSUME_NONNULL_BEGIN

@protocol HyprAuthenticatorUIProtocol <NSObject>
/**
 View controller that should be used to present authenticator UI. Provided by host application.
 */
@property(nonatomic, weak)UIViewController *parentViewController;
@end

@protocol HyprAuthenticatorProtocol <NSObject>

/**
 Invokes the appropriate authenticator and return the result.

 Implementations must invoke third-party authenticators as necessary.

 @param userIdentifier Unique user identifier for this verification
 @param appId If AppID is provided, and this authenticator has a TC Display, show the App ID to the user during verification.
 @param completion block to be called with the verification result.
 */
-(void)verifyUser:(NSString*)userIdentifier forAppId:(nullable NSString*)appId completion:(void(^)(HYPRAuthenticationResult result, LAContext * _Nullable authContext, NSError * _Nullable error))completion;

/**
 Invoke the appropriate authenticator for enrollment and return the result

 @param userIdentifier Unique user identifier for this enrollment
 @param appId if appId is provided, and this authenticator has a transaction confirmation display, show the appId to the user during enrollment.
 @param completion block to be called with the enrollment result
 */
-(void)enrollUser:(NSString*)userIdentifier appId:(nullable NSString*)appId completion:(void(^)(HYPREnrollmentResult result, LAContext * _Nullable authContext, NSError * _Nullable error))completion;

/**
 The authenticator should present the transaction confirmation content and return the result.

 @param userIdentifier Unique user identifier for this confirmation
 @param transactionContent content data according to AuthenticatorInfo.tcDisplayContentType. eg. text/plain or image/png
 @param appId If appId is provided, and this authenticator has a TC Display, show the App ID to the user during confirmation.
 @param completion block to be called with the confirmation result.
 */
-(void)presentTransactionConfirmationForUser:(NSString*)userIdentifier withContent:(NSData*)transactionContent appId:(nullable NSString*)appId completion:(void(^)(HYPRTransactionConfirmationResult result, NSError * _Nullable error))completion;

/**
 Remove any persistent data for the provided user identifier.

 @note
 This is a non-cancellable operation and no user interface / confirmation should be presented.

 @param userIdentifier Unique user identifier for deletion.
 @param appId If appId is provided, could use this to confirm user enrollment during deletion.
 @param completion block to be called with the deletion result.
 */
-(void)deleteUser:(NSString*)userIdentifier appId:(nullable NSString*)appId completion:(void(^)(HYPRUserDeletionResult result, NSError * _Nullable error))completion;

/// Authenticator Information configuration.
-(AuthenticatorInfo*)info;

/// Implementation-specific version number. eg. 1
-(NSUInteger)version;

/**
 Returns true if the authenticator is available on the current device
 */
-(BOOL) isAvailable;

@optional
/**
 Reset all local authenticator data.

 This should delete any templates or other persistent data the authenticator stores.
 */
-(void)resetAuthenticatorData;
@end

NS_ASSUME_NONNULL_END
