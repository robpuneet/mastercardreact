//
//  HYPRLoopbackAsm.h
//  HyprCore
//
//  Created by Robert Carlsen on 9/1/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HyprCore/HyprAsmProtocol.h>
#import <HyprCore/ASMManager.h>

@interface HYPRLoopbackAsm : ASMManager <HyprAsmProtocol>
@end
