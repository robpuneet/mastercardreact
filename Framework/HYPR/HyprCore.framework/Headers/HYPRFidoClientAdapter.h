//
//  HYPRFidoClientAdapter.h
//  HyprCore
//
//  Created by Edward Poon on 8/6/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "HYPRInterstitialDelegateProtocol.h"

/**
 * Stand-alone implementation of the Fido Client wrapper.
 * This class would be used by customers who want to integrate
 * only the HYPR Fido Client into their app,
 * maintain control over network calls, and do not want to directly interface
 * HYPRUAFClient
 */
@interface HYPRFidoClientAdapter : NSObject
/**
 * Callback parameter containing the results from registerRequest, authenticateRequest, and deregisterRequest.
 * If an error occurs during any of these operations, then the error parameter will be set and the result String will be nil.
 * If the operation was successful, then the resultString will be set and the error will be nil.
 * The resultString may contain either success FIDO payload or @"NoError" string where the FIDO payload is not available (e.g. fido deregister)
 */
typedef void(^HYPRFidoClientGenericResult)(NSString * _Nullable resultString, NSError * _Nullable error);
/**
 Set the view controller that HYPR User Agent may use to present required views.
 
 @param viewController The parent view controller which will be used for presentations. Weakly retained.
 */
-(void)setParentViewController:(UIViewController *_Nonnull)viewController;
/**
 * Use to start registration
 * Results are returned back in the completion parameter in the form of a HYPRFidoClientGenericResult
 * HYPRFidoClientGenericResult contains a resultString and error object
 * If successful, the resultString will contain success payload to send back to the server
 * Failures during registration are returned back in the NSError object in the completion block
 *
 * @param fidoPayload - Stringified Registration Fido Payload that was received from the server.
 * @param completion which returns the success payload string and an error object.
 If error object is not nil then an error occurred during registration
 */
-(void)registerRequest:(NSString * _Nonnull) fidoPayload completion:(HYPRFidoClientGenericResult _Nonnull ) completion;
/**
 * Use to relay the results for Registration from the Server.
 * If an error occurred, it will be set in the error parameter
 * If true is returned from this method, then the operation was successful
 *
 * @param success - true if server returned a success for the previous register fidoPayload.
 *                  - false otherwise.
 * @return true if registration completed successfully, false otherwise
 *
 */
-(BOOL)registerDidComplete:(BOOL)success;
/**
 * Use to start authenticate
 * Results are returned back in the completion parameter in the form of a HYPRFidoClientGenericResult
 * HYPRFidoClientGenericResult contains a resultString and error object
 * If successful, the resultString will contain success payload to send back to the server
 * Failures during registration are returned back in the NSError object in the completion block
 *
 * @param fidoPayload - Stringified Authentication Fido Payload that was received from the server.
 * @param username - username, associsated with the user to be authenticated.
 * @param completion which returns the success payload string and an error object.
 *          If error object is not nil then an error occurred during authentication
 *
 */
-(void)authenticateRequest:(NSString * _Nonnull) fidoPayload username:(NSString * _Nonnull)username completion:(HYPRFidoClientGenericResult _Nonnull ) completion;
/**
 * Use to relay the results for Authentication from the Server.
 * If an error occurred, it will be set in the error parameter
 * If true is returned from this method, then the operation was successful
 *
 * @param success - true if server returned a success for the previous register fidoPayload.
 *                  - false otherwise.
 * @return true if authentication completed successfully, false otherwise
 */
-(BOOL)authenticateDidComplete:(BOOL)success;
/**
 * Use to start deregistration
 * Results are returned back in the completion parameter in the form of a HYPRFidoClientGenericResult
 * Failures during registration are returned back in the NSError object in the completion block
 *
 * @param fidoPayload - Stringified Deregistration Fido Payload that was received from the server.
 * @param username - username, associsated with the user to be deregistered.
 * @param completion which returns the success string and an error object.
 *          If error object is not nil then an error occurred during deregistration
 *          In case of success the resultString will equal @"NoError"
 */
-(void)deregisterRequest:(NSString * _Nonnull)fidoPayload username:(NSString * _Nonnull)username completion:(HYPRFidoClientGenericResult _Nonnull ) completion;
/**
 * Gets the KeyIDs for the Authenticator ID (aaid)
 * Will return nil if the aaid does not exist in the registered authenticators
 * @param aaid - Authenticator ID, id for the authenticator
 * @param appID - appID, is either the facetID (app bundle id) or the url with the list of trusted facets.
                - this value comes from the appID field within the registration/authentication fido payload
 * @param username - username, the fido username
 * @return the list of all the KeyIDs for the aaid, nil if the aaid does not exist in the registered authenticators
 */
-(NSArray<NSString*>* _Nullable)keyIDsForAAID:(NSString * _Nonnull)aaid appID:(NSString * _Nonnull)appID username:(NSString * _Nonnull)username;
/**
 This method enables or disables presenting the UI for user to pick the AAID or AAIDs if more than one choice exists after policy matcing.
 @param enabled - enables presenting the AAID picker UI. By default it will be presented. If set to NO - UAFClient will pick the first set of the policy matching result array.
 */
-(void)setAAIDPickerViewEnabled:(BOOL)enabled;
/**
 This method sets the custom UIViewController which will be presented to the user for picking the appropriate AAID(s) from the list, based on policy matching result array.
 If method is not called and the setAAIDPickerViewEnabled: method called with YES - the default viewController will be presented.
 @param viewController - the viewController MUST use the AuthenticatorPicker category. Check the UIViewController+AuthenticatorPicker.h for details
 @return error if the view controller fails to respond the category's methods.
 */
-(NSError* _Nullable)setAAIDPickerViewController:(UIViewController* _Nonnull)viewController;
/**
 @property interstitialViewDelegate conforms to HYPRInterstitialDelegateProtocol.
 Has to implement it's single method to provide custom text for the interstitial views. Only alerts supported at the moment.
 */
@property (nonatomic, weak) _Nullable id<HYPRInterstitialDelegateProtocol> interstitialViewDelegate;

/**
 This method enables or disables Interstitial View. NOTE: By default, Interstitial View is enabled and a default one is provided.
 @param enabled - enables Interstitial View.
 */
-(void)enableInterstitialView:(BOOL)enabled;

@end
