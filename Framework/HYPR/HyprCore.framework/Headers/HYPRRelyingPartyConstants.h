//
//  HYPRRelyingPartyConstants.h
//  HyprCore
//
//  Created by Robert Carlsen on 11/15/17.
//  Copyright © 2017 HYPR Corp. All rights reserved.
//

#ifndef HYPRRelyingPartyResponseCodes_h
#define HYPRRelyingPartyResponseCodes_h

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, HYPRRelyingPartyResponseCode) {
    HYPRRelyingPartyResponseCodeUnknown = 0,
    HYPRRelyingPartyResponseCodeSuccess = 200,
    HYPRRelyingPartyResponseCodeUnauthorized = 401,
    HYPRRelyingPartyResponseCodeAuthorizationRejected = 403,
    HYPRRelyingPartyResponseCodePinNotFound = 404,
    HYPRRelyingPartyResponseCodeRegistrationTimeout = 408,
    HYPRRelyingPartyResponseCodeAuthorizationTimeout = 408,
    HYPRRelyingPartyResponseCodeCancelledUnlock = 410,
    HYPRRelyingPartyResponseCodeWorkstationUnlocked = 412,//attempt to unlock already unlocked workstation
    HYPRRelyingPartyResponseCodeWorkstationLoggedOut = 428,
    HYPRRelyingPartyResponseCodeSessionTimeout = 440,
    HYPRRelyingPartyResponseCodeInternalServerError = 500,
};

extern NSString * const HYPRRelyingPartyDeviceTypeIOS;

#endif /* HYPRRelyingPartyResponseCodes_h */
