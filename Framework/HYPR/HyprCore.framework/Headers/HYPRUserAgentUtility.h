//
//  HYPRUserAgentUtility.h
//  HyprCore
//
//  Created by Ievgen Kreshchenko on 12/6/18.
//  Copyright © 2018 HYPR Corp. All rights reserved.
//

#import <Foundation/Foundation.h>
@class HYPRUserAgentProfile;
@class HYPRUserAgentRemoteDevice;

@interface HYPRUserAgentUtility : NSObject

+ (NSArray<HYPRUserAgentProfile*>*)allProfiles;
+ (NSArray<HYPRUserAgentProfile*>*)webProfiles;
+ (NSArray<HYPRUserAgentProfile*>*)activeWebProfiles;
+ (NSArray<HYPRUserAgentProfile*>*)workstationProfiles;
+ (NSArray<HYPRUserAgentRemoteDevice*>*)allWorkstations;
+ (NSArray<HYPRUserAgentRemoteDevice*>*)allWebRemoteDevices;

@end
