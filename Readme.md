## IDCM-Reference App-React Native

#### Overview -

This reference app is developed in a minimal and straight forward way so that developer can understand the 'How to integrate IDCM FIDO SDK'
This React-Native based cross platform app helps to understand the overall setup and best practices to integrate biometric authentication and its use cases for various scenarios.

#### Prerequisites -

- react-native: 0.58.6+
- react-native-cli: 2.0.1+
- Xcode 10.1+
- Android Studio 3.3+
- gradle:3.2.1+

#### How to Install -

`npm install`
`npm install -g react-native-cli`
`npm install --save react-navigation`
`npm install --save react-native-gesture-handler`
`react-native link react-native-gesture-handler`
`npm install --save react-native-base64`
`npm install --save react-native-firebase`
`react-native link react-native-firebase`
`npm install --save react-native-event-listeners`


#### Running a Project -

##### iOS -

* To run iOS Project for the first time, execute following command
`react-native bundle --entry-file index.js --platform ios --dev false --bundle-output ios/main.jsbundle --assets-dest ios`
* Then click on 'Run' button in Xcode

##### Android -

* To run Android Project for the first time, execute following command
`react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res/`
* To run Android Project in DEBUG mode use following command (some features mayn't work)
`react-native run-android`

* To run Android Project in RELEASE mode use following command
* react-native run-android --variant=release

##### Flow support -

* If you are using VS Code, please consider installing `FLOW`
* Flow - Flow is a static type checker for javascript. 
* Installation Guide - `https://flow.org/en/docs/install/`
* Extenstions -> Show Built-in Extenstions -> TypeScript and JavaScript Language Features -> Disable (Workspace)
