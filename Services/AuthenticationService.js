'use strict';
import ReactNativeRouter from './../NativeModules';
import {Platform} from 'react-native';
import {NativeModules} from 'react-native';

export default class AuthenticationService{

    static initializeFidoAuthentication(transcationId, assuranceLevel, authenticationType, auxiliaryData, successBlock,failBlock){

        var reactNativeRouter = NativeModules.ReactNativeRouter;
        if (Platform.OS === 'ios'){
              reactNativeRouter.initializeFidoAuthentication(transcationId,assuranceLevel,authenticationType,auxiliaryData,(responseString,error) => {
                if (error) {
                    failBlock(error);
                  } else {
                    successBlock(responseString);
                  }
                });

        }else{ // In case of Android
            ReactNativeRouter.initializeFidoAuthentication(transcationId,assuranceLevel,authenticationType,auxiliaryData,(responseString) => {
                successBlock(responseString);
            },  (error) => {
                failBlock(error);
            });
        } 
    }

    static processFidoAuthentication(encryptedString,successBlock,failBlock){
        var reactNativeRouter = NativeModules.ReactNativeRouter;
        if (Platform.OS === 'ios'){
            reactNativeRouter.processFidoAuthentication(encryptedString,(response,error) => {
                if (error) {
                    failBlock(error);
                  } else if(response) {
                    successBlock(response);
                  }else{
                    failBlock(new Error('Cancelled'));
                  }
                });

        }else{
            ReactNativeRouter.processFidoAuthentication(encryptedString,(responseString) => {
                successBlock(responseString);
            },  (error) => {
                failBlock(error);
            });
        } 
    }

    static completeFidoAuthentication(acknowledgmentResponse,successBlock,failBlock){
        var reactNativeRouter = NativeModules.ReactNativeRouter;
        if (Platform.OS === 'ios'){
            reactNativeRouter.completeFidoAuthentication(acknowledgmentResponse,(responseString,error) => {
                if (error) {
                    failBlock(error);
                  } else {
                    successBlock(responseString);
                  }
            });
        }else{ // In case of Android
            ReactNativeRouter.completeFidoAuthentication(acknowledgmentResponse,(responseString) => {
                successBlock(responseString);
            },  (error) => {
                failBlock(error);
            });
        } 
    }
}  
