'use strict';
import ReactNativeRouter from './../NativeModules';
import {Platform} from 'react-native';
import {NativeModules} from 'react-native';

export default class VerifyDeviceService{

    static verifyDevice(nickname,verificationCode,successBlock,failBlock){
        if (Platform.OS === 'ios'){
            var reactNativeRouter = NativeModules.ReactNativeRouter;
            console.log('verifying device', verificationCode, nickname)
            reactNativeRouter.verifyDevice(verificationCode,nickname,(response,error)=>{
                console.log('verify response', response, error)
                if (error) {
                    failBlock(error);
                  } else {
                    successBlock(response);
                  }
            });

        }else{ // In case of Android
            ReactNativeRouter.verifyDevice(nickname,verificationCode,(response) => {
                successBlock(response);
            },  (error) => {
                failBlock(error);
            });
        } 
    }
}  