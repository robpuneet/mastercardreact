//
//  Constants.h
//  ReferenceAppReactN
//
//  Created by Ghorpade, Krupal on 13/02/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

typedef void(^FailBlock)(NSError *error);
typedef void(^SuccessBlock)(id results);

#endif /* Constants_h */
