//
//  ReactNativeRouter.m
//  ReferenceAppReactN
//
//  Created by Ghorpade, Krupal on 13/02/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "ReactNativeRouter.h"
#import <MCBiometricsFIDO/IDCheckConfig.h>
#import <MCBiometricsFIDO/IDCheckService.h>
#import <MCBiometricsFIDO/Enums.h>
#import <React/RCTConvert.h>

@implementation ReactNativeRouter
RCT_EXPORT_MODULE();


RCT_EXPORT_METHOD(setMISUrl:(NSString *)urlString){
  [IDCheckConfig setBaseURL:urlString];
}

RCT_EXPORT_METHOD(registerAuthenticators){
  [[IDCheckService sharedInstance]registerAuthenticators:@[[NSNumber numberWithInt:AuthenticatorTypeFace],
                                                           [NSNumber numberWithInt:AuthenticatorTypeFaceID],
                                                           [NSNumber numberWithInt:AuthenticatorTypeTouchID],
                                                           [NSNumber numberWithInt:AuthenticatorTypePin],
                                                           [NSNumber numberWithInt:AuthenticatorTypeVoice]
                                                           ]];
}


RCT_EXPORT_METHOD(initializeDeviceRegistrationWithCompletionHandler:(RCTResponseSenderBlock)callback){

  [[IDCheckService sharedInstance]initializeDeviceRegistrationWithCompletionHandler:^(NSString * _Nullable responseString, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error]);
    }else{
      callback(@[responseString, [NSNull null]]);
    }
  }];
}

RCT_EXPORT_METHOD(completeDeviceRegistrationForAccount:(NSString *)tenantAccountId registrationInfo:(NSString*)registrationInfo callBack:(RCTResponseSenderBlock)callback){
  
  [[IDCheckService sharedInstance]completeDeviceRegistrationForAccount:tenantAccountId registrationInfo:registrationInfo completionHandler:^(BOOL isSuccess, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error]);
    }else{
      callback(@[@"SUCCESSFUL", [NSNull null]]);
    }
  }];
  

}


RCT_EXPORT_METHOD(verifyDevice:(NSString *)verificationCode nickname:(NSString*)nickname callBack:(RCTResponseSenderBlock)callback){
 NSLog(@"%@ %@",nickname, verificationCode);
  [[IDCheckService sharedInstance]verifyDeviceWithVerificationCode:verificationCode forNickname:nickname completionHandler:^(BOOL isSuccess, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error]);
    }else{
      callback(@[@"SUCCESSFUL", [NSNull null]]);
    }
  }];
  
}

RCT_EXPORT_METHOD(setupPushNotificationPayload:(NSDictionary*)payload callBack:(RCTResponseSenderBlock)callback){
  
  NSString *channelId = [RCTConvert NSString:payload[@"channelId"]];
  NSString *serverRandomData = [RCTConvert NSString:payload[@"serverRandomData"]];

  NSDictionary* mutableDictionary = [[NSMutableDictionary alloc]init];
  [mutableDictionary setValue:@"MCSDKPayload" forKey:@"pushType"];
  [mutableDictionary setValue:channelId forKey:@"channelId"];
  [mutableDictionary setValue:serverRandomData forKey:@"serverRandomData"];
  
  [IDCheckConfig setupPushNotificationWithPayload:mutableDictionary completionHandler:^(NSDictionary * _Nullable status, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error]);
    }else{
      callback(@[status, [NSNull null]]);
    }
  }];
}


 
RCT_EXPORT_METHOD(initializeFidoRegistration:(RCTResponseSenderBlock)callback){
  
  [[IDCheckService sharedInstance]initializeFIDORegistrationWithCompletionHandler:^(NSString * _Nullable responseString, NSError * _Nullable error) {
    if (error) {
      NSLog(@"%@",[error localizedDescription]);
      callback(@[[NSNull null], error]);
    }else{
      callback(@[responseString, [NSNull null]]);
    }
  }];
  
}

RCT_EXPORT_METHOD(processFidoRegistration:(NSString*)responseString callBack:(RCTResponseSenderBlock)callback){
  
  UIViewController* viewController = [UIApplication sharedApplication].keyWindow.rootViewController;
  
  [[IDCheckService sharedInstance]processFIDORegistrationWithResponseObject:responseString withParentViewController:viewController delegate:nil withCompletionHandler:^(NSString * _Nullable response, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error]);

    }else{
      callback(@[response, [NSNull null]]);
    }
  }];
  
}

RCT_EXPORT_METHOD(completeFidoRegistration:(NSString*)responseString callBack:(RCTResponseSenderBlock)callback){
  
  [[IDCheckService sharedInstance]completeFIDORegistrationWithResponse:responseString withCompletionHandler:^(NSString * _Nullable responseString, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error]);
    }else{
      callback(@[responseString, [NSNull null]]);
    }
  }];
  
}

RCT_EXPORT_METHOD(initializeFidoAuthentication:(NSString*)transcationId
                  assuranceLevel:(NSString*)assuranceLevelString
                  authenticationType:(NSString*)authenticationType
                  auxiliaryData:(NSString*)auxiliaryData
                  callBack:(RCTResponseSenderBlock)callback){
  
  AssuranceLevel assuranceLevel;
  if ([assuranceLevelString isEqualToString:@"low"]) {
    assuranceLevel = AssuranceLevelLow;
  }else if ([assuranceLevelString isEqualToString:@"medium"]) {
    assuranceLevel = AssuranceLevelMedium;
  }else{
    assuranceLevel = AssuranceLevelHigh;
  }
  
  [[IDCheckService sharedInstance]initializeFIDOAuthenticationWithTransactionID:transcationId
                                              withTransactionAuthenticationType:authenticationType
                                                              withAuxiliaryData:auxiliaryData
                                               withAuthenticationAssuranceLevel:assuranceLevel
                                                          withCompletionHandler:^(NSString * _Nullable responseString, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error]);
    }else{
      callback(@[responseString, [NSNull null]]);
    }
  }];
  
}

RCT_EXPORT_METHOD(processFidoAuthentication:(NSString*)encryptedString callBack:(RCTResponseSenderBlock)callback){
  
  UIViewController* viewController = [UIApplication sharedApplication].keyWindow.rootViewController;
  
  [[IDCheckService sharedInstance]processFIDOAuthenticationWithResponse:encryptedString delegate:nil onParentViewController:viewController withCompletionHandler:^(NSString * _Nullable responseString, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error]);
      
    }else{
      callback(@[responseString, [NSNull null]]);
    }
  }];
  
}

RCT_EXPORT_METHOD(completeFidoAuthentication:(NSString*)acknowledgmentResponse callBack:(RCTResponseSenderBlock)callback){
  
  [[IDCheckService sharedInstance]completeFIDOAuthenticationWithResponse:acknowledgmentResponse withCompletionHandler:^(BOOL isSuccess, NSError * _Nullable error){
    if (error) {
      callback(@[[NSNull null], error.localizedDescription]);
    }else{
      callback(@[@"SUCCESSFUL", [NSNull null]]);
    }
  }];
  
}

RCT_EXPORT_METHOD(initializeListDevices:(RCTResponseSenderBlock)callback){
  
  [[IDCheckService sharedInstance]initializeListDevicesWithCompletionHandler:^(NSString * _Nullable responseString, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error.localizedDescription]);
    }else{
      callback(@[responseString, [NSNull null]]);
    }
  }];
}


RCT_EXPORT_METHOD(completeListDevices:(NSString*)listDevicesResponse callBack:(RCTResponseSenderBlock)callback){
  
  [[IDCheckService sharedInstance]completeListDevicesWithObject:listDevicesResponse withCompletionHandler:^(id  _Nullable responseString, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error.localizedDescription]);
    }else{
      callback(@[responseString, [NSNull null]]);
    }
  }];
}

RCT_EXPORT_METHOD(initializeDisableDevices:(NSArray*)deviceNicknames callBack:(RCTResponseSenderBlock)callback){
  
  [[IDCheckService sharedInstance]initializeDisableDevices:deviceNicknames withCompletionHandler:^(NSString * _Nullable responseString, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error.localizedDescription]);
    }else{
      callback(@[responseString, [NSNull null]]);
    }
  }];
}


RCT_EXPORT_METHOD(completeDisableDevices:(NSString*)disableDevicesResponse callBack:(RCTResponseSenderBlock)callback){
  
  [[IDCheckService sharedInstance]completeDisableDevicesWithObject:disableDevicesResponse withCompletionHandler:^(id  _Nullable responseString, NSError * _Nullable error) {
    if (error) {
      callback(@[[NSNull null], error.localizedDescription]);
    }else{
      callback(@[responseString, [NSNull null]]);
    }
  }];
}

RCT_EXPORT_METHOD(deleteAccount){
  [[IDCheckService sharedInstance]deleteAccount];
}

RCT_EXPORT_METHOD(getAccountStatus:(RCTResponseSenderBlock)callback){
  if ([[IDCheckService sharedInstance]getNicknameForAccountStatus:ACCOUNT_ENROLLED].length > 0) {
    callback(@[[NSNumber numberWithInteger:ACCOUNT_ENROLLED], [NSNull null]]);
  }else if ([[IDCheckService sharedInstance]getNicknameForAccountStatus:ACCOUNT_VERIFIED].length > 0){
    callback(@[[NSNumber numberWithInteger:ACCOUNT_VERIFIED], [NSNull null]]);
  }else if ([[IDCheckService sharedInstance]getNicknameForAccountStatus:ACCOUNT_REGISTERED].length > 0){
    callback(@[[NSNumber numberWithInteger:ACCOUNT_REGISTERED], [NSNull null]]);
  }else{
    callback(@[[NSNumber numberWithInteger:ACCOUNT_INDETERMINATE], [NSNull null]]);
  }
}

RCT_EXPORT_METHOD(getNickname:(RCTResponseSenderBlock)callback){
  if ([[IDCheckService sharedInstance]getNicknameForAccountStatus:ACCOUNT_ENROLLED].length > 0) {
    callback(@[[[IDCheckService sharedInstance]getNicknameForAccountStatus:ACCOUNT_ENROLLED], [NSNull null]]);
  }else if ([[IDCheckService sharedInstance]getNicknameForAccountStatus:ACCOUNT_VERIFIED].length > 0){
    callback(@[[[IDCheckService sharedInstance]getNicknameForAccountStatus:ACCOUNT_VERIFIED], [NSNull null]]);
  }else if ([[IDCheckService sharedInstance]getNicknameForAccountStatus:ACCOUNT_REGISTERED].length > 0){
    callback(@[[[IDCheckService sharedInstance]getNicknameForAccountStatus:ACCOUNT_REGISTERED], [NSNull null]]);
  }else{
    callback(@[[NSNull null], [NSNull null]]);
  }
}
@end
